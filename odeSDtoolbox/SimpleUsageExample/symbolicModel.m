function s = symbolicModel()
    s = struct;
     
    %% STATES
    s.c = sym('c', [1 1]);
    s.h = sym('h', [1 1]);
    
    %% PARAMETES
    s.k = sym('k', [1 1]);
    s.n = sym('n', [1 1]);
    
    %% RIGHT HAND SIDE
    s.rhs_c =   - s.k*s.c*s.h;
    s.rhs_h = - s.n*s.c*s.h;

    %% group parameters, states, RHS
    s.symParameters = [s.k, s.n];
                   
    s.states = [s.c; s.h];
                          
    s.RHS = [s.rhs_c; s.rhs_h];
end