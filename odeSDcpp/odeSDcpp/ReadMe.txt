odeSDcpp is is part of the odeSD suite.
Coypright (c) 2016 Lukas Widmer

Reference:
P. Gonnet, S. Dimopoulos, L. Widmer and J. Stelling, "A Specialized ODE
Integrator for the Efficient Computation of Parameter Sensitivities",
Submitted to BMC Systems Biology, 2011.

License: 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.