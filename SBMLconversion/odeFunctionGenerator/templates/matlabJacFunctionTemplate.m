function [Jfx,Jgx,Jfp,Jgp] = ##jacFunction##(varargin)

    ##timeVariable## = varargin{1};
    x = varargin{2};

    if nargout > 1
        % odeSD
        if nargin > 3
            p = varargin{4};
            ##inputVariablesODESD##
        else
            p = ##Parameters##
        end
        if nargout > 1
            f = varargin{3};
        end
    else
        % MATLAB ODE integrators
        if nargin > 2
            p = varargin{3};
            ##inputVariablesMATLAB##
        else
            p = ##Parameters##
        end
    end


    if nargout<1
        error('Too many output arguments');
    end

    Jfx = zeros(##nStates##, ##nStates##);
##Jfx##

    if nargout == 1
        return
    end

    Jgx = Jfx*Jfx;
##Jgx##

    if nargout == 2
        return
    end

    Jfp = zeros(##nStates##, ##nParameters##);
##Jfp##

    if nargout == 3
        return
    end

    Jgp = Jfx*Jfp;
##Jgp##

end

