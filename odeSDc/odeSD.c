/*******************************************************************************
 * This file is part of odeSD.
 * Coypright (c) 2011 Pedro Gonnet (gonnet@maths.ox.ac.uk), Lukas Widmer,
 * Sotiris Dimopoulos and J�rg Stelling.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


 /* Standard header files */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>

/* Library headers */

#include "cblas.h"
#include "clapack.h"

/* Local includes. */
#include "errs.h"
#include "odeSD.h"


/** Default integrator options. */
const struct odeSD_opts odeSD_opts_default = { 1.e-6 , 1.e-6 , 0 , 0 , NULL , NULL , 0 , NULL , 1 , 0.0 , 0.0 };


/** Error codes. */
#define odeSD_err_ok                            0
#define odeSD_err_null                          -1
#define odeSD_err_malloc                        -2
#define odeSD_err_tspan                         -3
#define odeSD_err_params                        -4
#define odeSD_err_rhs                           -5
#define odeSD_err_rhsjac                        -6
#define odeSD_err_minstep                       -7
#define odeSD_err_lapack                        -8

/** Error messages. */
char *odeSD_err_msg[9] = {
	"Nothing bad happened.",
	"An unexpected NULL pointer was encountered.",
	"A call to malloc failed, probably due to insufficient memory.",
	"The supplied tspan is either too short or not monotonically increasing.",
	"No parameters were specified.",
	"Error calling the right-hand side function.",
	"Error calling the right-hand side Jacobian function.",
	"Minimum stepsize reached.",
	"A call to a LAPACK function failed."
};

/* the last error */
int odeSD_err = odeSD_err_ok;

/* the error macro. */
#define error(id)				( odeSD_err = errs_register( id , odeSD_err_msg[-(id)] , __LINE__ , __FUNCTION__ , __FILE__ ) )

/* LW Patch for Visual Studio */
#ifdef WIN32
#define alloca _alloca
#endif

/**
 * @brief Second-Derivative ODE Integrator.
 *
 * @param f The right-hand side function.
 * @param dfdx The Jacobians of the right-hand side.
 * @param nr_tspan Number of time intervals, should be at least 2.
 * @param tspan Pointer to an array of time points.
 * @param N Number of system variables.
 * @param x0 Initial state.
 * @param opts Pointer to a #odeSD_opts structure containing information
 *      on the behaviour of the integrator.
 * @param nr_params Number of parameters, use @c 0 to switch of parameter
 *      sensitivity calculations.
 * @param params Pointer to a vector with the @c M parameters.
 * @param varargin Pointer to be passed to right-hand side functions.
 */

int odeSD(
	int(*f)(double t, const double *x, const double *p, void *varargin, double *f, double *dfdt, double *J, double *dJ, double *dfdp, double *d2fdtdp),
	int(*dfdx)(double t, const double *x, const double *f, const double *p, void *varargin, double *J, double *dJ, double *dfdp, double *d2fdtdp),
	int nr_tspan, double *tspan,
	int N, const double *x0,
	struct odeSD_opts *opts,
	int nr_params, const double *params,
	void *varargin,
	double **t_out,
	double **x_out,
	double **s_out) 
{
	/* Constants */
	const int max_iter = 5;


	/* Local variables */
	double h_max, h_LU = 0, rtol, atol, tol;
	int i, j, k, offset, norm_control, *nonneg, nr_nonneg, tid, ind;
	int iter, recompJ = 1, recompLU, stepped, nrsteps = 0;
	int nvx = 0, nrtees, doparams = 0;
	int *p, info;

	/* Size of output. */
	int out_size = 100;

	/* Time steps. */
	double tee;

	/* System variables. */
	double *x, *x_new, *x_last;

	double *s = NULL, *dsdt = NULL, *d2sdt2 = NULL, *s_new = NULL, *dsdt_new = NULL, *d2sdt2_new = NULL;

	double h, h2, h3, h4, h_last = 0.0, scale = 1.0, scale_new;
	double *err, nerr, *relwt, relerr, y_max, *delta, ndelta, ndelta_old = 0.0, r, rpow;
	double *J_f, *J_Jf, *ecoeffs, *eff, *eff_new, *eff_last, *deffdt, *deffdt_new, *M;
	double t5, t17, t2, t4, t18, t22, t21, t19, t16, t15, t6, t11, t7, t12, t14, t13;
	double t9, t8, t10, t1, t23, t27, t33, t36;
	double w_f = 0, w_dfdt = 0, w_f_new = 0, w_deffdt_new = 0, w_f_last = 0;
	double temp, *dptr;
	double *vx = NULL, alpha[5];
	double *dfdp = NULL, *d2fdtdp = NULL, *perr = NULL, *dsdt_last = NULL;


	/* Check that we got f and dfdx. */
	if (f == NULL || dfdx == NULL)
		return error(odeSD_err_null);

	/* Check if we got a valid time span. */
	if (nr_tspan < 2 || tspan == NULL)
		return error(odeSD_err_tspan);
	for (k = 2; k < nr_tspan; k++)
		if (tspan[k] <= tspan[k - 1])
			return error(odeSD_err_tspan);

	/* Check if we got an initial condition. */
	if (x0 == NULL)
		return error(odeSD_err_null);

	/* Check if we got a valid options structure. */
	if (opts == NULL)
		return error(odeSD_err_null);

	/* Allocate and initialize the time array. */
	if (nr_tspan != 2)
		out_size = nr_tspan;
	if ((*t_out = (double *)malloc(sizeof(double) * out_size)) == NULL)
		return error(odeSD_err_malloc);
	tee = tspan[0];
	tid = 1;

	/* Allocate and initialize the system variables x. */
	if ((*x_out = (double *)malloc(sizeof(double) * out_size * N)) == NULL ||
		(x = (double *)alloca(sizeof(double) * N)) == NULL ||
		(x_new = (double *)alloca(sizeof(double) * N)) == NULL ||
		(x_last = (double *)alloca(sizeof(double) * N)) == NULL)
		return error(odeSD_err_malloc);
	memcpy(*x_out, x0, sizeof(double) * N);
	memcpy(x, x0, sizeof(double) * N);

	/* Extract the initial step size. */
	if (opts->InitialStep <= 0.0)
		h = (tspan[nr_tspan - 1] - tspan[0]) / 100;
	else
		h = opts->InitialStep;

	/* Extract the maximum step size. */
	h_max = opts->MaxStep;

	/* Extract the relative and absloute tolerance. */
	rtol = opts->RelTol;
	atol = opts->AbsTol;

	/* Get NormControl bit. */
	norm_control = opts->flags & odeSD_flag_NormControl;

	/* Get NonNegative array. */
	nr_nonneg = opts->nr_NonNegative;
	nonneg = opts->NonNegative;

	/* Get params (if there). */
	if (nr_params > 0 && params == NULL)
		return error(odeSD_err_null);
	if (nr_params == 0)
		params = NULL;

	/* Initialize the sensitivity matrices. */
	if ((doparams = (s_out != NULL)))
	{
		if (nr_params == 0)
			return error(odeSD_err_params);
		if ((*s_out = (double *)malloc(sizeof(double) * out_size * nr_params * N)) == NULL ||
			(s = (double *)alloca(sizeof(double) * nr_params * N)) == NULL ||
			(s_new = (double *)alloca(sizeof(double) * nr_params * N)) == NULL ||
			(dsdt = (double *)alloca(sizeof(double) * nr_params * N)) == NULL ||
			(d2sdt2 = (double *)alloca(sizeof(double) * nr_params * N)) == NULL ||
			(dsdt_new = (double *)alloca(sizeof(double) * nr_params * N)) == NULL ||
			(d2sdt2_new = (double *)alloca(sizeof(double) * nr_params * N)) == NULL ||
			(dfdp = (double *)alloca(sizeof(double) * nr_params * N)) == NULL ||
			(d2fdtdp = (double *)alloca(sizeof(double) * nr_params * N)) == NULL ||
			(perr = (double *)alloca(sizeof(double) * nr_params * N)) == NULL ||
			(dsdt_last = (double *)alloca(sizeof(double) * nr_params * N)) == NULL)
			return error(odeSD_err_malloc);
		memset(s, 0, sizeof(double) * nr_params * N);
		// bzero( s , sizeof(double) * nr_params * N );
		memset(*s_out, 0, sizeof(double) * nr_params * N);
		// bzero( *s_out , sizeof(double) * nr_params * N );
	}
	else 
	{
		dsdt = NULL;
		d2sdt2 = NULL;
	}


	/* Allocate the iteration matrix M. */
	if ((M = (double *)alloca(sizeof(double) * N * N)) == NULL)
		return error(odeSD_err_malloc);

	/* Allocate some local vectors. */
	if ((err = (double *)alloca(sizeof(double) * N)) == NULL ||
		(relwt = (double *)alloca(sizeof(double) * N)) == NULL ||
		(p = (int *)alloca(sizeof(int) * N)) == NULL ||
		(delta = (double *)alloca(sizeof(double) * N)) == NULL)
		return error(odeSD_err_malloc);

	/* Allocate output vectors and matrices for f and dfdx. */
	if ((eff = (double *)alloca(sizeof(double) * N)) == NULL ||
		(deffdt = (double *)alloca(sizeof(double) * N)) == NULL ||
		(eff_new = (double *)alloca(sizeof(double) * N)) == NULL ||
		(deffdt_new = (double *)alloca(sizeof(double) * N)) == NULL ||
		(eff_last = (double *)alloca(sizeof(double) * N)) == NULL ||
		(J_f = (double *)alloca(sizeof(double) * N * N)) == NULL ||
		(J_Jf = (double *)alloca(sizeof(double) * N * N)) == NULL)
		return error(odeSD_err_malloc);


	/* Call the RHS once to get an initial guess for the first step. */
	if (f(tee, x, params, varargin, eff, deffdt, J_f, J_Jf, dsdt, d2sdt2) < 0)
		return error(odeSD_err_rhs);
	recompJ = 0;


	/* Allocate and construct the first set of extrapolation coefficients. */
	if ((ecoeffs = (double *)alloca(sizeof(double) * N * 4)) == NULL)
		return error(odeSD_err_malloc);
	h2 = h * h;
	for (i = 0; i < N; i++) 
	{
		ecoeffs[i] = deffdt[i] * h2 * 0.5 - h * eff[i] + x[i];
		ecoeffs[i + N] = (eff[i] - deffdt[i] * h) * h;
		ecoeffs[i + 2 * N] = deffdt[i] * h2 * 0.5;
		ecoeffs[i + 3 * N] = 0.0;
	}


	/* Reduce the step size h until the first step is sane. */
	if (nr_nonneg > 0) 
	{

		/* loop... */
		while (1) 
		{

			/* Evaluate x_new. */
			temp = 1.0 + scale;
			for (i = 0; i < N; i++)
				x_new[i] = ecoeffs[i] + temp * (ecoeffs[i + N] + temp * (ecoeffs[i + 2 * N] + temp * ecoeffs[i + 3 * N]));

			/* Check for negative values. */
			for (i = 0; i < nr_nonneg; i++)
				if (x_new[nonneg[i]] < 0.0)
					break;

			/* If the above loop ran clean, just break. */
			if (i == nr_nonneg)
				break;

			/* Otherwise, scale the step and start over. */
			else 
			{
				h *= 0.7;
				h2 = h * h;
				scale *= 0.7;
			}

			/* If h is too small, bail with an error. */
			if (h < DBL_EPSILON * tee || h < DBL_EPSILON * (tspan[nr_tspan - 1] - tee))
				return error(odeSD_err_minstep);

		}

	}


	/* Main loop. */
	while (tee < tspan[nr_tspan - 1]) 
	{
		/* Truncate this step? */
		if (tee + h > tspan[nr_tspan - 1]) 
		{
			scale *= (tspan[nr_tspan - 1] - tee) / h;
			h = tspan[nr_tspan - 1] - tee;
			h2 = h * h;
		}
		if (h_max > 0 && h > h_max) 
		{
			scale *= h_max / h;
			h = h_max;
			h2 = h * h;
		}


		/* Extrapolate the new step. */
		temp = 1.0 + scale;
		for (i = 0; i < N; i++)
			x_new[i] = ecoeffs[i] + temp * (ecoeffs[i + N] + temp * (ecoeffs[i + 2 * N] + temp * ecoeffs[i + 3 * N]));


		/* Initialize the Newton iteration. */
		iter = 1;
		recompLU = (h != h_LU);
		y_max = fabs(x_new[0]);
		for (i = 1; i < N; i++)
			if (y_max < fabs(x_new[i]))
				y_max = fabs(x_new[i]);
		tol = y_max * DBL_EPSILON * N * 10.0;


		/* Newton iteration. */
		while (1) 
		{

			/* Check for smallest-possible step size. */
			if (h < DBL_EPSILON * tee || h < DBL_EPSILON * (tspan[nr_tspan - 1] - tee))
				return error(odeSD_err_minstep);


			/* Get eff_new and deffdt_new. */
			if (f(tee + h, x_new, params, varargin, eff_new, deffdt_new, NULL, NULL, NULL, NULL) < 0)
				return error(odeSD_err_rhs);


			/* Compute the error vector and the error norm. */
			relerr = 0.0;
			for (i = 0; i < N; i++) 
			{
				err[i] = x[i] + h * 0.5 * (eff[i] + eff_new[i]) + h2 * 8.33333333333333333e-02 * (deffdt[i] - deffdt_new[i]) - x_new[i];
				if ((tol > fabs(x[i])) && (tol > fabs(x_new[i])))
					relwt[i] = 1.0 / tol;
				else if (fabs(x[i]) > fabs(x_new[i]))
					relwt[i] = 1.0 / fabs(x[i]);
				else
					relwt[i] = 1.0 / fabs(x_new[i]);
				if (relerr < fabs(err[i] * relwt[i]))
					relerr = fabs(err[i] * relwt[i]);
			}


			/* Check for convergence. */
			if (relerr < DBL_EPSILON * 10.0) 
			{
				/* printf("odeSD_mex: step with t=%e, h=%e, relerr=%e, iter=%i\N",tee,h,relerr,iter); */
				break;
			}


			/* Do one Newton step, re-computing the Jacobian and/or
				LU-decomposition if necessary. */
			stepped = 0;
			while (1)
			{


				/* Recompute Jacobians and LU-decomposition? */
				if (iter == 1 && (recompJ || recompLU)) 
				{

					/* Get the Jacobians if necessary. */
					if (recompJ) 
					{
						if (dfdx(tee + h, x_new, eff_new, params, varargin, J_f, J_Jf, NULL, NULL) < 0)
							return error(odeSD_err_rhsjac);
					}

					/* Assemble the iteration matrix. */
					for (i = 0; i < N; i++) 
					{
						offset = i*N;
						for (j = 0; j < N; j++)
							M[offset + j] = h * 0.5 * J_f[offset + j] - h2 * 8.33333333333333333e-02 * J_Jf[offset + j];
						M[offset + i] -= 1.0;
					}

					/* Decompose the iteration matrix. */
					/* dgetrf( &N , &N , M , &N , p , &info ); */
					if ((info = clapack_dgetrf(CblasColMajor, N, N, M, N, p)) < 0)
						return error(odeSD_err_lapack);
					if (info > 0)
						for (i = 0; i < N; i++)
							if (M[i*N + i] == 0.0)
								M[i*N + i] = 1.0e-10;

					/* Set some flags and counters. */
					h_LU = h;

				}


				/* Compute a step. */
				for (i = 0; i < N; i++)
					delta[i] = err[i];
				/* dgetrs( "N" , &N , &one , M , &N , p , delta , &N , &info ); */
				if (clapack_dgetrs(CblasColMajor, CblasNoTrans, N, 1, M, N, p, delta, N) != 0)
					return error(odeSD_err_lapack);
				ndelta = fabs(delta[0] * relwt[0]);
				for (i = 1; i < N; i++)
					if (ndelta < fabs(delta[i] * relwt[i]))
						ndelta = fabs(delta[i] * relwt[i]);


				/* Approximate the convergence rate. */
				if (iter > 1)
					r = ndelta / ndelta_old;
				else
					r = 1.0;


				/* Check for slow convergence. */
				for (rpow = r, i = 1; i < max_iter - iter; i++)
					rpow *= r;
				if ((iter > 1 && (ndelta > 0.9*ndelta_old || ndelta * rpow > 0.5 * rtol)) || (iter > max_iter)) 
				{
					/* printf("odeSD_mex: slow convergence at t=%e, h=%e, r=%e, ||delta||=%e, iter=%i\N",tee,h,r,ndelta,iter); */
					iter = 1;
					if (recompJ) 
					{
						h *= 0.7; h2 = h * h;
						scale *= 0.7;
						temp = 1.0 + scale;
						for (i = 0; i < N; i++)
							x_new[i] = ecoeffs[i] + temp * (ecoeffs[i + N] + temp * (ecoeffs[i + 2 * N] + temp * ecoeffs[i + 3 * N]));
						break;
					}
					else 
					{
						recompJ = 1;
						continue;
					}
				}

				/* Accept the step. */
				for (i = 0; i < N; i++)
					x_new[i] -= delta[i];

				/* LW Patch: Enforce Non-Negativity */
				for (i = 0; i < nr_nonneg; i++)
					if (x_new[nonneg[i]] < 0.0)
						x_new[nonneg[i]] = 0.0;

				stepped = 1;
				break;


			} /* Take a single Newton step. */


		/* Check if step was taken. */
			if (stepped) 
			{

				/* Have we converged? */
				if ((iter > 1 && r / (1.0 - r) * ndelta < 0.5 * rtol) || (iter == 1 && ndelta < 0.5 * rtol)) 
				{
					/* printf("odeSD_mex: newton iteration converged at t=%e, h=%e (ndelta=%e, relerr=%e, r=%e ,iter=%i)\N",tee,h,ndelta,relerr,r,iter); */
					/* Fix-up the derivatives. */
					/* dgemv( "N" , &N , &N , &dmone , J_f , &N , delta , &one , &done , eff_new , &one );
					dgemv( "N" , &N , &N , &dmone , J_Jf , &N , delta , &one , &done , deffdt_new , &one ); */
					cblas_dgemv(CblasColMajor, CblasNoTrans, N, N, -1, J_f, N, delta, 1, 1.0, eff_new, 1);
					cblas_dgemv(CblasColMajor, CblasNoTrans, N, N, -1, J_Jf, N, delta, 1, 1.0, deffdt_new, 1);
					break;
				}

				/* Patch-up some things before we go on. */
				ndelta_old = ndelta;
				iter++;

			}


		} /* Newton iteration. */


	/* Reset the iteration flags. */
		recompJ = 0;


		/* Go through the non-negative array and set bad values to 0. */
		for (i = 0; i < nr_nonneg; i++)
			if (x_new[nonneg[i]] < 0.0)
				x_new[nonneg[i]] = 0.0;


		/* Select the correct error estimator. */
		if (nrsteps == 0)
		{
			h4 = h2 * h2;
			t9 = 3.0 / 256.0*h;
			t8 = h2 / 512.0;
			for (i = 0; i < N; i++) 
			{
				t11 = (eff_new[i] + eff[i])*h;
				t10 = (eff[i] - eff_new[i])*h;
				t7 = (deffdt[i] + deffdt_new[i])*h2;
				t6 = (deffdt[i] - deffdt_new[i])*h2;
				t5 = eff_new[i] * t9;
				t4 = deffdt_new[i] * t8;
				t2 = eff[i] * t9;
				t1 = deffdt[i] * t8;
				t19 = fabs(t2 - t5 + 3.0 / 512.0*t7);
				t23 = fabs(t2 - t4 + t5 + t1 - 3.0 / 128.0*x_new[i] + 3.0 / 128.0*x[i]);
				t27 = fabs(t10 / 64.0 + t7 / 128.0);
				t33 = fabs(9.0 / 512.0*t11 - 9.0 / 256.0*x_new[i] + 9.0 / 256.0*x[i] + 3.0 / 1024.0*t6);
				t36 = fabs(t1 + t4 + t10 / 256.0);
				err[i] = t19 + t23 + t27 + t33 + t36;
			}
		}
		else 
		{
			t5 = h_last * h_last;
			t17 = 1.0 / ((t5 + h2 + 2 * h*h_last)*t5);
			t2 = t5 * t5;
			t4 = t5*h_last;
			t18 = h2*t5;
			t22 = 0.4*t18 + 1.3333333333333333e+00*h*t4 + 0.5*t2;
			h4 = h2 * h2;
			t21 = h*t2;
			t19 = -0.2*h2;
			t16 = h2*t4;
			h3 = h*h2;
			t15 = t5*h3;
			w_f = (h + ((h3 - 2 * t4)*t19 + (1.6666666666666666e-01*h4 - t22)*h)*t17);
			w_dfdt = (0.5*h2 + ((-h3*h_last - 2 * t18)*t19 + (-0.8*t16 - 0.75*t15 - 1.6666666666666666e-01*h4*h_last - 4.1666666666666666e-01*t21)*h)*t17);
			w_f_new = ((-0.4*t16 + t22*h)*t17);
			w_deffdt_new = ((0.2*h4*t5 + (-1.3333333333333333e-01*t16 - 0.25*t15 - 8.3333333333333333e-02*t21)*h)*t17);
			w_f_last = (3.3333333333333333e-02*h3*h2*t17);
			for (i = 0; i < N; i++)
				err[i] = -x_new[i] + x[i] + w_f*eff[i] + w_dfdt*deffdt[i] + w_f_new*eff_new[i] + w_deffdt_new*deffdt_new[i] + w_f_last*eff_last[i];
			/* dgetrs( "N" , &N , &one , M , &N , p , err , &N , &info ); */
			if (clapack_dgetrs(CblasColMajor, CblasNoTrans, N, 1, M, N, p, err, N) != 0)
				return error(odeSD_err_lapack);
		}


		/* Compute scaling for next step. */
		if (norm_control)
		{
			for (tol = 0.0, nerr = 0.0, i = 0; i < N; i++)
			{
				nerr += err[i] * err[i];
				tol += x_new[i] * x_new[i];
			}
			nerr = sqrt(nerr);

			tol = rtol * sqrt(tol);
			if (tol < atol)
				tol = atol;
			scale_new = pow(0.5 * tol / nerr, 0.2);
			if (scale_new > 2.0)
				scale_new = 2.0;
		}
		else
		{
			scale_new = 2.0;
			for (i = 0; i < N; i++) {
				tol = rtol * fabs(x_new[i]);
				if (tol < atol)
					tol = atol;
				temp = pow(0.5 * tol / fabs(err[i]), 0.2);
				if (temp < scale_new)
					scale_new = temp;
			}
		}


		/* Did the error test fail? */
		if (scale_new < 8.705505632961241e-01)
		{
			/* printf("odeSD_mex: error test failed on vars at t=%e, h=%e, scale=%e\N",tee,h,scale_new); */
			if (scale_new < 0.1)
				scale_new = 0.1;
			else if (scale_new > 0.8)
				scale_new = 0.8;
			h *= scale_new;
			h2 = h * h;
			scale *= scale_new;
			continue;
		}


		/* Compute the parameter sensitivities if required. */
		if (doparams) 
		{
			/* Get the Jacobians at the converged solution. */
			if (dfdx(tee + h, x_new, eff_new, params, varargin, J_f, J_Jf, dfdp, d2fdtdp) < 0)
				return error(odeSD_err_rhsjac);


			/* Compute the iteration matrix. */
			for (i = 0; i < N * N; i++)
				M[i] = h * 0.5 * J_f[i] - h2 * 8.33333333333333333e-02 * J_Jf[i];
			for (i = 0; i < N; i++)
				M[i*N + i] -= 1.0;
			/* dgetrf( &N , &N , M , &N , p , &info ); */
			if ((info = clapack_dgetrf(CblasColMajor, N, N, M, N, p)) < 0)
				return error(odeSD_err_lapack);
			if (info > 0)
				for (i = 0; i < N; i++)
					if (M[i*N + i] == 0.0)
						M[i*N + i] = 1.0e-10;
			h_LU = h; recompJ = 0;


			/* Assemble the right-hand side of the sensitivity equations. */
			for (i = 0; i < nr_params * N; i++)
				s_new[i] = -s[i] - 0.5 * h * (dsdt[i] + dfdp[i]) - h2 * 8.33333333333333333e-02 * (d2sdt2[i] - d2fdtdp[i]);


			/* Solve the sensitivity equations. */
			/* dgetrs( "N" , &N , &nr_params , M , &N , p , s_new , &N , &info ); */
			if (clapack_dgetrs(CblasColMajor, CblasNoTrans, N, nr_params, M, N, p, s_new, N) != 0)
				return error(odeSD_err_lapack);

			/* Compute dsdt_new and d2sdt2_new. */
			for (i = 0; i < N * nr_params; i++) 
			{
				dsdt_new[i] = dfdp[i];
				d2sdt2_new[i] = d2fdtdp[i];
			}
			/* dgemm( "N" , "N" , &N , &nr_params , &N , &done , J_f , &N , s_new , &N , &done , dsdt_new , &N );
			dgemm( "N" , "N" , &N , &nr_params , &N , &done , J_Jf , &N , s_new , &N , &done , d2sdt2_new , &N ); */
			cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, N, nr_params, N, 1.0, J_f, N, s_new, N, 1.0, dsdt_new, N);
			cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, N, nr_params, N, 1.0, J_Jf, N, s_new, N, 1.0, d2sdt2_new, N);


			/* Compute the integration error in each parameter. */
			if (nrsteps == 0) 
			{
				/* For each parameter... */
				for (j = 0; j < nr_params; j++) 
				{
					h4 = h2 * h2;
					t9 = 3.0 / 256.0*h;
					t8 = h2 / 512.0;
					for (i = 0; i < N; i++) 
					{
						ind = j*N + i;
						t11 = (dsdt_new[ind] + dsdt[ind])*h;
						t10 = (dsdt[ind] - dsdt_new[ind])*h;
						t7 = (d2sdt2[ind] + d2sdt2_new[ind])*h2;
						t6 = (d2sdt2[ind] - d2sdt2_new[ind])*h2;
						t5 = dsdt_new[ind] * t9;
						t4 = d2sdt2_new[ind] * t8;
						t2 = dsdt[ind] * t9;
						t1 = d2sdt2[ind] * t8;
						t19 = fabs(t2 - t5 + 3.0 / 512.0*t7);
						t23 = fabs(t2 - t4 + t5 + t1 - 3.0 / 128.0*s_new[ind] + 3.0 / 128.0*s[ind]);
						t27 = fabs(t10 / 64.0 + t7 / 128.0);
						t33 = fabs(9.0 / 512.0*t11 - 9.0 / 256.0*s_new[ind] + 9.0 / 256.0*s[ind] + 3.0 / 1024.0*t6);
						t36 = fabs(t1 + t4 + t10 / 256.0);
						err[i] = t19 + t23 + t27 + t33 + t36;
					}
					if ( norm_control ) 
					{
						for ( nerr = 0.0, i = 0 ; i < N ; i++ )
						{
							nerr += err[i] * err[i];
						}
						nerr = sqrt( nerr );
                        for ( tol = 0.0 , i = j*N ; i < (j+1)*N ; i++ )
						{
							tol += s_new[i] * s_new[i];
						}
                            
                        tol = rtol * sqrt( tol );
                        if ( tol < atol )
                            tol = atol;
                        temp = pow( 0.5 * tol / nerr , 0.2 );
                        if ( temp < scale_new )
                            scale_new = temp;
                    }
					else 
					{
						for ( i = 0 ; i < N ; i++ ) 
						{
							ind = j*N + i;
							tol = rtol * fabs( s_new[ind] );
							if ( tol < atol )
								tol = atol;
                            temp = pow( 0.5 * tol / fabs( err[i] ) , 0.2 );
                            if ( temp < scale_new )
                                scale_new = temp;
                        }
					}
				}

			}

			else 
			{

				/* Construct the perr matrix. */
				for (i = 0; i < N * nr_params; i++)
					perr[i] = -s_new[i] + s[i] + w_f*dsdt[i] + w_dfdt*d2sdt2[i] + w_f_new*dsdt_new[i] + w_deffdt_new*d2sdt2_new[i] + w_f_last*dsdt_last[i];

				/* Solve using the iteration matrix to compute a step. */
				/* dgetrs( "N" , &N , &nr_params , M , &N , p , perr , &N , &info ); */
				if (clapack_dgetrs(CblasColMajor, CblasNoTrans, N, nr_params, M, N, p, perr, N) != 0)
					return error(odeSD_err_lapack);

				/* Get the scaling for each parameter. */
				for (j = 0; j < nr_params; j++) 
				{
					if ( norm_control ) 
					{
						for ( nerr = 0.0, tol = 0.0, i = j*N ; i < (j+1)*N; i++ )
						{
							tol  += s_new[i] * s_new[i];
							nerr += perr[i] * perr[i];
						}
						nerr = sqrt( nerr );
							
						tol = rtol * sqrt( tol );
						if ( tol < atol )
							tol = atol;
						temp = pow( 0.5 * tol / nerr , 0.2 );
						
                        if ( temp < scale_new )
                            scale_new = temp;
                    }
                    else 
					{
                        for ( i = j*N ; i < (j+1)*N ; i++ ) 
						{
							tol = rtol * fabs( s_new[i] );
							if ( tol < atol )
								tol = atol;
                            temp = pow( 0.5 * tol / fabs( perr[i] ) , 0.2 );
                            if ( temp < scale_new )
                                scale_new = temp;
                        }
                    }
				}

			}

			/* Did the error test fail? */
			if (scale_new < 8.705505632961241e-01)
			{
				/* printf("odeSD_mex: error test failed on vars at t=%e, h=%e, scale=%e\N",tee,h,scale_new); */
				if (scale_new < 0.1)
					scale_new = 0.1;
				else if (scale_new > 0.8)
					scale_new = 0.8;
				h *= scale_new;
				h2 = h * h;
				scale *= scale_new;
				continue;
			}

		} /* Compute parameter sensitivities. */


	/* Compute intermediate steps. */
		if (nr_tspan > 2 && tid < nr_tspan && tee + h >= tspan[tid]) 
		{

			/* Collect the intermediate times. */
			for (nrtees = 0; tid < nr_tspan && tee + h >= tspan[tid]; tid++)
			{
				if (++nrtees > nvx) 
				{
					if ((vx = (double *)realloc(vx, sizeof(double) * 5 * (nrtees + 5))) == NULL)
						return error(odeSD_err_malloc);
					nvx = nrtees + 5;
				}
				dptr = &(vx[(nrtees - 1) * 5]);
				dptr[0] = 1.0;
				dptr[1] = 2.0 * (tspan[tid] - tee) / h - 1.0;
				for (i = 2; i < 5; i++)
					dptr[i] = 2.0 * dptr[1] * dptr[i - 1] - dptr[i - 2];
			}

			/* Is there enough room? */
			if (nrsteps + nrtees >= out_size) 
			{
				out_size += nrtees + 100;
				if ((*t_out = (double *)realloc(*t_out, sizeof(double) * out_size)) == NULL)
					return error(odeSD_err_malloc);
				if ((*x_out = (double *)realloc(*x_out, sizeof(double) * N * out_size)) == NULL)
					return error(odeSD_err_malloc);
				if (doparams) 
				{
					if ((*s_out = (double *)realloc(*s_out, sizeof(double) * N * nr_params * out_size)) == NULL)
						return error(odeSD_err_malloc);
				}
			}

			/* Store values for T. */
			dptr = *t_out; dptr = &(dptr[nrsteps + 1]);
			for (i = 0; i < nrtees; i++)
				dptr[i] = tee + (vx[i * 5 + 1] + 1.0)*0.5 * h;

			/* For each variable... */
			for (i = 0; i < N; i++) 
			{

				/* Compute the coefficients of the system variables x[i]. */
				t9 = (-eff[i] - eff_new[i])*h;
				t10 = (-eff[i] + eff_new[i])*h;
				t16 = (deffdt_new[i] + deffdt[i])*h2;
				t15 = (deffdt[i] - deffdt_new[i])*h2;
				t8 = 0.1953125E-2*h2;
				t7 = deffdt[i] * t8;
				t5 = deffdt_new[i] * t8;
				alpha[0] = 0.5*x_new[i] + 0.5*x[i] - 0.7421875E-1*t10 + 0.5859375E-2*t16;
				alpha[1] = -1.0*t7 + t5 + 0.4296875E-1*t9 + 0.5859375*x_new[i] - 0.5859375*x[i];
				alpha[2] = 0.78125E-1*t10 - 0.78125E-2*t16;
				alpha[3] = 0.29296875E-2*t15 - 0.48828125E-1*t9 + 0.9765625E-1*x[i] - 0.9765625E-1*x_new[i];
				alpha[4] = t5 + t7 - 0.390625E-2*t10;

				/* For each time point... */
				for (j = 0; j < nrtees; j++) 
				{
					dptr = &(vx[j * 5]);
					temp = alpha[0] + alpha[1] * dptr[1] + alpha[2] * dptr[2] + alpha[3] * dptr[3] + alpha[4] * dptr[4];
					dptr = *x_out;
					dptr[(nrsteps + j + 1) * N + i] = temp;
				}

			}

			/* For each parameter sensitivity. */
			if (doparams)
				for (k = 0; k < nr_params; k++) 
				{

					/* Set the offset inside the sentitivity matrices. */
					offset = k*N;

					for (i = 0; i < N; i++) 
					{

						/* Compute the coefficients of the sensitivities s_k[i]. */
						offset = k*N;
						t9 = (-dsdt[offset + i] - dsdt_new[offset + i])*h;
						t10 = (-dsdt[offset + i] + dsdt_new[offset + i])*h;
						t16 = (d2sdt2_new[offset + i] + d2sdt2[offset + i])*h2;
						t15 = (d2sdt2[offset + i] - d2sdt2_new[offset + i])*h2;
						t8 = 0.1953125E-2*h2;
						t7 = d2sdt2[offset + i] * t8;
						t5 = d2sdt2_new[offset + i] * t8;
						alpha[0] = 0.5*s_new[offset + i] + 0.5*s[offset + i] - 0.7421875E-1*t10 + 0.5859375E-2*t16;
						alpha[1] = -1.0*t7 + t5 + 0.4296875E-1*t9 + 0.5859375*s_new[offset + i] - 0.5859375*s[offset + i];
						alpha[2] = 0.78125E-1*t10 - 0.78125E-2*t16;
						alpha[3] = 0.29296875E-2*t15 - 0.48828125E-1*t9 + 0.9765625E-1*s[offset + i] - 0.9765625E-1*s_new[offset + i];
						alpha[4] = t5 + t7 - 0.390625E-2*t10;

						/* For each time point... */
						for (j = 0; j < nrtees; j++) 
						{
							dptr = &(vx[j * 5]);
							temp = alpha[0] + alpha[1] * dptr[1] + alpha[2] * dptr[2] + alpha[3] * dptr[3] + alpha[4] * dptr[4];
							dptr = *s_out;
							dptr[(nrsteps + j + 1) * N * nr_params + offset + i] = temp;
						}

					}

				}

			/* Adjust nrsteps. */
			nrsteps += nrtees;

		} /* Compute intermediate steps. */


	/* Truncate small scalings to avoid re-computing the LU-decomposition. */
		if (fabs(1.0 - scale_new) < 0.05)
			scale_new = 1.0;


		/* Compute extrapolation coefficients for next step. */
		if (nrsteps == 0)
		{
			for (i = 0; i < N; i++) 
			{
				ecoeffs[i] = x[i];
				ecoeffs[i + N] = eff[i] * h;
				ecoeffs[i + 2 * N] = -(2.0 * eff[i] * h + 3.0 * x[i] - 3.0 * x_new[i] + h * eff_new[i]);
				ecoeffs[i + 3 * N] = 2.0 * x[i] + h * eff[i] - 2.0 * x_new[i] + h * eff_new[i];
			}
		}
		else 
		{
			t6 = h_last * h_last;
			t11 = h2 + 2 * h*h_last + t6;
			t16 = 1.0 / (t11*h_last);
			t7 = h*h2;
			t5 = h_last*t6;
			t12 = -3 * h_last*h2 + t5;
			for (i = 0; i < N; i++)
			{
				t15 = x_new[i] * t6;
				t14 = t7 * x_last[i];
				t13 = eff_new[i] * t6;
				ecoeffs[i] = x[i];
				ecoeffs[i + N] = (2 * t5*x_new[i] - t14 - h2*t13 + (-t5*eff_new[i] + 3 * t15)*h + (-2 * t5 + t7 - 3 * t6*h)*x[i])*t16;
				ecoeffs[i + 2 * N] = -(-2 * t14 + (-t5*h + t7*h_last)*eff_new[i] + t12*x_new[i] + (2 * t7 - t12)*x[i])*t16;
				ecoeffs[i + 3 * N] = h*(-t15 + (h_last*eff_new[i] - x_last[i])*h2 + (-2 * h_last*x_new[i] + t13)*h + t11*x[i])*t16;
			}
		}


		/* Update the system variables. */
		for (i = 0; i < N; i++) 
		{
			x_last[i] = x[i];
			eff_last[i] = eff[i];
			x[i] = x_new[i];
			eff[i] = eff_new[i];
			deffdt[i] = deffdt_new[i];
		}
		if (doparams)
			for (i = 0; i < N * nr_params; i++) 
			{
				s[i] = s_new[i];
				dsdt_last[i] = dsdt[i];
				dsdt[i] = dsdt_new[i];
				d2sdt2[i] = d2sdt2_new[i];
			}
		h_last = h;
		tee += h;
		scale = scale_new;
		h *= scale;
		h2 = h * h;

		/* Add the current point to the results. */
		if (nr_tspan == 2) 
		{
			if (++nrsteps >= out_size) 
			{
				out_size += 100;
				if ((*t_out = (double *)realloc(*t_out, sizeof(double) * out_size)) == NULL)
					return error(odeSD_err_malloc);
				if ((*x_out = (double *)realloc(*x_out, sizeof(double) * N * out_size)) == NULL)
					return error(odeSD_err_malloc);
				if (doparams) 
				{
					if ((*s_out = (double *)realloc(*s_out, sizeof(double) * N * nr_params * out_size)) == NULL)
						return error(odeSD_err_malloc);
				}
			}
			dptr = *t_out;
			dptr[nrsteps] = tee;
			dptr = *x_out; dptr = &(dptr[nrsteps*N]);
			for (i = 0; i < N; i++)
				dptr[i] = x[i];
			if (doparams) 
			{
				dptr = *s_out; dptr = &(dptr[nrsteps*N*nr_params]);
				for (i = 0; i < N * nr_params; i++)
					dptr[i] = s[i];
			}
		}

	} /* Main loop. */


/* Trim T and X. */
	if (nr_tspan == 2)
		out_size = nrsteps + 1;
	*t_out = (double *)realloc(*t_out, sizeof(double) * out_size);
	*x_out = (double *)realloc(*x_out, sizeof(double) * N * out_size);
	if (doparams)
		*s_out = (double *)realloc(*s_out, sizeof(double) * N * nr_params * out_size);

	/* Return total number of steps. */
	return nrsteps + 1;

}
