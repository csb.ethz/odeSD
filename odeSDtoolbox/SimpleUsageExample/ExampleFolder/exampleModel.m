function [f,g,Jfx,Jgx,Jfp,Jgp] = exampleModel( varargin )
    additionalArgs = {};

    if nargin<1
        f = [1;1];
        g = [1;1];
        return
    elseif nargin==2 
        time = varargin{1};
        x = varargin{2};
        p = [1;1];
    else
        time = varargin{1};
        x = varargin{2};
        p = varargin{3};
        if nargin > 3
            additionalArgs = varargin(4:end);
        end
    end 

    f = [-1.0*p(1)*x(1)*x(2);
		-1.0*p(2)*x(1)*x(2)];

    if nargout<2 
        return
    end

    g = [- 1.0*f(1)*p(1)*x(2) - 1.0*f(2)*p(1)*x(1);
		- 1.0*f(1)*p(2)*x(2) - 1.0*f(2)*p(2)*x(1)];

    if nargout<3 
         return 
    elseif nargout==3 
         [Jfx] = jac_exampleModel(time, x, f, p, additionalArgs{:});
    elseif nargout==4 
         [Jfx,Jgx] = jac_exampleModel(time, x, f, p, additionalArgs{:});
    elseif nargout==5 
          [Jfx,Jgx,Jfp] = jac_exampleModel(time, x, f, p, additionalArgs{:});
    elseif nargout==6 
          [Jfx,Jgx,Jfp,Jgp] = jac_exampleModel(time, x, f, p, additionalArgs{:});
    elseif nargout>6 
         error('Too many output arguments');
    end
end
