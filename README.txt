A Specialized ODE Integrator for the Efficient Computation of Parameter Sensitivities
--------------------------------------------------------------------------------------

Version 1.1

Contents:
----------------------------

1) odeSDtoolbox:   folder with MATLAB implementation of ODE Integrator (odeSD.m) as well as the 
                   c-programming language implementation of the solver (odeSD_mex.c), using
                   the MATLAB mex interface with calls to the LAPACK and BLAS libraries for the 
			       algebraic operations. Example is provided in the Example subfolder.

2) odeSDc:         folder with c-programming language implementation of the solver (odeSD.c) and example

3) SBMLconversion: folder that contains the following subfolders:
                   a) all SBML models used in the paper (allSBMLmodels) 
                   b) matlab files for generating from an SBML model the necessary functions for
                      the solvers (ODEFUN_GEN)


Requirements:
----------------------------

 1) odeSDtoolbox:   The integrator has been tested under Matlab versions R2014b and R2015a on 
				    Linux, Windows and Mac OS. It does not require any specific toolboxes or features.
                    odeSD_mex requires a C compiler that is compatible with MATLAB

 2) odeSDc:         The integrator requires both the LAPACK and CBLAS libraries, as well as the 
                    CLAPACK headers, for compilation. The Makefile assumes that the Atlas library is
                    installed in "/usr/include/atlas". If this is not the case, the "CFLAGS" and 
					"LIBS" lines in the Makefile need to be modified accordingly.
					
					On Ubuntu, you will need to install the libatlas-dev and libatlas-base-dev packages.
					For Windows x86 ATLAS is provided, see ATLAS-LICENSE.txt. Binary libraries are 
					from http://www.terborg.net/research/kml/installation.html#x1-80003.3.1

 3) SBMLconversion: The SBMLconversion routines have the 2 following requirements
                     a) SBtoolbox2 (http://www.sbtoolbox2.org) 
                     b) Symbolic Toolbox V5.5 or higher
					 
					Version 1.1 tested with libSBML 5.11.4, SBMLToolbox 4.1.0 and SBPOP r1361



Usage:
----------------------------

Every folder contains an example script that the user can follow and run whichever
version of the solver is most appropriate.  
