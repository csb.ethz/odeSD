// odeSDcpp.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include <functional>
#include <odeSD/Solver>
#include <odeSD/SensitivitySolver>
#include "ElowitzModel.hpp"
#include "HornbergModel.hpp"
#include "UnderdampedOscillator.hpp"
#include "Elowitz2000_6states.cpp"

#include <chrono>


using namespace std;
using namespace odeSD;
using namespace Eigen;





int main(int argc, char* argv[])
{
	typedef std::chrono::high_resolution_clock Clock;

	odeSD::Options options;
	options.absoluteTolerance = 1e-4;
	options.relativeTolerance = 1e-4;

	Elowitz2000_6statesModel testModel;



	/* Initialize Result Data Structures */
	vector<Eigen::MatrixXd> results;
	vector<Eigen::MatrixXd> sensitivities;
	int nIntegrations = 1;

	results.resize(nIntegrations);
	sensitivities.resize(nIntegrations);

	for (int i = 0; i < nIntegrations; i++)
	{
		results[i] = MatrixXd(testModel.getInitialState().nStates, testModel.tspan.rows());
		sensitivities[i] = MatrixXd(testModel.getInitialState().nStates * testModel.getInitialState().nParameters, testModel.tspan.rows());
	}

	
	/* Benchmark without sensitivities */
	{
		Solver integrator(testModel, options);
		// Enforce non-negativity
		options.nonNegativeStates = Eigen::VectorXi(6);
		for (int i = 0; i < testModel.initialState.nStates; i++)
		{
			options.nonNegativeStates(i) = i;
		}
		auto tStart = Clock::now();
		for (int i = 0; i < nIntegrations; i++)
		{
			integrator.integrate();

			results[i] = testModel.states;
		}
		auto tEnd = Clock::now();
		chrono::milliseconds deltaT = chrono::duration_cast<chrono::milliseconds>(tEnd - tStart);

		ofstream myfile;
		myfile.open("matrix.txt");

		myfile << "Integrated " << nIntegrations << " times. Duration: " << deltaT.count() << endl;
		std::cout << "Integrated " << nIntegrations << " times. Duration: " << deltaT.count() << endl;

		for (int i = 0; i < nIntegrations; i += 10)
		{
			myfile << "Result " << i << ":\n";
			myfile << results[i].format(Eigen::FullPrecision) << endl;

			myfile << "Intermediate Times " << i << ":\n";
			myfile << testModel.intermediateTimes.format(Eigen::FullPrecision) << endl;
			myfile << "Intermediate Results " << i << ":\n";
			myfile << testModel.intermediateStates.format(Eigen::FullPrecision) << endl;
		}
		myfile.close();
	}
	
	/* Benchmark with sensitivities */
	{
		SensitivitySolver sensitivityIntegrator(testModel, options);

		auto tStart = Clock::now();
		for (int i = 0; i < nIntegrations; i++)
		{
			sensitivityIntegrator.integrate();

			results[i] = testModel.states;
			sensitivities[i] = testModel.sensitivities;
		}
		auto tEnd = Clock::now();
		chrono::milliseconds deltaT = chrono::duration_cast<chrono::milliseconds>(tEnd - tStart);

		ofstream myfile;
		myfile.open("sensitivityMatrix.txt");

		myfile << "Integrated with sensitivities " << nIntegrations << " times. Duration: " << deltaT.count() << endl;
		std::cout << "Integrated with sensitivities " << nIntegrations << " times. Duration: " << deltaT.count() << endl;

		for (int i = 0; i < nIntegrations; i += 10)
		{
			myfile << "Result " << i << ":\n";
			myfile << results[i].format(Eigen::FullPrecision) << endl;
			myfile << "Sensitivities" << endl;
			myfile << sensitivities[i].format(Eigen::FullPrecision) << endl;


			myfile << "Intermediate Times " << i << ":\n";
			myfile << testModel.intermediateTimes.format(Eigen::FullPrecision) << endl;
			myfile << "Intermediate Results " << i << ":\n";
			myfile << testModel.intermediateStates.format(Eigen::FullPrecision) << endl;
			myfile << "Intermediate Sensitivites " << i << ":\n";
			myfile << testModel.intermediateSensitivities.format(Eigen::FullPrecision) << endl;
			
		}
		myfile.close();
	}
	
	
}
