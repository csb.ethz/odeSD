function install_odeSDtoolbox(options)
    debug = false;
    
    if exist('options', 'var')
        if ~isstruct(options)
            error('Options must be a struct!');
        end
        if isfield(options, 'debug')
            if ~islogical(options.debug) || ~isscalar(options.debug)
                error('Debug flag must be a boolean (true / false)');
            end
            debug = options.debug;
        end
    end


    clc
    odeSDroot = fileparts(mfilename('fullpath'));
    currentFolder = cd([odeSDroot filesep 'odeSDtoolbox']);
    
    version = odeSD_version();

    fprintf('\n');
    fprintf('---------------------------------------------------------------------------\n');
    fprintf('ODESD Toolbox Version %s, http://www.csb.ethz.ch \n', version);
    fprintf('---------------------------------------------------------------------------\n');
    fprintf('Configuration:\n');
    if debug
        fprintf('\tDebug Mode: On\n');
    else
        fprintf('\tDebug Mode: Off\n');
    end
    fprintf('\n');
        
    solvers = 'odeSD';
    compileMex = YNprompt('The default is to compile the faster MEX version odeSD_mex.\nThis requires an installed C compiler (see mex -setup).\nWould you like to compile odeSD_mex [Y/N]? ', true);

    if compileMex
        fprintf('Compiling Solver:\n\n');
        
        odeSD_compile('odeSD_mex.c', debug);
        fprintf('\nodeSD_mex solver compiled successfully.\n');
        solvers = [solvers ', odeSD_mex'];
    end

    %% 
    printSeparator();

    addPathToMATLAB = YNprompt('The default is to add the toolbox path to MATLAB, so you can use\nodeSD and odeSD_mex from the command line in your current session.\nWould you like to add the path [Y/N]? ', true);

    if addPathToMATLAB
        addpath(pwd);
        addpath([odeSDroot filesep 'SBMLconversion' filesep '']);
        addpath([odeSDroot filesep 'SBMLconversion' filesep 'odeFunctionGenerator']);
        

        printSeparator();

        savePaths = YNprompt('The default is to add the toolbox path to MATLAB permanently.\nWould you like to add the path permanently [Y/N]? ', true);

        if savePaths
            savepath;
        end
    end

    printSeparator();
    fprintf('ODESD Toolbox Version %s installed successfully \n', version);
    fprintf('Installed Solvers: %s \n\n', solvers);
    cd(currentFolder);
    
    function answer = YNprompt(question, default)
        while true
            answer = input(question,'s');
            answer = upper(answer);

            if strcmp(answer, 'Y') || (isempty(answer) && default)
                answer = true;
                break;
            elseif strcmp(answer, 'N') || (isempty(answer) && ~default)
                answer = false;
                break;
            end
        end
    end

    function printSeparator()
        fprintf('\n');
        fprintf('---------------------------------------------------------------------------\n');
    end
end