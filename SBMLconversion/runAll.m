clear all
clc

ModelNames = { 'Elowitz2000_6states.xml'}%,...
%                'LeLoup1999_10states.xml',...
%                'Wolf2001_13states.xml',...
%                'Goldbeter2008_20states.xml',...
%                'Xie2007_24states.xml',...
%                'Hornberg2005_8states.xml', ...
%                'Kholodenko1999_23states.xml', ...
%                'Singh2006_66states.xml',...
%                'Borisov2009_90states.xml',...
%                'Ung2008_200states.xml'};

           
% Ensure we are in the correct folder
cd(fileparts(mfilename('fullpath')));

if ~exist('SBMLout', 'dir')
    mkdir('SBMLout');
end
addpath([fileparts(mfilename('fullpath')) filesep 'SBMLout']);

files = {};
handles = {};
models = {};

%% Convert all models
for modelName = ModelNames
    sbmlFileName = ['allSBMLmodels/' modelName{1}];
    tic
    [currentFiles, currentHandles, currentModel] = generateRhs(sbmlFileName, {'matlabMex', 'cpp', 'c'}, 'SBMLout');
    toc
    files{end+1} = currentFiles;
    handles{end+1} = currentHandles;
    models{end+1} = currentModel;
end


%% Compile all models to hybrid models
for i = 1:length(ModelNames)
    odeSD_compile_hybrid(files{i}{end}, models{i}.name, 'SBMLout');
end

%%
save('filesAndHandles.mat', 'files', 'handles', 'models', 'ModelNames');

%%
load('filesAndHandles.mat');
%% Run all models
for i = 1:length(ModelNames)
    modelName = models{i}.name;
    
    modelHandle = handles{i}{1};
    jacHandle = handles{i}{2};
    
    mexModelHandle = handles{i}{3};
    mexOdeSDJacHandle = handles{i}{4};
    mexMatlabOdeJacHanlde = handles{i}{5};
    
    [x0, p0] = handles{i}{1}();
    x0 = x0 + 1e-100;
    nonNegative = 1:length(x0);
    
    disp(ModelNames{i});
    
    options = odeset('Jacobian', jacHandle, 'NonNegative', nonNegative);
    
    tic
    [T, X] = odeSD(modelHandle, linspace(0,100,101), x0, options, p0);
    toc
    figure;
    subplot(3,2,1);
    plot(T,X);
    title('odeSD');
    
    tic
    [T, X] = odeSD_wrapper(mexModelHandle, linspace(0,100,101), x0, options, p0);
    toc
    subplot(3,2,2);
    plot(T,X);
    title('odeSD\_mex with mex RHS');
    
    tic
    [T, X] = ode15s(modelHandle, linspace(0,100,101), x0, options, p0);
    toc
    subplot(3,2,3);
    plot(T,X);  
    title('ode15s');
    
    tic
    [T, X] = ode15s(mexModelHandle, linspace(0,100,101), x0, options, p0);
    toc
    subplot(3,2,4);
    plot(T,X);  
    title('ode15s with mex RHS');
    disp(' ');
    
    tic
    [T, X] = odeSD_wrapper_hybrid(modelName, linspace(0,100,101), x0, options, p0);
    toc
    subplot(3,2,5);
    plot(T,X);  
    title('odeSD\_hybrid');
    disp(' ');
    drawnow;
end

