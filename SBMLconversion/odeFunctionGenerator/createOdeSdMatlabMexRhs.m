function [files, handles] = createOdeSdMatlabMexRhs( symModel, targetFolder, options )
    %% Generate Matlab RHS
    [files, handles] = createOdeSdMatlabRhs( symModel, targetFolder, options );
    
    mainFunctionName = func2str(handles{1});
    jacFunctionName  = func2str(handles{2});
    
    nStates = length(symModel.stateNames);
    nParameters = length(symModel.parameterNames);
    
    if ~isempty(targetFolder)
        if targetFolder(end) ~= filesep
            targetFolder = [targetFolder filesep];
        end
        currentDir = cd(targetFolder);
    end
    %% Compile Matlab RHS
    codegen('-config:mex', mainFunctionName, '-args', {0, zeros(nStates, 1), zeros(nParameters, 1)});
    codegen('-config:mex', jacFunctionName, '-args', {0, zeros(nStates, 1), zeros(nStates, 1), zeros(nParameters, 1)} );
    codegen('-config:mex', jacFunctionName, '-args', {0, zeros(nStates, 1), zeros(nParameters, 1)}, '-o', [jacFunctionName '_matlabODE_mex']);
    
    if ~isempty(targetFolder)
        cd(currentDir);
    end
    
    
    files{end+1} = [mainFunctionName '_mex.m'];
    files{end+1} = [jacFunctionName '_mex.m'];
    files{end+1} = [jacFunctionName '_matlabODE_mex.' mexext()];
    handles{end+1} = str2func([mainFunctionName '_mex']);
    handles{end+1} = str2func([jacFunctionName '_mex']);
    handles{end+1} = str2func([jacFunctionName '_matlabODE_mex']);
end

