function odeSD_compile_hybrid(modelCFile, modelName, targetFolder)
    %% 1. Patch Model C File
    modelHybridFile = [modelName '_hybrid.c'];
    modelHybridFileRelPath = [modelName '_hybrid.c'];
    modelHybridSolver = ['odeSD_hybrid_' modelName '.c'];
    modelHybridSolverRelPath = ['odeSD_hybrid_' modelName '.c'];
    if exist('targetFolder', 'var')
        if (targetFolder(end) ~= filesep)
            targetFolder = [targetFolder filesep];
        end
        modelHybridFile = [targetFolder modelName '_hybrid.c'];
        modelHybridSolver = [targetFolder 'odeSD_hybrid_' modelName '.c'];
    end
    copyfile(modelCFile, modelHybridFile);
    
    hybridModelCode = fileread(modelHybridFile);
    hybridModelCode = strrep(hybridModelCode, '#include <clapack.h>', '#include <lapack.h>');
    hybridModelCode = strrep(hybridModelCode, '#include <cblas.h>', sprintf('#include <blas.h>\n#include <mex.h>'));
    
    hybridModelCode = regexprep(hybridModelCode, ...
        'int [^\n]+?_dsdt \([^)]+?\) \{(.+?)\}', ''); 
    
    hybridModelCode = regexprep(hybridModelCode, ...
    '/[\*]+ Initial conditions. \*/[\s]*?const int (.+?) = (.+?);[\s]*?const int (.+?) = (.+?);', ...
    '$0\nmwSignedIndex mw$1 = $2;\nmwSignedIndex mw$3 = $4;\ndouble done = 1.0;');

    % regexp(foo, 'cblas_dgemm\(.+?,.+?,.+?,[\s]*(.+?)[\s]*,[\s]*(.+?)[\s]*,[\s]*(.+?)[\s]*,[\s]*', 'tokens')
    hybridModelCode = regexprep(hybridModelCode, ...
        'cblas_dgemm\(.+?,.+?,.+?,[\s]*(.+?)[\s]*,[\s]*(.+?)[\s]*,[\s]*(.+?)[\s]*,[\s]*1.0[\s]*,[\s]*(.+?)[\s]*,[\s]*(.+?)[\s]*,[\s]*(.+?)[\s]*,[\s]*(.+?)[\s]*,[\s]*1.0[\s]*,[\s]*(.+?)[\s]*,[\s]*(.+?)[\s]*);', ...
        'dgemm("N", "N", &mw$1, &mw$2, &mw$3, &done, $4, &mw$5, $6, &mw$7, &done, $8, &mw$9);');
    
    
    fileID = fopen(modelHybridFile,'w');
    fprintf(fileID,'%s',hybridModelCode);
    fclose(fileID);
    
    
    
    %% 2. Create Custom Solver
    copyfile([fileparts(mfilename('fullpath')) filesep 'odeSD_mex_hybrid.c'], modelHybridSolver);

    % Create a custom solver for this model
    hybridSolverCode = fileread(modelHybridSolver);
    hybridSolverCode = strrep(hybridSolverCode, '#MODEL_C_FILE#', modelHybridFileRelPath);
    hybridSolverCode = strrep(hybridSolverCode, '#MODEL_F#', [modelName '_f']);
    hybridSolverCode = strrep(hybridSolverCode, '#MODEL_JAC#', [modelName '_jac']);
    
    fileID = fopen(modelHybridSolver,'w');
    fprintf(fileID,'%s',hybridSolverCode);
    fclose(fileID);
    
    if exist('targetFolder', 'var')
        currentFolder = cd(targetFolder);
        odeSD_compile(modelHybridSolverRelPath);
        cd(currentFolder);
    end
end