/*******************************************************************************
 * This file is part of odeSD.
 * Coypright (c) 2011 Pedro Gonnet (gonnet@maths.ox.ac.uk), Lukas Widmer,
 * Sotiris Dimopoulos and J�rg Stelling.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ******************************************************************************/

/* Standard headers. */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <fenv.h>

/* Local headers. */
#include "errs.h"
#include "odeSD.h"

/* Model file headers. */
#include "Elowitz2000_6states.h"

/** LW: Patch for Visual Studio */
#ifdef WIN32
#define alloca _alloca
#endif

/* Main routine. */

int main ( int argc , char *argv[] )
{
    double *T, *X, *S, *x0;
    struct odeSD_opts opts = odeSD_opts_default;
    int i, k, nr_steps;
    double tspan[2] = { 0.0 , 300 };
    int nneg[ Elowitz2000_6states_Nstates ];
    
    /* Init x0. */
    x0 = (double *)alloca( sizeof(double) * Elowitz2000_6states_Nstates );
    memcpy( x0 , Elowitz2000_6states_x0 , sizeof(double) * Elowitz2000_6states_Nstates );

    /* Set some options. */
    opts.RelTol = 1.0e-6;
    opts.AbsTol = DBL_EPSILON;
    for ( k = 0 ; k < Elowitz2000_6states_Nstates ; k++ )
        nneg[k] = k;
    opts.NonNegative = nneg;
    opts.nr_NonNegative = Elowitz2000_6states_Nstates;

    /* Call the integrator on the MODEL problem. */
    if ( ( nr_steps = odeSD( &Elowitz2000_6states_f , &Elowitz2000_6states_jac ,
        2 , tspan , Elowitz2000_6states_Nstates , x0 , &opts , 
        Elowitz2000_6states_Nparams , Elowitz2000_6states_p , NULL ,
        &T , &X , &S ) ) < 0 ) 
	{
        errs_dump(stderr);
        abort();
    }
        
    /* Print the data in gnuplot-readable format. */
    printf( "# t" );
    for ( k = 0 ; k < Elowitz2000_6states_Nstates ; k++ )
        printf( " x%02i" , k );
    printf( "\n" );
    for ( i = 0 ; i < nr_steps ; i++ ) 
	{
        printf( "%e" , T[i] );
        for ( k = 0 ; k < Elowitz2000_6states_Nstates ; k++ )
            printf( " %e" , X[ i*Elowitz2000_6states_Nstates + k ] );
        printf("\n");
    }


    /* Clean up after odeSD. */
    free( T ); free( X ); free( S );
    
    /* End on a good note. */
    return 0;
    
}