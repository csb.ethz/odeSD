function cppString = cppWriteSymVector(symVector, vectorName, nnzOnly, doAddition)
    
    if ~exist('doAddition', 'var')
        doAddition = false;
    end
    
    if doAddition
        operator = '+=';
    else
        operator = '=';
    end
    
    if nnzOnly
        indices = find(symVector)';
    else
        indices = 1:length(symVector);
    end
    cppString = cell(1, length(indices));
    index = 1;
    for i = indices
        codeString = strtrim(ccode(symVector(i)));
        codeString = [sprintf('\t\t') vectorName '[' int2str(i-1) '] ' operator ' ' codeString(6:end)];
        codeString = regexprep(codeString, 'odeSD([xpf])([\d]+?)var', '$1[${sprintf(''%i'',eval($2)-1)}]');
        cppString{index} = codeString;
        index = index + 1;
    end
    
    cppString = sprintf('%s\n', cppString{:});
end