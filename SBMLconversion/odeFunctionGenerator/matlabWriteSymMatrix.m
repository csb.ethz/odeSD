function matrixAsString = matlabWriteSymMatrix( symMatrix, matrixName, doAddition )
    indices = find(symMatrix)';

    if doAddition
        indexCell = cell(5, length(indices));
        indexCell([1 3],:) = {matrixName};
        indexCell(2,:) = arrayfun(@(x)sprintf('%i',x),indices, 'UniformOutput', false);
        indexCell(4,:) = indexCell(2,:);
        indexCell(5,:) = arrayfun(@char, symMatrix(indices), 'UniformOutput', false);

        matrixAsString = sprintf('\t%s(%s) = %s(%s) + %s;\n', indexCell{:});
    else
        indexCell = cell(3, length(indices));
        indexCell(1,:) = {matrixName};
        indexCell(2,:) = arrayfun(@(x)sprintf('%i',x),indices, 'UniformOutput', false);
        indexCell(3,:) = arrayfun(@char, symMatrix(indices), 'UniformOutput', false);

        matrixAsString = sprintf('\t%s(%s) = %s;\n', indexCell{:});
    end

end

