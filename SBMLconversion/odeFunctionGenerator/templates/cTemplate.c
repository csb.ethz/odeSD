/******************************
* This file is part of odeSD. *
******************************/ 

/* Standard header files. */ 
#include <stdlib.h> 
#include <string.h> 
#include <math.h> 

/* Library header files. */ 
#include <clapack.h> 
#include <cblas.h> 

/* Initial conditions. */ 
const int ##ModelName##_Nstates = ##nStates##; 
const int ##ModelName##_Nparams = ##nParameters##; 
const double ##ModelName##_x0[##nStates##] = {##InitialConditions##};
const double ##ModelName##_p[##nParameters##] = {##Parameters##};
const char *##ModelName##_name = "##ModelName##";

/*****************************************
* Jacobian Right-hand side for ODE model *
*****************************************/

int ##ModelName##_jac ( double ##timeVariable## , const double *x , const double *f , const double *p , void *varargin , double *J , double *dJ , double *dfdp , double *d2fdtdp ) 
{ 
	/*******************************
	* STATE JACOBIAN EQUATIONS (J) *
	*******************************/ 
	
	if ( J != NULL || dJ != NULL || d2fdtdp != NULL ) 
	{ 
		memset( J , 0.0, sizeof(double) * ##ModelName##_Nstates * ##ModelName##_Nstates );
		
##J##
	} 

	/*********************************************
	* JACOBIAN OF 2nd DERIVATIVE EQUATIONS (Jgx) *
	*********************************************/

	if ( dJ != NULL )
    { 
		memset( dJ , 0.0, sizeof(double) * ##ModelName##_Nstates * ##ModelName##_Nstates ); 
        
##dJ##

		cblas_dgemm( CblasColMajor , CblasNoTrans , CblasNoTrans , ##ModelName##_Nstates , ##ModelName##_Nstates , 
		##ModelName##_Nstates , 1.0, J , 
		##ModelName##_Nstates , J , ##ModelName##_Nstates , 
		1.0 , dJ , ##ModelName##_Nstates );  
	 } 


    /************************************** 
    * PARAMETER JACOBIAN EQUATIONS (dfdp) * 
    **************************************/

    if ( dfdp != NULL || d2fdtdp != NULL )
    { 
        memset( dfdp , 0.0, sizeof(double) * ##ModelName##_Nstates * ##ModelName##_Nparams ); 
##dfdp##
    } 

    /****************************************************
    * 2nd derivative PARAMETER JACOBIAN EQUATIONS (Jgp) *
    ****************************************************/ 

    if ( d2fdtdp != NULL )
    { 
        memset( d2fdtdp , 0.0, sizeof(double) * ##ModelName##_Nstates * ##ModelName##_Nparams ); 

##d2fdtdp##

        cblas_dgemm( CblasColMajor , CblasNoTrans , CblasNoTrans , ##ModelName##_Nstates , ##ModelName##_Nparams , 
        ##ModelName##_Nstates , 1.0, J , 
        ##ModelName##_Nstates , dfdp , ##ModelName##_Nstates , 
        1.0 , d2fdtdp , ##ModelName##_Nstates );  
    } 


 /* All is well that ends well... */ 
 return 0; 

} 


/********************************
* Right-hand side for ODE model *
********************************/

int ##ModelName##_f ( double ##timeVariable## , const double *x , const double *p , void *varargin , double *f , double *dfdt , double *J , double *dJ , double *dfdp , double *d2fdtdp )
{ 
	int res = 0;

	/*******************************
	* 1st DERIVATIVE EQUATIONS (f) *
	*******************************/

##f##

	/***************************************
	* 2nd DERIVATIVE EQUATIONS (Jfx*f = g) *  
	****************************************/

	if ( dfdt != NULL ) 
	{ 
##dfdt##
	} 

	/* Jacobians and stuff. */ 
	if ( ( res = ##ModelName##_jac(##timeVariable## , x , f , p , varargin , J , dJ , dfdp , d2fdtdp ) ) < 0 ) 
	return res; 

	/* All is well... */ 
	return 0; 
 } 

