function [f,g,Jfx,Jgx,Jfp,Jgp] = ##odeFunction##( varargin )
    additionalArgs = {};

    if nargin<1
        f = ##InitialConditions##
        g = ##Parameters##
        return
    elseif nargin==2 
        ##timeVariable## = varargin{1};
        x = varargin{2};
        p = ##Parameters##
    else
        ##timeVariable## = varargin{1};
        x = varargin{2};
        p = varargin{3};
        if nargin > 3
            ##inputVariables##
        end
    end 

    f = ##fValues##

    if nargout<2 
        return
    end

    g = ##gValues##

    if nargout<3 
         return 
    elseif nargout==3 
         [Jfx] = ##jacFunction##(##timeVariable##, x, f, p, additionalArgs{:});
    elseif nargout==4 
         [Jfx,Jgx] = ##jacFunction##(##timeVariable##, x, f, p, additionalArgs{:});
    elseif nargout==5 
          [Jfx,Jgx,Jfp] = ##jacFunction##(##timeVariable##, x, f, p, additionalArgs{:});
    elseif nargout==6 
          [Jfx,Jgx,Jfp,Jgp] = ##jacFunction##(##timeVariable##, x, f, p, additionalArgs{:});
    elseif nargout>6 
         error('Too many output arguments');
    end
end
