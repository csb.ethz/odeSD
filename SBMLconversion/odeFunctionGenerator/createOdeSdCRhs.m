function [files, handles] = createOdeSdCRhs( symModel, targetFolder, options )
    %% Write Files
    syms odeSDtimeVariable real;
    
    initialStateString = sprintf('%#.0g, ', symModel.initStateVals);
    initialStateString = initialStateString(1:end-2);
    parameterString = sprintf('%#.0g, ', symModel.parameterValues);
    parameterString = parameterString(1:end-2);
    
    fString = cppWriteSymVector(vpa(symModel.RHS), 'f', false);
    gString = cppWriteSymVector(vpa(symModel.dRHSdt), 'dfdt', false);
    
    dfdxVector = cppWriteSymVector(vpa(symModel.dRHSdx(:)), 'J', true);
    dfdpVector = cppWriteSymVector(vpa(symModel.dRHSdp(:)), 'dfdp', true);    
    dfPrimedxVector = cppWriteSymVector(vpa(symModel.d2RHSdtdx(:)), 'dJ', true, true);
    dfPrimedpVector = cppWriteSymVector(vpa(symModel.d2RHSdtdp(:)), 'd2fdtdp', true, true);    
    
    if isempty(targetFolder)
        modelFile = [symModel.name '.c'];
    else
        if (targetFolder(end) ~= filesep)
            targetFolder = [targetFolder filesep];
        end
        modelFile = [targetFolder symModel.name '.c'];
    end
    
    templateRoot = [fileparts(mfilename('fullpath')) filesep 'templates' filesep];
    copyfile([templateRoot 'cTemplate.c'], modelFile);
    odeCode = fileread(modelFile);
    odeCode = strrep(odeCode, '##ModelName##', symModel.name);
    odeCode = strrep(odeCode, '##nStates##', int2str(length(symModel.initStateVals)));
    odeCode = strrep(odeCode, '##nParameters##', int2str(length(symModel.parameterValues)));
odeCode = strrep(odeCode, '##timeVariable##', char(symModel.timeVariable));
        
    odeCode = strrep(odeCode, '##InitialConditions##', initialStateString);
    odeCode = strrep(odeCode, '##Parameters##', parameterString);
    odeCode = strrep(odeCode, '##f##', fString);
    odeCode = strrep(odeCode, '##dfdt##', gString);
        
    odeCode = strrep(odeCode, '##J##', dfdxVector);
    odeCode = strrep(odeCode, '##dJ##', dfPrimedxVector);
    odeCode = strrep(odeCode, '##dfdp##', dfdpVector);
    odeCode = strrep(odeCode, '##d2fdtdp##', dfPrimedpVector);
    
    fileID = fopen(modelFile,'w');
    fprintf(fileID,'%s',odeCode);
    fclose(fileID);
    
    files = {modelFile};
    handles = {};    
end

