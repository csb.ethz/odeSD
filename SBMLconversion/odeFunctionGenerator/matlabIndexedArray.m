function symArray = matlabIndexedArray( arrayName, arraySize )
    symArray = sym(zeros(arraySize,1));
    syms([arrayName '(ind)']);
    
    for i = 1:arraySize
        symArray(i) = eval([arrayName '(' int2str(i) ')']);
    end
end

