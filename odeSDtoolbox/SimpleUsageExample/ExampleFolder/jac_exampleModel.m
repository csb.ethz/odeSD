function [Jfx,Jgx,Jfp,Jgp] = jac_exampleModel(varargin)

    time = varargin{1};
    x = varargin{2};

    if nargout > 1
        % odeSD
        if nargin > 3
            p = varargin{4};
            
        else
            p = [1;1];
        end
        if nargout > 1
            f = varargin{3};
        end
    else
        % MATLAB ODE integrators
        if nargin > 2
            p = varargin{3};
            
        else
            p = [1;1];
        end
    end


    if nargout<1
        error('Too many output arguments');
    end

    Jfx = zeros(2, 2);
	Jfx(1) = -p(1)*x(2);
	Jfx(2) = -p(2)*x(2);
	Jfx(3) = -p(1)*x(1);
	Jfx(4) = -p(2)*x(1);


    if nargout == 1
        return
    end

    Jgx = Jfx*Jfx;
	Jgx(1) = Jgx(1) + -f(2)*p(1);
	Jgx(2) = Jgx(2) + -f(2)*p(2);
	Jgx(3) = Jgx(3) + -f(1)*p(1);
	Jgx(4) = Jgx(4) + -f(1)*p(2);


    if nargout == 2
        return
    end

    Jfp = zeros(2, 2);
	Jfp(1) = -x(1)*x(2);
	Jfp(4) = -x(1)*x(2);


    if nargout == 3
        return
    end

    Jgp = Jfx*Jfp;
	Jgp(1) = Jgp(1) + - f(1)*x(2) - f(2)*x(1);
	Jgp(4) = Jgp(4) + - f(1)*x(2) - f(2)*x(1);


end

