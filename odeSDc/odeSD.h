/*******************************************************************************
 * This file is part of odeSD.
 * Coypright (c) 2011 Pedro Gonnet (gonnet@maths.ox.ac.uk), Lukas Widmer,
 * Sotiris Dimopoulos and J�rg Stelling.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ******************************************************************************/


/** Option flags. */
#define odeSD_flag_NormControl          1
#define odeSD_flag_Stats                2

/** Structure for odeSD options. */
struct odeSD_opts 
{
    /** Relative tolerance. */
    double RelTol;
    
    /** Absolute tolerance. */
    double AbsTol;
    
    /** Binary flags. */
    unsigned int flags;
    
    /** Number of non-negative entries. */
    int nr_NonNegative;
    
    /** Indices of the non-negative entries. */
    int *NonNegative;
    
    /** Output function. */
    int (*OutputFcn)( double t , int N , double *y );
    
    /** Which values to send to the output function. */
    int nr_OutputSel;
    int *OutputSel;
    
    /** Refinement level for each step. */
    int Refine;
    
    /** Initial step. */
    double InitialStep;
    
    /** Largest permissible step. */
    double MaxStep;
};
    
/** Default integrator options. */
extern const struct odeSD_opts odeSD_opts_default;

/** Function headers. */
int odeSD (
    int (*f)( double t , const double *x , const double *p , void *varargin , double *f , double *dfdt , double *J , double *dJ , double *dfdp , double *d2fdtdp ),
    int (*dfdx)( double t , const double *x , const double *f , const double *p , void *varargin , double *J , double *dJ , double *dfdp , double *d2fdtdp ),
    int nr_tspan, double *tspan,
    int N, const double *x0,
    struct odeSD_opts *opts,
    int nr_params, const double *params,
    void *varargin,
    double **t_out,
    double **x_out,
    double **s_out );

    
