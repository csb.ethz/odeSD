/* 
	Replace #MODEL_C_FILE# with the .c file for the model
	Replace #MODEL_F# with the RHS function
	Replace #MODEL_JAC# with the Jacobian function
*/

/* Header files */
#include <math.h>
#include <string.h>
/* Modifications for 64bit platforms */
#include <mex.h>
#include <blas.h>
#include <lapack.h>

#include "#MODEL_C_FILE#"

void mexFunction ( int nlhs , mxArray *plhs[] , int nrhs , const mxArray *prhs[] ) 
{

    /* Matlab calling sequence:
    
        function [ T , Y ] = odeSD_mex_hybrid( tspan , x0 , h0 , MaxStep , RelTol , AbsTol , NormControl , NonNegative , params )
        
    */
    
	int(*odeFunc)(double t, const double *x, const double *p, void *varargin, double *f, double *dfdt, double *J, double *dJ, double *dfdp, double *d2fdtdp) = &#MODEL_F#; 
	int(*jacFunc)(double t, const double *x, const double *f, const double *p, void *varargin, double *J, double *dJ, double *dfdp, double *d2fdtdp) = &#MODEL_JAC#;


    /* Constants */
    const int max_iter = 5;
    ptrdiff_t one = 1;
    double done = 1.0;
    double dmone = -1.0;
    
    
    /* Local variables */
        double h_max, h_LU = 0, rtol, atol, tol;
    mwSignedIndex i, j, k, offset, norm_control, nr_nonneg, nr_tspan, tid, ind;
	int* nonneg = NULL;
    mwSignedIndex iter, recompJ = 1, recompLU, stepped, nrsteps = 0, nrpoints = 0;
    mwSignedIndex nvx = 0, nrtees, doparams = 0;
	/* memory-dependent types */
	mwSignedIndex n, *p = NULL, nparam = 0, *iptr = NULL;
    mwSignedIndex pdims[3];
	mwSignedIndex info;

	double t, *tspan = NULL, *x = NULL, *x_new = NULL, *x_last = NULL, h, h2, h3, h4, h_last, scale = 1.0, scale_new;
    double *err = NULL, nerr, *relwt = NULL, relerr, y_max, *delta = NULL, ndelta, ndelta_old, r, rpow;
    double *J_f = NULL, *J_Jf = NULL, *ecoeffs = NULL, *f = NULL, *f_new = NULL, *f_last = NULL, *dfdt = NULL, *dfdt_new = NULL, *M = NULL;
    double t5, t17, t2, t4, t18, t22, t21, t19, t16, t15, t6, t11, t7, t12, t14, t13;
    double t3, t9, t8, t10, t1, t23, t27, t33, t36, t42;
    double w_f, w_dfdt, w_f_new, w_dfdt_new, w_f_last, temp, *dptr = NULL;
    double *vx = NULL, alpha[5];
	double *s = NULL, *dsdt = NULL, *d2sdt2 = NULL, *s_new = NULL, *dsdt_new = NULL, *d2sdt2_new = NULL, *dfdp = NULL, *d2fdtdp = NULL, *perr = NULL, *dsdt_last = NULL;
	double* params = NULL;
	mxArray *mx0 = NULL;
	mxArray *mparams = NULL;
    mxArray *mX = NULL, *mT = NULL, *mS = NULL;
    
    
    /* Get the time span. */
    if ( mxGetClassID( prhs[0] ) != mxDOUBLE_CLASS || mxGetN( prhs[0] ) != 1 || mxGetM( prhs[0] ) < 2 )
        mexErrMsgTxt("First argument t_span must be a column vector of length >= 2.");
    nr_tspan = mxGetM( prhs[0] );
    tspan = mxGetPr( prhs[0] );
    tid = 1;
	t = tspan[0];
    
    

    /* Extract the initial condition x0. */
    if ( mxGetClassID( prhs[1] ) != mxDOUBLE_CLASS || mxGetN( prhs[1] ) != 1 )
        mexErrMsgTxt("Second argument x0 must be a column vector of type double.");
    /* Get length of x0. */
 	/* Modifications for 64bit platforms */
    n = mxGetM( prhs[1] );

    /* Allocate and fill x. */
    if ( ( x = mxCalloc( sizeof(double) , n ) ) == NULL ) 
        mexErrMsgTxt("Error allocating memory for x.");
    mx0 = prhs[1]; dptr = mxGetPr( mx0 );
    for ( i = 0 ; i < n ; i++ )
        x[i] = dptr[i];

    
    /* Extract the initial step. */
    if ( !mxIsScalar( prhs[2] ) || mxGetClassID( prhs[2] ) != mxDOUBLE_CLASS )
        mexErrMsgTxt("Third argument h0 must be a double scalar.");
    h = mxGetScalar( prhs[2] );
    
    
    /* Extract the maximum step size. */
    if ( !mxIsScalar( prhs[3] ) || mxGetClassID( prhs[3] ) != mxDOUBLE_CLASS )
        mexErrMsgTxt("Fourth argument h_max must be a double scalar.");
    h_max = mxGetScalar( prhs[3] );
    
    
    /* Extract the relative and absloute tolerance. */
    if ( !mxIsScalar( prhs[4] ) || mxGetClassID( prhs[4] ) != mxDOUBLE_CLASS )
        mexErrMsgTxt("Fifth argument rtol must be a double scalar.");
    rtol = mxGetScalar( prhs[4] );
    if ( !mxIsScalar( prhs[5] ) || mxGetClassID( prhs[5] ) != mxDOUBLE_CLASS )
        mexErrMsgTxt("Sixth argument atol must be a double scalar.");
    atol = mxGetScalar( prhs[5] );
       
    
    /* Get NormControl bit. */
    if ( !mxIsLogicalScalar( prhs[6] ) )
        mexErrMsgTxt("Seventh argument NormControl must be a logical scalar.");
    norm_control = *mxGetLogicals( prhs[6] );
    
    
    /* Get NonNegative array. */
    if ( !mxIsInt32( prhs[7] ) || mxGetN( prhs[7] ) != 1 )
        mexErrMsgTxt("Tenth argument NonNegative must be a column vector of int32.");
    nr_nonneg = mxGetM( prhs[7] );
    nonneg = (int *)mxGetData( prhs[7] );
    
	/* Determine if we should compute sensitivities */
    doparams = (nlhs == 3);
	
	int paramsArg = 8 + doparams;
	
    /* Get params (if there). */
    if ( nrhs > paramsArg ) 
    {
        if ( mxGetClassID( prhs[paramsArg] ) != mxDOUBLE_CLASS || mxGetN( prhs[paramsArg] ) != 1 )
            mexErrMsgTxt("Eleventh argument params must be a column vector of doubles.");
        mparams = prhs[paramsArg];
        nparam = mxGetM( mparams );
		params = mxGetPr(mparams);
    }
    
     
    /* Initialize the output matrices. */
    if ( nlhs < 2 || nlhs > 3 )
        mexErrMsgTxt("At least two output arguments, T and X, are required, along with an additional argument S if sensitivity analysis is requested.");
    if ( doparams ) 
	{
        if ( nparam == 0 )
            mexErrMsgTxt("A vector of parameters (9th argument) must be specified for sensitivity analysis.");
        if ( ( s = (double *)mxCalloc( sizeof(double) , nparam * n ) ) == NULL )
            mexErrMsgTxt("Failed to allocate the s matrix.");
		
		if ( mxGetClassID( prhs[8] ) != mxDOUBLE_CLASS || mxGetM( prhs[8] ) != n || mxGetN( prhs[8] ) != nparam )
			mexErrMsgTxt("Eleventh argument must be the initial sensitivities, a double matrix of of size n x nparams.");
		
		dptr = mxGetPr( prhs[8] );
		for ( i = 0 ; i < n*nparam ; i++ )
			s[i] = dptr[i];
		
        if ( ( s_new = (double *)mxCalloc( sizeof(double) , nparam * n ) ) == NULL )
            mexErrMsgTxt("Failed to allocate the s_new matrix.");
        if ( ( dsdt_new = (double *)mxCalloc( sizeof(double) , nparam * n ) ) == NULL )
            mexErrMsgTxt("Failed to allocate the dsdt_new matrix.");
        if ( ( d2sdt2_new = (double *)mxCalloc( sizeof(double) , nparam * n ) ) == NULL )
            mexErrMsgTxt("Failed to allocate the d2sdt2_new matrix.");
        if ( ( perr = (double *)mxCalloc( sizeof(double) , nparam * n ) ) == NULL )
            mexErrMsgTxt("Failed to allocate the perr matrix.");
        if ( ( dsdt_last = (double *)mxCalloc( sizeof(double) , nparam * n ) ) == NULL )
            mexErrMsgTxt("Failed to allocate the dsdt_last matrix.");
    }

    if ( nr_tspan == 2 ) 
	{
        if ( ( mT = mxCreateDoubleMatrix( 1 , 100 , mxREAL ) ) == NULL )
            mexErrMsgTxt("Error allocating mT.");
        if ( ( mX = mxCreateDoubleMatrix( n , 100 , mxREAL ) ) == NULL )
            mexErrMsgTxt("Error allocating mX.");
        if ( nparam > 0 ) 
		{
            pdims[0] = n; pdims[1] = nparam; pdims[2] = 100;
            if ( ( mS = mxCreateNumericArray( 3 , pdims , mxDOUBLE_CLASS , mxREAL ) ) == NULL )
                mexErrMsgTxt("Allocation of mS failed.");
            plhs[2] = mS;
        }
    }
    else 
	{
        if ( ( mT = mxCreateDoubleMatrix( 1 , nr_tspan , mxREAL ) ) == NULL )
            mexErrMsgTxt("Error allocating mT.");
        if ( ( mX = mxCreateDoubleMatrix( n , nr_tspan , mxREAL ) ) == NULL )
            mexErrMsgTxt("Error allocating mX.");
        if ( nparam > 0 ) 
		{
            pdims[0] = n; pdims[1] = nparam; pdims[2] = nr_tspan;
            if ( ( mS = mxCreateNumericArray( 3 , pdims , mxDOUBLE_CLASS , mxREAL ) ) == NULL )
                mexErrMsgTxt("Allocation of mS failed.");
            plhs[2] = mS;
        }
    }

    plhs[0] = mT; plhs[1] = mX;
    dptr = mxGetPr( mT ); dptr[0] = t;
    dptr = mxGetPr( mX );
    for ( i = 0 ; i < n ; i++ )
        dptr[i] = x[i];
    
    /* Allocate the iteration matrix, x_new and err. */
    if ( ( M = (double *)mxCalloc( sizeof(double) , n * n ) ) == NULL )
        mexErrMsgTxt("Error allocating the iteration matrix.");
	if ((x_new = (double *)mxCalloc(sizeof(double), n)) == NULL)
        mexErrMsgTxt("Error creating x_new.");
    if ( ( err = (double *)mxCalloc( sizeof(double) , n ) ) == NULL )
        mexErrMsgTxt("Error allocating err vector.");
    if ( ( relwt = (double *)mxCalloc( sizeof(double) , n ) ) == NULL )
        mexErrMsgTxt("Error allocating relwt vector.");
    if ( ( x_last = (double *)mxCalloc( sizeof(double) , n ) ) == NULL )
        mexErrMsgTxt("Error allocating x_last vector.");
    if ( ( f_last = (double *)mxCalloc( sizeof(double) , n ) ) == NULL )
        mexErrMsgTxt("Error allocating f_last vector.");
    if ( ( p = (size_t *)mxCalloc( sizeof(size_t) , n ) ) == NULL )
        mexErrMsgTxt("Error allocating permutation vector.");
    if ( ( delta = (double *)mxCalloc( sizeof(double) , n ) ) == NULL )
        mexErrMsgTxt("Error allocating delta vector.");
    
	if ((f = (double *)mxCalloc(sizeof(double), n)) == NULL)
		mexErrMsgTxt("Error allocating f vector.");
	if ((dfdt = (double *)mxCalloc(sizeof(double), n)) == NULL)
		mexErrMsgTxt("Error allocating dfdt vector.");
	if ((J_f = (double *)mxCalloc(sizeof(double), n * n)) == NULL)
		mexErrMsgTxt("Error allocating dfdt vector.");
	if ((J_Jf = (double *)mxCalloc(sizeof(double), n * n)) == NULL)
		mexErrMsgTxt("Error allocating dfdt vector.");
	if ((dsdt = (double *)mxCalloc(sizeof(double), nparam * n)) == NULL)
		mexErrMsgTxt("Error allocating dfdt vector.");
	if ((d2sdt2 = (double *)mxCalloc(sizeof(double), nparam * n)) == NULL)
		mexErrMsgTxt("Error allocating dfdt vector.");

	if ((f_new = (double *)mxCalloc(sizeof(double), n)) == NULL)
		mexErrMsgTxt("Error allocating f vector.");
	if ((dfdt_new = (double *)mxCalloc(sizeof(double), n)) == NULL)
		mexErrMsgTxt("Error allocating dfdt vector.");

	if ((dfdp = (double *)mxCalloc(sizeof(double), nparam * n)) == NULL)
		mexErrMsgTxt("Error allocating f vector.");
	if ((d2fdtdp = (double *)mxCalloc(sizeof(double), nparam * n)) == NULL)
		mexErrMsgTxt("Error allocating dfdt vector.");

	/* Call the RHS once to get an initial guess for the first step. */
	if (odeFunc(t, x, params, NULL, f, dfdt, J_f, J_Jf, dsdt, d2sdt2) < 0)
		mexErrMsgTxt("Error calling the right-hand side 'f'.");

	if (doparams)
	{
		dptr = mxGetPr(mS);
		for (i = 0; i < n * nparam; i++)
			dptr[i] = s[i];
	}

    recompJ = 0;
    
    
    /* Allocate and construct the first set of extrapolation coefficients. */
    if ( ( ecoeffs = (double *)mxCalloc( sizeof(double) , n * 4 ) ) == NULL )
        mexErrMsgTxt("Error allocating memory for extrapolation coefficients.");
    h2 = h * h;
    for ( i = 0 ; i < n ; i++ ) 
	{
        ecoeffs[i] = dfdt[i] * h2 * 0.5 - h * f[i] + x[i];
        ecoeffs[i+n] = ( f[i] - dfdt[i] * h ) * h;
        ecoeffs[i+2*n] = dfdt[i] * h2 * 0.5;
        ecoeffs[i+3*n] = 0.0;
    }
        
     
    /* Reduce the step size h until the first step is sane. */
    if ( nr_nonneg > 0 ) 
	{
    
        /* loop... */
        while ( 1 ) 
		{
        
            /* Evaluate x_new. */
            temp = 1.0 + scale;
            for ( i = 0 ; i < n ; i++ )
                x_new[i] = ecoeffs[i] + temp * (ecoeffs[i+n] + temp * (ecoeffs[i+2*n] + temp * ecoeffs[i+3*n]));
        
            /* Check for negative values. */
            for ( i = 0 ; i < nr_nonneg ; i++ )
                if ( x_new[ nonneg[i] - 1 ] < 0.0 )
                    break;
                    
            /* If the above loop ran clean, just break. */
            if ( i == nr_nonneg )
                break;
                
            /* Otherwise, scale the step and start over. */
            else 
			{
                h *= 0.7;
                h2 = h * h;
                scale *= 0.7;
            }
                
            /* If h is too small, bail with an error. */
            if ( h <  2.220446049250313e-16 * t || h <  2.220446049250313e-16 * (tspan[nr_tspan-1] - t) )
                mexErrMsgTxt("Minimal stepsize reached, aborting!");
                
        }
    
    }
    
     
    /* Main loop. */
    while ( t < tspan[nr_tspan-1] )
	{ 
    
    
        /* Truncate this step? */
        if ( t + h > tspan[nr_tspan-1] )
		{
            scale *= ( tspan[nr_tspan-1] - t ) / h;
            h = tspan[nr_tspan-1] - t;
            h2 = h * h;
        }
        if ( h_max > 0 && h > h_max ) 
		{
            scale *= h_max / h;
            h = h_max;
            h2 = h * h;
        }
            
        
        /* Extrapolate the new step. */
        temp = 1.0 + scale;
        for ( i = 0 ; i < n ; i++ )
            x_new[i] = ecoeffs[i] + temp * (ecoeffs[i+n] + temp * (ecoeffs[i+2*n] + temp * ecoeffs[i+3*n]));
		/* Check for negative values. */
		for (i = 0; i < nr_nonneg; i++)
			if (x_new[nonneg[i] - 1] < 0.0)
				x_new[nonneg[i] - 1] = 0.0;
        
        /* Initialize the Newton iteration. */
        iter = 1;
        recompLU = (h != h_LU);
        y_max = fabs( x_new[0] );
        for ( i = 1 ; i < n ; i++ )
            if ( y_max < fabs( x_new[i] ) )
                y_max = fabs( x_new[i] );
        tol = y_max *  2.220446049250313e-16 * n * 10.0;
        
        
        /* Newton iteration. */
        while ( 1 ) 
		{        
            /* Check for smallest-possible step size. */
            if ( h <  2.220446049250313e-16 * t || h <  2.220446049250313e-16 * (tspan[nr_tspan-1] - t) )
                mexErrMsgTxt("Minimal stepsize reached, aborting!");
                
            /* Get f_new and dfdt_new. */
			if (odeFunc(t + h, x_new, params, NULL, f_new, dfdt_new, NULL, NULL, NULL, NULL) < 0)
				mexErrMsgTxt("Error evaluating f.");

            /* Compute the error vector and the error norm. */
            relerr = 0.0;
            for ( i = 0 ; i < n ; i++ ) 
			{
                err[i] = x[i] + h * 0.5 * ( f[i] + f_new[i] ) + h2 * 8.33333333333333333e-02 * ( dfdt[i] - dfdt_new[i] ) - x_new[i];
                if ( ( tol > fabs( x[i] ) ) && ( tol > fabs( x_new[i] ) ) )
                    relwt[i] = 1.0 / tol;
                else if ( fabs( x[i] ) > fabs( x_new[i] ) )
                    relwt[i] = 1.0 / fabs( x[i] );
                else
                    relwt[i] = 1.0 / fabs( x_new[i] );
                if ( relerr < fabs( err[i] * relwt[i] ) )
                    relerr = fabs( err[i] * relwt[i] );
            }
            
            
            /* Check for convergence. */
            if ( relerr <  2.220446049250313e-16 * 10.0 )
			{
                /* mexPrintf("odeSD_mex: step with t=%e, h=%e, relerr=%e, iter=%i\n",(*t),h,relerr,iter); */
                break;
            }
                
            
            /* Do one Newton step, re-computing the Jacobian and/or
                LU-decomposition if necessary. */
            stepped = 0;
            while ( 1 ) 
			{
                /* Recompute Jacobians and LU-decomposition? */
                if ( iter == 1 && ( recompJ || recompLU ) ) 
				{
                    /* Get the Jacobians if necessary. */
                    if ( recompJ ) 
					{
						/* mexPrintf("odeSD_mex: re-computing jacobian at t=%e, h=%e\n",(*t),h); */
						if (jacFunc(t + h, x_new, f_new, params, NULL, J_f, J_Jf, NULL, NULL) < 0)
							mexErrMsgTxt("Evaluation of Jacobian function failed.");
					}

                    /* Assemble the iteration matrix. */
                    for ( i = 0 ; i < n ; i++ ) 
					{
                        offset = i*n;
                        for ( j = 0 ; j < n ; j++ )
                            M[offset+j] = h * 0.5 * J_f[offset+j] - h2 * 8.33333333333333333e-02 * J_Jf[offset+j];
                        M[offset+i] -= 1.0;
                    }
                        
                    /* Decompose the iteration matrix. */                 
                    dgetrf( &n , &n , M , &n , p , &info );
                    
                    if ( info < 0 )
                        mexErrMsgTxt("Call to DGETRF failed.");
                    if ( info > 0 )
                        for ( i = 0 ; i < n ; i++ )
                            if ( M[i*n+i] == 0.0 )
                                M[i*n+i] = 1.0e-10;
                    
                    /* Set some flags and counters. */
                    h_LU = h;
                        
                }
                    
                    
                /* Compute a step. */
                for ( i = 0 ; i < n ; i++ )
                    delta[i] = err[i];
				
                dgetrs( "N" , &n , &one , M , &n , p , delta , &n , &info );
				
                if ( info )
                    mexErrMsgTxt("Call to DGETRS failed.");
				
                ndelta = fabs( delta[0] * relwt[0] );
                for ( i = 1 ; i < n ; i++ )
                    if ( ndelta < fabs( delta[i] * relwt[i] ) )
                        ndelta = fabs( delta[i] * relwt[i] );
                        
                        
                /* Approximate the convergence rate. */
                if ( iter > 1 )
                    r = ndelta / ndelta_old;
                else
                    r = 1.0;
                
                
                /* Check for slow convergence. */
                for ( rpow = r, i = 1 ; i < max_iter - iter ; i++ )
                    rpow *= r;
                if ( ( iter > 1 && ( ndelta > 0.9*ndelta_old || ndelta * rpow > 0.5 * rtol ) ) || ( iter > max_iter ) ) 
				{
                    /* mexPrintf("odeSD_mex: slow convergence at t=%e, h=%e, r=%e, ||delta||=%e, iter=%i\n",(*t),h,r,ndelta,iter); */
                    iter = 1;
                    if ( recompJ ) {
                        h *= 0.7; h2 = h * h;
                        scale *= 0.7;
                        temp = 1.0 + scale;
                        for ( i = 0 ; i < n ; i++ )
                            x_new[i] = ecoeffs[i] + temp * (ecoeffs[i+n] + temp * (ecoeffs[i+2*n] + temp * ecoeffs[i+3*n]));
						for (i = 0; i < nr_nonneg; i++) /* LW Patch: Check for non-negativity */
							if (x_new[nonneg[i] - 1] < 0.0)
								x_new[nonneg[i] - 1] = 0.0;
                        break;
                    }
                    else 
					{
                        recompJ = 1;
                        continue;
                    }
                }
                    
                /* Accept the step. */
                for ( i = 0 ; i < n ; i++ )
                    x_new[i] -= delta[i];
                
                /* LW Patch: Check for non-negativity */
                            /* Check for negative values. */
                for ( i = 0 ; i < nr_nonneg ; i++ )
                    if ( x_new[ nonneg[i] - 1 ] < 0.0 )
                        x_new[ nonneg[i] - 1 ] = 0.0;
                
                stepped = 1;
                break;
            
            
            } /* Take a single Newton step. */
                
                
            /* Check if step was taken. */
            if ( stepped ) 
			{
                /* Have we converged? */
                if ( ( iter > 1 && r / (1.0 - r) * ndelta < 0.5 * rtol ) || ( iter == 1 && ndelta < 0.5 * rtol ) ) 
				{
                    /* mexPrintf("odeSD_mex: newton iteration converged at t=%e, h=%e (ndelta=%e, relerr=%e, r=%e ,iter=%i)\n",(*t),h,ndelta,relerr,r,iter); */
                    /* Fix-up the derivatives. */
                    dgemv( "N" , &n , &n , &dmone , J_f , &n , delta , &one , &done , f_new , &one );
                    dgemv( "N" , &n , &n , &dmone , J_Jf , &n , delta , &one , &done , dfdt_new , &one );
                    break;
                }

                /* Patch-up some things before we go on. */
                ndelta_old = ndelta;
                iter++;
            }
        } /* Newton iteration. */
            
            
        /* Reset the iteration flags. */
        recompJ = 0;
        
        
        /* Go through the non-negative array and set bad values to 0. */
        for ( i = 0 ; i < nr_nonneg ; i++ )
            if ( x_new[ nonneg[i] - 1 ] < 0.0 )
                x_new[ nonneg[i] - 1 ] = 0.0;
                
                
        /* Select the correct error estimator. */
        if ( nrsteps == 0 ) 
		{ 
            h4 = h2 * h2;
            t9 = 3.0/256.0*h;
            t8 = h2/512.0;
            for ( i = 0 ; i < n ; i++ ) 
			{
                t11 = (f_new[i]+f[i])*h;
                t10 = (f[i]-f_new[i])*h;
                t7 = (dfdt[i]+dfdt_new[i])*h2;
                t6 = (dfdt[i]-dfdt_new[i])*h2;
                t5 = f_new[i]*t9;
                t4 = dfdt_new[i]*t8;
                t2 = f[i]*t9;
                t1 = dfdt[i]*t8;
                t19 = fabs(t2-t5+3.0/512.0*t7);
                t23 = fabs(t2-t4+t5+t1-3.0/128.0*x_new[i]+3.0/128.0*x[i]);
                t27 = fabs(t10/64.0+t7/128.0);
                t33 = fabs(9.0/512.0*t11-9.0/256.0*x_new[i]+9.0/256.0*x[i]+3.0/1024.0*t6);
                t36 = fabs(t1+t4+t10/256.0);
                err[i] = t19+t23+t27+t33+t36;
            }
        }
        else 
		{
            t5 = h_last * h_last;
            t17 = 1.0 / ((t5+h2+2*h*h_last)*t5);
            t2 = t5 * t5;
            t4 = t5*h_last;
            t18 = h2*t5;
            t22 = 0.4*t18+1.3333333333333333e+00*h*t4+0.5*t2;
            h4 = h2 * h2;
            t21 = h*t2;
            t19 = -0.2*h2;
            t16 = h2*t4;
            h3 = h*h2;
            t15 = t5*h3;
            w_f = (h+((h3-2*t4)*t19+(1.6666666666666666e-01*h4-t22)*h)*t17);
            w_dfdt = (0.5*h2+((-h3*h_last-2*t18)*t19+(-0.8*t16-0.75*t15-1.6666666666666666e-01*h4*h_last-4.1666666666666666e-01*t21)*h)*t17);
            w_f_new = ((-0.4*t16+t22*h)*t17);
            w_dfdt_new = ((0.2*h4*t5+(-1.3333333333333333e-01*t16-0.25*t15-8.3333333333333333e-02*t21)*h)*t17);
            w_f_last = (3.3333333333333333e-02*h3*h2*t17);
            for ( i = 0 ; i < n ; i++ )
                err[i] = -x_new[i] + x[i] + w_f*f[i] + w_dfdt*dfdt[i] + w_f_new*f_new[i] + w_dfdt_new*dfdt_new[i] + w_f_last*f_last[i];
            dgetrs( "N" , &n , &one , M , &n , p , err , &n , &info );
            if ( info )
                mexErrMsgTxt("Call to DGETRS in error estimate failed.");
        }
            
            
        /* Compute scaling for next step. */
        if ( norm_control ) 
		{
            for ( nerr = 0.0, tol = 0.0, i = 0 ; i < n ; i++ )
			{
				tol  += x_new[i] * x_new[i];
				nerr += err[i] * err[i];
			}
			nerr = sqrt( nerr );
                
            tol = rtol * sqrt( tol );
            if ( tol < atol )
                tol = atol;
            scale_new = pow( 0.5 * tol / nerr , 0.2 );
            if ( scale_new > 2.0 )
                scale_new = 2.0;
        }
        else 
		{			
            scale_new = 2.0;
            for ( i = 0 ; i < n ; i++ ) 
			{
                tol = rtol * fabs( x_new[i] );
                if ( tol < atol )
                    tol = atol;
                temp = pow( 0.5 * tol / fabs(err[i]) , 0.2 );
                if ( temp < scale_new )
                    scale_new = temp;
            }
        }
            
            
        /* Did the error test fail? */
        if ( scale_new < 8.705505632961241e-01 ) 
		{
            /* mexPrintf("odeSD_mex: error test failed on vars at t=%e, h=%e, scale=%e\n",(*t),h,scale_new); */
            if ( scale_new < 0.1 )
            {
                scale_new = 0.1;
                recompJ = true;
            }
            else if ( scale_new > 0.8 )
                scale_new = 0.8;
            h *= scale_new;
            h2 = h * h;
            scale *= scale_new;
            continue;
        }
            
            
        /* Compute the parameter sensitivities if required. */
        if ( doparams ) 
		{
            /* Get the Jacobians at the converged solution. */
			if (jacFunc(t + h, x_new, f_new, params, NULL, J_f, J_Jf, dfdp, d2fdtdp) < 0)
				mexErrMsgTxt("Evaluation of Jacobian function failed.");            
            
            /* Compute the iteration matrix. */
            for ( i = 0 ; i < n * n ; i++ )
                M[i] = h * 0.5 * J_f[i] - h2 * 8.33333333333333333e-02 * J_Jf[i];
            for ( i = 0 ; i < n ; i++ )
                M[i*n+i] -= 1.0;
            dgetrf( &n , &n , M , &n , p , &info );
            if ( info < 0 )
                mexErrMsgTxt("Call to DGETRF failed (sensitivities).");
            if ( info > 0 )
                for ( i = 0 ; i < n ; i++ )
                    if ( M[i*n+i] == 0.0 )
                        M[i*n+i] = 1.0e-10;
            h_LU = h; recompJ = 0;
            
            /* Assemble the right-hand side of the sensitivity equations. */
            for ( i = 0 ; i < nparam * n ; i++ )
                s_new[i] = -s[i] - 0.5 * h * ( dsdt[i] + dfdp[i] ) - h2 * 8.33333333333333333e-02 * ( d2sdt2[i] - d2fdtdp[i] );
			
			/* Solve the sensitivity equations. */
            dgetrs( "N" , &n , &nparam , M , &n , p , s_new , &n , &info );
            if ( info )
                mexErrMsgTxt("Call to DGETRS failed (sensitivities).");
                
            /* Compute dsdt_new and d2sdt2_new. */
            for ( i = 0 ; i < n * nparam ; i++ ) 
			{
                dsdt_new[i] = dfdp[i];
                d2sdt2_new[i] = d2fdtdp[i];
            }
            dgemm( "N" , "N" , &n , &nparam , &n , &done , J_f , &n , s_new , &n , &done , dsdt_new , &n );
            dgemm( "N" , "N" , &n , &nparam , &n , &done , J_Jf , &n , s_new , &n , &done , d2sdt2_new , &n );
            
            
            /* Compute the integration error in each parameter. */
            if ( nrsteps == 0 ) 
			{
                /* For each parameter... */
                for ( j = 0 ; j < nparam ; j++ ) 
				{
                    h4 = h2 * h2;
                    t9 = 3.0/256.0*h;
                    t8 = h2/512.0;
                    for ( i = 0 ; i < n ; i++ ) 
					{
                        ind = j*n + i;
                        t11 = (dsdt_new[ind]+dsdt[ind])*h;
                        t10 = (dsdt[ind]-dsdt_new[ind])*h;
                        t7 = (d2sdt2[ind]+d2sdt2_new[ind])*h2;
                        t6 = (d2sdt2[ind]-d2sdt2_new[ind])*h2;
                        t5 = dsdt_new[ind]*t9;
                        t4 = d2sdt2_new[ind]*t8;
                        t2 = dsdt[ind]*t9;
                        t1 = d2sdt2[ind]*t8;
                        t19 = fabs(t2-t5+3.0/512.0*t7);
                        t23 = fabs(t2-t4+t5+t1-3.0/128.0*s_new[ind]+3.0/128.0*s[ind]);
                        t27 = fabs(t10/64.0+t7/128.0);
                        t33 = fabs(9.0/512.0*t11-9.0/256.0*s_new[ind]+9.0/256.0*s[ind]+3.0/1024.0*t6);
                        t36 = fabs(t1+t4+t10/256.0);
                        err[i] = t19+t23+t27+t33+t36;
                    }
                    
                    if ( norm_control ) 
					{
						for ( nerr = 0.0, i = 0 ; i < n ; i++ )
						{
							nerr += err[i] * err[i];
						}
						nerr = sqrt( nerr );
                        for ( tol = 0.0 , i = j*n ; i < (j+1)*n ; i++ )
						{
							tol += s_new[i] * s_new[i];
						}
                            
                        tol = rtol * sqrt( tol );
                        if ( tol < atol )
                            tol = atol;
                        temp = pow( 0.5 * tol / nerr , 0.2 );
                        if ( temp < scale_new )
                            scale_new = temp;
                    }
                    else 
					{
						for ( i = 0 ; i < n ; i++ ) 
						{
							ind = j*n + i;
							tol = rtol * fabs( s_new[ind] );
							if ( tol < atol )
								tol = atol;
                            temp = pow( 0.5 * tol / fabs( err[i] ) , 0.2 );
                            if ( temp < scale_new )
                                scale_new = temp;
                        }
                    }
                }       
            }
            else 
			{
                /* Construct the perr matrix. */
                for ( i = 0 ; i < n * nparam ; i++ )
                    perr[i] = -s_new[i] + s[i] + w_f*dsdt[i] + w_dfdt*d2sdt2[i] + w_f_new*dsdt_new[i] + w_dfdt_new*d2sdt2_new[i] + w_f_last*dsdt_last[i];
				
                /* Solve using the iteration matrix to compute a step. */
                dgetrs( "N" , &n , &nparam , M , &n , p , perr , &n , &info );
                if ( info )
                    mexErrMsgTxt("Call to DGETRS failed (sensitivities).");
                
                /* Get the scaling for each parameter. */
                for ( j = 0 ; j < nparam ; j++ ) 
				{
                    if ( norm_control ) 
					{
						for ( nerr = 0.0, tol = 0.0, i = j*n ; i < (j+1)*n; i++ )
						{
							tol  += s_new[i] * s_new[i];
							nerr += perr[i] * perr[i];
						}
						nerr = sqrt( nerr );
							
						tol = rtol * sqrt( tol );
						if ( tol < atol )
							tol = atol;
						temp = pow( 0.5 * tol / nerr , 0.2 );
						
                        if ( temp < scale_new )
                            scale_new = temp;
                    }
                    else 
					{
                        for ( i = j*n ; i < (j+1)*n ; i++ ) 
						{
							tol = rtol * fabs( s_new[i] );
							if ( tol < atol )
								tol = atol;
                            temp = pow( 0.5 * tol / fabs( perr[i] ) , 0.2 );
                            if ( temp < scale_new )
                                scale_new = temp;
                        }
                    }
                }
            }
			
            /* Did the error test fail? */
            if ( scale_new < 8.705505632961241e-01 ) 
			{
                /* mexPrintf("odeSD_mex: error test failed on vars at t=%e, h=%e, scale=%e\n",(*t),h,scale_new); */
                if ( scale_new < 0.1 )
                {
                    scale_new = 0.1;
                    recompJ = true;
                }
                else if ( scale_new > 0.8 )
                    scale_new = 0.8;
                h *= scale_new;
                h2 = h * h;
                scale *= scale_new;
                continue;
            }
            
        } /* Compute parameter sensitivities. */
            
            
        /* Compute intermediate steps. */
        if ( nr_tspan > 2 && tid < nr_tspan && t + h >= tspan[tid] ) 
		{
            /* Collect the intermediate times. */
            for ( nrtees = 0 ; tid < nr_tspan && t + h >= tspan[tid] ; tid++ ) 
			{
                if ( ++nrtees > nvx ) 
				{
                    if ( ( vx = (double *)mxRealloc( vx , sizeof(double) * 5 * (nrtees + 5) ) ) == NULL )
                        mexErrMsgTxt("Error re-allocating vx.");
                    nvx = nrtees + 5;
                }
                dptr = &( vx[ (nrtees - 1) * 5 ] );
                dptr[0] = 1.0;
                dptr[1] = 2.0 * ( tspan[tid] - t ) / h - 1.0;
                for ( i = 2 ; i < 5 ; i++ )
                    dptr[i] = 2.0 * dptr[1] * dptr[i-1] - dptr[i-2];
            }
                    
            /* Is there enough room? */
            if ( nrpoints + nrtees >= mxGetN( mT ) ) 
			{
                if ( ( dptr = (double *)mxRealloc( mxGetPr( mT ) , sizeof(double) * (mxGetN( mT ) + nrtees + 100) ) ) == NULL )
                    mexErrMsgTxt("Error re-allocating T.");
                mxSetPr( mT , dptr );
                mxSetN( mT , mxGetN( mT ) + 100 );
                if ( ( dptr = (double *)mxRealloc( mxGetPr( mX ) , sizeof(double) * n * (mxGetN( mX ) + nrtees + 100) ) ) == NULL )
                    mexErrMsgTxt("Error re-allocating X.");
                mxSetPr( mX , dptr );
                mxSetN( mX , mxGetN( mX ) + 100 );
                if ( doparams )
				{
                    pdims[0] = n; pdims[1] = nparam;
                    iptr = mxGetDimensions( mS ); pdims[2] = iptr[2] + nrtees + 100;
                    if ( ( dptr = (double *)mxRealloc( mxGetPr( mS ) , sizeof(double) * n * nparam * pdims[2] ) ) == NULL )
                        mexErrMsgTxt("Error re-allocating S.");
                    mxSetPr( mS , dptr );
                    mxSetDimensions( mS , pdims , 3 );
                }
            }
                
            /* Store values for T. */
            dptr = mxGetPr( mT ); dptr = &( dptr[ nrpoints + 1 ] );
            for ( i = 0 ; i < nrtees ; i++ )
                dptr[i] = t + (vx[i*5+1]+1.0)*0.5 * h;
                    
            /* For each variable... */
            for ( i = 0 ; i < n ; i++ ) 
			{
                /* Compute the coefficients of the system variables x[i]. */
                t9 = (-f[i]-f_new[i])*h;
                t10 = (-f[i]+f_new[i])*h;
                t16 = (dfdt_new[i]+dfdt[i])*h2;
                t15 = (dfdt[i]-dfdt_new[i])*h2;
                t8 = 0.1953125E-2*h2;
                t7 = dfdt[i]*t8;
                t5 = dfdt_new[i]*t8;
                alpha[0] = 0.5*x_new[i]+0.5*x[i]-0.7421875E-1*t10+0.5859375E-2*t16;
                alpha[1] = -1.0*t7+t5+0.4296875E-1*t9+0.5859375*x_new[i]-0.5859375*x[i];
                alpha[2] = 0.78125E-1*t10-0.78125E-2*t16;
                alpha[3] = 0.29296875E-2*t15-0.48828125E-1*t9+0.9765625E-1*x[i]-0.9765625E-1*x_new[i];
                alpha[4] = t5+t7-0.390625E-2*t10;
                
                /* For each time point... */
                for ( j = 0 ; j < nrtees ; j++ ) 
				{
                    dptr = &( vx[j*5] );
                    temp = alpha[0] + alpha[1]*dptr[1] + alpha[2]*dptr[2] + alpha[3]*dptr[3] + alpha[4]*dptr[4];
                    dptr = mxGetPr( mX );
                    dptr[ (nrpoints + j + 1) * n + i ] = temp;
                }
            }
                
            /* For each parameter sensitivity. */
            if ( doparams )
                for ( k = 0 ; k < nparam ; k++ ) 
				{
                    /* Set the offset inside the sentitivity matrices. */
                    offset = k*n;

                    for ( i = 0 ; i < n ; i++ ) 
					{

                        /* Compute the coefficients of the sensitivities s_k[i]. */
                        offset = k*n;
                        t9 = (-dsdt[offset+i]-dsdt_new[offset+i])*h;
                        t10 = (-dsdt[offset+i]+dsdt_new[offset+i])*h;
                        t16 = (d2sdt2_new[offset+i]+d2sdt2[offset+i])*h2;
                        t15 = (d2sdt2[offset+i]-d2sdt2_new[offset+i])*h2;
                        t8 = 0.1953125E-2*h2;
                        t7 = d2sdt2[offset+i]*t8;
                        t5 = d2sdt2_new[offset+i]*t8;
                        alpha[0] = 0.5*s_new[offset+i]+0.5*s[offset+i]-0.7421875E-1*t10+0.5859375E-2*t16;
                        alpha[1] = -1.0*t7+t5+0.4296875E-1*t9+0.5859375*s_new[offset+i]-0.5859375*s[offset+i];
                        alpha[2] = 0.78125E-1*t10-0.78125E-2*t16;
                        alpha[3] = 0.29296875E-2*t15-0.48828125E-1*t9+0.9765625E-1*s[offset+i]-0.9765625E-1*s_new[offset+i];
                        alpha[4] = t5+t7-0.390625E-2*t10;

                        /* For each time point... */
                        for ( j = 0 ; j < nrtees ; j++ ) 
						{
                            dptr = &( vx[j*5] );
                            temp = alpha[0] + alpha[1]*dptr[1] + alpha[2]*dptr[2] + alpha[3]*dptr[3] + alpha[4]*dptr[4];
                            dptr = mxGetPr( mS );
                            dptr[ (nrpoints + j + 1) * n * nparam + offset + i ] = temp;
                        }

                    }

                }
                
            /* Adjust nrpoints. */
            nrpoints += nrtees;
            
        } /* Compute intermediate steps. */
            
            
        /* Truncate small scalings to avoid re-computing the LU-decomposition. */
        if ( fabs( 1.0 - scale_new ) < 0.05 )
            scale_new = 1.0;
            
            
        /* Compute extrapolation coefficients for next step. */
        if ( nrsteps == 0 ) 
		{
            for ( i = 0 ; i < n ; i++ ) 
			{
                ecoeffs[i] = x[i];
                ecoeffs[i+n] = f[i] * h;
                ecoeffs[i+2*n] = -( 2.0 * f[i] * h + 3.0 * x[i] - 3.0 * x_new[i] + h * f_new[i] );
                ecoeffs[i+3*n] = 2.0 * x[i] + h * f[i] - 2.0 * x_new[i] + h * f_new[i];
            }
        }
        else 
		{
            t6 = h_last * h_last;
            t11 = h2+2*h*h_last+t6;
            t16 = 1.0 / (t11*h_last);
            t7 = h*h2;
            t5 = h_last*t6;
            t12 = -3*h_last*h2+t5;
            for ( i = 0 ; i < n ; i++ ) 
			{
                t15 = x_new[i] * t6;
                t14 = t7 * x_last[i];
                t13 = f_new[i] * t6;
                ecoeffs[i] = x[i];
                ecoeffs[i+n] = (2*t5*x_new[i]-t14-h2*t13+(-t5*f_new[i]+3*t15)*h+(-2*t5+t7-3*t6*h)*x[i])*t16;
                ecoeffs[i+2*n] = -(-2*t14+(-t5*h+t7*h_last)*f_new[i]+t12*x_new[i]+(2*t7-t12)*x[i])*t16;
                ecoeffs[i+3*n] = h*(-t15+(h_last*f_new[i]-x_last[i])*h2+(-2*h_last*x_new[i]+t13)*h+t11*x[i])*t16;
            }
        }
            
            
        /* Update the system variables. */
        for ( i = 0 ; i < n ; i++ ) 
		{
            x_last[i] = x[i];
            f_last[i] = f[i];
            x[i] = x_new[i];
            f[i] = f_new[i];
            dfdt[i] = dfdt_new[i];
        }
        if ( doparams )
            for ( i = 0 ; i < n * nparam ; i++ ) 
			{
                s[i] = s_new[i];
                dsdt_last[i] = dsdt[i];
                dsdt[i] = dsdt_new[i];
                d2sdt2[i] = d2sdt2_new[i];
            }
        h_last = h;
        t += h;
        nrsteps++;
        scale = scale_new;
        h *= scale;
        h2 = h * h;
        
        /* Add the current point to the results. */
        if ( nr_tspan == 2 ) 
		{
            if ( nrsteps >= mxGetN( mT ) ) 
			{
                if ( ( dptr = (double *)mxRealloc( mxGetPr( mT ) , sizeof(double) * (mxGetN( mT ) + mxGetN( mT )) ) ) == NULL )
                    mexErrMsgTxt("Error re-allocating T.");
                mxSetPr( mT , dptr );
                mxSetN( mT , mxGetN( mT ) + mxGetN( mT ));
                if ( ( dptr = (double *)mxRealloc( mxGetPr( mX ) , sizeof(double) * n * (mxGetN( mX ) + mxGetN( mX )) ) ) == NULL )
                    mexErrMsgTxt("Error re-allocating X.");
                mxSetPr( mX , dptr );
                mxSetN( mX , mxGetN( mX ) + mxGetN( mX ));
                if ( doparams ) 
				{
                    pdims[0] = n; pdims[1] = nparam;
                    iptr = mxGetDimensions( mS ); pdims[2] = iptr[2] + iptr[2];
                    if ( ( dptr = (double *)mxRealloc( mxGetPr( mS ) , sizeof(double) * n * nparam * pdims[2] ) ) == NULL )
                        mexErrMsgTxt("Error re-allocating S.");
                    mxSetPr( mS , dptr );
                    mxSetDimensions( mS , pdims , 3 );
                }
            }
            dptr = mxGetPr( mT );
            dptr[nrsteps] = t;
            dptr = mxGetPr( mX ); dptr = &(dptr[nrsteps*n]);
            for ( i = 0 ; i < n ; i++ )
                dptr[i] = x[i];
            if ( doparams ) 
			{
                dptr = mxGetPr( mS ); dptr = &(dptr[nrsteps*n*nparam]);
                for ( i = 0 ; i < n * nparam ; i++ )
                    dptr[i] = s[i];
            }
        }
    
    } /* Main loop. */
        
        
	/* Trim T and X. */
	if ( nr_tspan == 2 ) 
	{
		mxSetN( mT , nrsteps + 1 );
		mxSetN( mX , nrsteps + 1 );
		if ( doparams ) 
		{
			pdims[0] = n; pdims[1] = nparam; pdims[2] = nrsteps + 1;
			mxSetDimensions( mS , pdims , 3 );
		}
	}
	else 
	{
		mxSetN( mT , nrpoints + 1 );
		mxSetN( mX , nrpoints + 1 );
		if ( doparams ) 
		{
			pdims[0] = n; pdims[1] = nparam; pdims[2] = nrpoints + 1;
			mxSetDimensions( mS , pdims , 3 );
		}
	}

	/* Clean up data. */
	if (x) {mxFree(x); x = NULL;}
	if (s) {mxFree(s); s = NULL;}
	if (s_new) {mxFree(s_new); s_new = NULL;}
	if (dsdt_new) {mxFree(dsdt_new); dsdt_new = NULL;}
	if (d2sdt2_new) {mxFree(d2sdt2_new); d2sdt2_new = NULL;}
	if (perr) {mxFree(perr); perr = NULL;}
	if (dsdt_last) {mxFree(dsdt_last); dsdt_last = NULL;}
	if (M) {mxFree(M); M = NULL;}
	if (x_new) {mxFree(x_new); x_new = NULL;}
	if (err) {mxFree(err); err = NULL;}
	if (relwt) {mxFree(relwt); relwt = NULL;}
	if (x_last) {mxFree(x_last); x_last = NULL;}
	if (f_last) {mxFree(f_last); f_last = NULL;} 
	if (p) {mxFree(p); p = NULL;} 
	if (delta) {mxFree(delta); delta = NULL;} 
	if (f) {mxFree(f); f = NULL;}
	if (dfdt) {mxFree(dfdt); dfdt = NULL;}
	if (J_f) {mxFree(J_f); J_f = NULL;} 
	if (J_Jf) {mxFree(J_Jf); J_Jf = NULL;} 
	if (dsdt) {mxFree(dsdt); dsdt = NULL;}
	if (f_new) {mxFree(f_new); f_new = NULL;}
	if (dfdt_new) {mxFree(dfdt_new); dfdt_new = NULL;} 
	if (dfdp) {mxFree(dfdp); dfdp = NULL;}
	if (d2fdtdp) {mxFree(d2fdtdp); d2fdtdp = NULL;}
	if (ecoeffs) {mxFree(ecoeffs); ecoeffs = NULL;}
		
		

}
