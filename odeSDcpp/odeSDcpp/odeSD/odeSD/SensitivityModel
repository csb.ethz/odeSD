#pragma once
#include <odeSD/Model>
#include <odeSD/SensitivityState>

namespace odeSD
{

	class SensitivityModel :
		public Model
	{
	public:
		SensitivityModel()
		{
		}

		virtual ~SensitivityModel()
		{
		}

		virtual void calculateJacAndSensitivities(JacobianState& jacState, SensitivityState& state) = 0;
		virtual void calculateRHSandJacAndSensitivities(SensitivityState& state, JacobianState& jacState)
		{
			// Default implementation: 1. calculate RHS, 2. calculate jacobian & sensitivities
			calculateRHS(state);
			calculateJacAndSensitivities(jacState, state);
		}

		virtual void observeNewSensitivityState(Eigen::DenseIndex timeIndex, const SensitivityState& state)
		{
		}

		virtual void observeIntermediateSensitivityState(Eigen::DenseIndex stepIndex, const SensitivityState& state)
		{
		}

		virtual const SensitivityState& getInitialState() = 0;
	};

}
