function [ T , X , S ] = odeSD_wrapper_hybrid ( modelName , tspan , x0 , options , params )
% ODESD_WRAPPER Solve an ODE using a second-derivative rule, MEX wrapper.
% 
%    [T,Y] = ODESD_WRAPPER(F,TSPAN,Y0,OPTS) integrates the ordinary
%    differential equiation defined by
%
%       dY/dT = F(T,Y)
%
%    From time TSPAN(1) to TSPAN(end), using the initial values Y(TSPAN(1))=Y0.
%    The right-hand side F(T,Y) must be of the form
%
%       [ DXDT, D2XDT2, J_F, J_JF ] = F ( T, X )
%
%    where DXDT and D2XDT2 are the first and second derivatives of X with
%    respect to T and J_F and J_JF are the Jacobian matrices of these
%    two values with respect to X.
%
%    The OPTS parameter is a structure created and modified with the ODESET
%    command. It contains parameters to the integrator such as the relative
%    and absolute tolerances. ODESD requires the option "Jacobian" to be
%    set to a function of the type
%
%       [ J_F, J_JF ] = J ( T, X, P, DXDT )
%
%    which returns the same Jacobians as F. If TSPAN is of length 2, the output
%    variables T and Y will contain the time steps taken by the integrator
%    and the system variables at those times respectively. If TSPAN is of
%    length greater than 2, only the times and values at the times specified
%    in TSPAN are returned. The system variables at the kth time step are
%    stored in the kth row of Y.
%
%    [T,Y,S] = ODESD_WRAPPER(F,TSPAN,Y0,OPTS,P) computes the parameter
%    sensitivities S, i.e. the derivatives of Y with respect to the parameters
%    P. If the output S is requested, the right-hand side F must be of the form
%
%       [ DXDT, D2XDT2, J_F, J_JF , DFDP , D2FDTDP ] = F ( T, X, P )
% 
%    where DFDP ad D2FDTDP are matrices containing the derivatives of 
%    DXDT and D2XDT2 (columns), respectively, with respect to each parameter
%    P (rows). The parameter sensitivities S are stored in a three dimensional
%    array in which S(I,J,K) contains the derivative of the Ith system variable
%    with respect to the Jth parameter at the Kth time step.
%
%    Reference:
%      P. Gonnet, S. Dimopoulos, L. Widmer and J. Stelling, "A Specialized ODE
%      Integrator for the Efficient Computation of Parameter Sensitivities",
%      Submitted to BMC Systems Biology, 2011.
%
%    See also:
%      other ODE solvers:    ODESD, ODE15S, ODE23S, ODE23T, ODE45, ODE23
%      implicit ODEs:        ODE15I
%      options handling:     ODESET, ODEGET
%      function handles:     FUNCTION_HANDLE

% This file is part of odeSD.
% Coypright (c) 2011 Pedro Gonnet (gonnet@maths.ox.ac.uk), Lukas Widmer,
% Sotiris Dimopoulos and J�rg Stelling.
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

    % convert the odeset options to CVodeSetOptions options
    if nargin < 4, options = []; end;
    RelTol = odeget(options,'RelTol',1.0e-6,'fast');
    AbsTol = odeget(options,'AbsTol',1.0e-6,'fast');
    h0 = odeget(options,'InitialStep',(tspan(end)-tspan(1))/100,'fast');
    MaxStep = odeget(options,'MaxStep',0,'fast');
    NormControl = strcmp(odeget(options,'NormControl',[],'fast'),'on');
    NonNegative = odeget(options,'NonNegative',[],'fast');

    func = ['odeSD_hybrid_' modelName];
    func = str2func(func);

    if nargin < 5
        [ T , X ] = func( tspan(:) , x0 , h0 , MaxStep , RelTol , AbsTol , NormControl , int32(NonNegative(:)) );
    elseif nargout < 3
        [ T , X ] = func( tspan(:) , x0 , h0 , MaxStep , RelTol , AbsTol , NormControl , int32(NonNegative(:)) , params );
    else
        n = length(x0);
        nparam = length(params);
        if isstruct(options) && isfield(options, 'initialSensitivities')
            if any(size(options.initialSensitivities) ~= [n nparam])
                error('odeSD:wrongSensitivitySize', 'The initial sensitivity matrix size must be nStates x nParameters.');
            end
            s0 = options.initialSensitivities;
        else
            s0 = zeros(n, nparam);
        end
        [ T , X , S ] = func( tspan(:) , x0 , h0 , MaxStep , RelTol , AbsTol , NormControl , int32(NonNegative(:)) , s0, params );
    end
    
    % wrap the result correctly
    X = X';
