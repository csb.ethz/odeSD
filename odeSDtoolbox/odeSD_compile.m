function odeSD_compile( file, debug )

    mexOptions = {'-largeArrayDims'};
    if exist('debug', 'var') && debug
        mexOptions{end+1} = '-g';
    end
    
    if ispc
        mexOptions{end+1} = 'COMPFLAGS="$COMPFLAGS /Wall"';
        % On Windows, link against libmwblas.lib and libmwlapack.lib
        mex(mexOptions{:}, file, 'libmwblas.lib', 'libmwlapack.lib');
    elseif isunix
        mexOptions{end+1} = 'COMPFLAGS="$COMPFLAGS -Wall"';
        % On Linux, link against libmwblas.so and libmwlapack.so
        extension = '.so';
        if ismac
            % On Mac, link against libmwblas.dylib and libmwlapack.dylib
            extension = '.dylib';
        end
        mex(mexOptions{:}, file, [matlabroot '/bin/' computer('arch') '/libmwblas' extension],  [matlabroot '/bin/' computer('arch') '/libmwlapack' extension]);
    else
        error('Unknown Platform');
    end
end

