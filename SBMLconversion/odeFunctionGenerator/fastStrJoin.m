function concatenatedString = fastStrJoin(array, delimiter)
    concatenatedString = sprintf(['%s' delimiter], array{:});
    concatenatedString = concatenatedString(1:end-length(sprintf(delimiter)));
end