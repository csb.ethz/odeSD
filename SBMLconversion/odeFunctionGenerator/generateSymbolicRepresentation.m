function symModel = generateSymbolicRepresentation( inputModel, varargin )
    symModel = struct();

    isIQM = false;
    if ischar(inputModel)
        % SBML File
        [~, symModel.name, ~] = fileparts(inputModel);
        isIQM = true;
    elseif isa(inputModel,'sym')
        if nargin < 5 || ~isa(varargin{1},'char') || ~isa(varargin{2},'sym') || ~isnumeric(varargin{3}) || ~isa(varargin{4},'sym') || ~isnumeric(varargin{5})
            error('Symbolic models requires symbolic right-hand-side, symbolic state vector, symbolic parameter vector and numeric parameter value vector as inputs');
        end
    elseif isa(inputModel,'IQMmodel')
        % IQM Model
        symModel.name = symModel.model.name;
        isIQM = true;
    else
        error('Unknown Input');
    end
    
    syms time real;
    symModel.symTimeVariable = time;
    symModel.timeVariable = 'time';
    if isIQM
        symModel.model = IQMmodel(inputModel);

        % Extract States
        [symModel.stateNames,symModel.stateODEs] = IQMstates(symModel.model);
        symModel.symStateNames = sym(symModel.stateNames);

        % Extract Initial Values
        symModel.initStateVals = IQMinitialconditions(symModel.model);
        symModel.initStateVals = symModel.initStateVals(:);

        % Extract Variables
        [symModel.varNames, symModel.varFormulas] = IQMvariables(symModel.model);
        symModel.symVarNames = sym(symModel.varNames);

        % Extract Reactions
        [symModel.reactionNames,symModel.reactionFormulas] = IQMreactions(symModel.model);
        symModel.symReactionNames = sym(symModel.reactionNames);

        % Extract Parameters
        [symModel.parameterNames, symModel.parameterValues] = IQMparameters(symModel.model); % that includes the compartments
        symModel.parameterValues = symModel.parameterValues(:);
        symModel.symParameterNames = sym(symModel.parameterNames);

        % Check for inputs
        symModel.noSubstVarNames = {}; 
        symModel.noSubstSymVarNames = sym([]);
        symModel.noSubstVarFormulas = {};
        
        if nargin >= 2
            options = varargin{1};
            if ~isstruct(options)
                error('Options must be a struct!');
            end
            
            if isfield(options, 'inputs')
                if ~iscell(options.inputs)
                    error('Inputs must be a cell array of variable names');
                end
                
                symModel.inputNames = options.inputs(:);
                symModel.symInputNames = sym(symModel.inputNames);
                nInputs = length(symModel.symInputNames);
                
                for inputIndex = 1:nInputs
                    stateIndex = find(symModel.symStateNames == symModel.symInputNames(inputIndex));
                    parameterIndex = find(symModel.symParameterNames == symModel.symInputNames(inputIndex));

                    if stateIndex
                        % exclude state
                        symModel.symStateNames = symModel.symStateNames([1:(stateIndex-1) (stateIndex+1):end]); 
                        symModel.stateNames = symModel.stateNames([1:(stateIndex-1) (stateIndex+1):end]);
                        symModel.initStateVals = symModel.initStateVals([1:(stateIndex-1) (stateIndex+1):end]);
                        symModel.stateODEs = symModel.stateODEs([1:(stateIndex-1) (stateIndex+1):end]);
                    end                   
                    
                    if parameterIndex
                        % exclude parameter
                        symModel.symParameterNames = symModel.symParameterNames([1:(parameterIndex-1) (parameterIndex+1):end]);
                        symModel.parameterNames = symModel.parameterNames([1:(parameterIndex-1) (parameterIndex+1):end]);
                        symModel.parameterValues = symModel.parameterValues([1:(parameterIndex-1) (parameterIndex+1):end]);
                    end
                end
                
                
                symModel.inputVector      = sym('odeSDin%dvar', [length(symModel.inputNames) 1]);
                symModel.inputVectorNames = arrayfun(@char, symModel.inputVector, 'UniformOutput', false);
                assignIt(symModel.inputNames, symModel.symInputNames);
                assignIt(symModel.inputVectorNames, symModel.inputVector);
            end
            if isfield(options, 'doNotSubstitute')
                symModel.doNotSubstitute = options.doNotSubstitute;
                nNoSubstVars = length(options.doNotSubstitute);
                symModel.doNotSubstituteIndices = [];
                symModel.symDoNotSubstitute = sym(symModel.doNotSubstitute);
                
                for noSubstIndex = 1:nNoSubstVars
                    varIndex = find(symModel.symVarNames == symModel.symDoNotSubstitute(noSubstIndex));
                    if varIndex
                        symModel.doNotSubstituteIndices = [symModel.doNotSubstituteIndices varIndex];
                        
                        % Need to substitute these afterwards
                        symModel.noSubstVarNames{end+1} = symModel.varNames{varIndex}; 
                        symModel.noSubstSymVarNames(end+1) = symModel.symVarNames(varIndex); 
                        symModel.noSubstVarFormulas{end+1} = symModel.varFormulas{varIndex};

                        %symModel.symVarNames = symModel.symVarNames([1:(varIndex-1) (varIndex+1):end]);
                        %symModel.varNames = symModel.varNames([1:(varIndex-1) (varIndex+1):end]);
                        symModel.varFormulas{varIndex} = symModel.varNames{varIndex};% = symModel.varFormulas([1:(varIndex-1) (varIndex+1):end]);
                    end
                end
                assignIt(symModel.doNotSubstitute, symModel.symDoNotSubstitute);
            end
        end
        
        
        % Create First Derivative Variables
        symModel.stateDiffVector = sym('odeSDf%dvar', [length(symModel.stateNames) 1]);
        symModel.stateDiffVectorNames = arrayfun(@char, symModel.stateDiffVector, 'UniformOutput', false);

        % Assign variables in current workspace
        assignIt(symModel.stateNames    , symModel.symStateNames);
        assignIt(symModel.varNames      , symModel.symVarNames);
        assignIt(symModel.reactionNames , symModel.symReactionNames);
        assignIt(symModel.parameterNames, symModel.symParameterNames);
        assignIt(symModel.stateDiffVectorNames, symModel.stateDiffVector);
    
        %% Generate RHS
        symModel.RHS = eval(['[' fastStrJoin(symModel.stateODEs,';') ']']);
        symModel.RHS = subs(symModel.RHS, symModel.symReactionNames, eval(['[' strjoin(symModel.reactionFormulas,';') ']']));

        % Substitue Variables recursively (variables can be defined as other
        % variables) until convergence
        symModel.symVarFormulas = sym('symVarFormulas', [length(symModel.varFormulas) 1]);
        for i = 1:length(symModel.varFormulas)
            temp = eval(symModel.varFormulas{i});
            while ischar(temp)
                temp = eval(temp);
            end
            symModel.symVarFormulas(i) = temp;
        end
        symModel.RHSold = '';
        while ~strcmp(char(symModel.RHS),char(symModel.RHSold))
            symModel.RHSold = symModel.RHS;
            symModel.RHS = subs(symModel.RHS,symModel.symVarNames,symModel.symVarFormulas);
        end
        symModel.RHSold = '';
        symModel = rmfield(symModel, 'RHSold');
        
    else
        symModel.RHS = inputModel;
        symModel.name = varargin{1};
        
        symModel.symStateNames = varargin{2}(:);
        symModel.stateNames    = arrayfun(@char, symModel.symStateNames, 'UniformOutput', false);

        symModel.initStateVals  = varargin{3}(:);
        symModel.symParameterNames = varargin{4}(:);
        symModel.parameterNames    = arrayfun(@char, symModel.symParameterNames, 'UniformOutput', false);
        
        symModel.parameterValues = varargin{5}(:);
        
        symModel.stateDiffVector = sym('odeSDf%dvar', [length(symModel.stateNames) 1]);
        symModel.stateDiffVectorNames = arrayfun(@char, symModel.stateDiffVector, 'UniformOutput', false);
        
        assignIt(symModel.stateNames    , symModel.symStateNames);
        assignIt(symModel.parameterNames, symModel.symParameterNames);
        assignIt(symModel.stateDiffVectorNames, symModel.stateDiffVector);
        
        symModel.noSubstVarNames = {}; 
    end
    
    %% Generate State Arrays
    symModel.stateVector      = sym('odeSDx%dvar', [length(symModel.stateNames) 1]);
    symModel.stateVectorNames = arrayfun(@char, symModel.stateVector, 'UniformOutput', false);
    assignIt(symModel.stateVectorNames, symModel.stateVector);

    %% Generate Parameter Arrays
    symModel.parameterVector          = sym('odeSDp%dvar', [length(symModel.parameterNames) 1]);
    symModel.parameterVectorNames     = arrayfun(@char, symModel.parameterVector, 'UniformOutput', false);
    assignIt(symModel.parameterVectorNames, symModel.parameterVector);
    
    %% Substitute Generic Names
    if isfield(symModel, 'inputNames')
        symModel.RHS = subs(symModel.RHS,[symModel.symStateNames;symModel.symParameterNames;symModel.symInputNames],[symModel.stateVector; symModel.parameterVector; symModel.inputVector]);
    else
        symModel.RHS = subs(symModel.RHS,[symModel.symStateNames;symModel.symParameterNames],[symModel.stateVector; symModel.parameterVector]);
    end
    
    %% First Derivative Jacobians
    symModel.dRHSdx = jacobian(symModel.RHS, symModel.stateVector);
    symModel.dRHSdp = jacobian(symModel.RHS, symModel.parameterVector);
    
    %% Generate Second Derivative of RHS
    symModel.dRHSdt = symModel.dRHSdx * symModel.stateDiffVector;
    
    %% Second Derivative Jacobians
    symModel.d2RHSdtdx = jacobian(symModel.dRHSdt, symModel.stateVector);% + dRHSdx*dRHSdx;
    symModel.d2RHSdtdp = jacobian(symModel.dRHSdt, symModel.parameterVector);% + dRHSdx*dRHSdp;
    
    %% Substitute Variables
    if ~isempty(symModel.noSubstVarNames)
        currentIndex = 1;
        for substIndex = symModel.doNotSubstituteIndices
            symModel.varFormulas{substIndex} = symModel.noSubstVarFormulas{currentIndex};
            currentIndex = currentIndex + 1;
            
            temp = eval(symModel.varFormulas{substIndex});
            while ischar(temp)
                temp = eval(temp);
            end
            symModel.symVarFormulas(substIndex) = temp;
        end

        if isfield(symModel, 'inputNames')
            symModel.symVarFormulas = subs(symModel.symVarFormulas,[symModel.symStateNames;symModel.symParameterNames;symModel.symInputNames],[symModel.stateVector; symModel.parameterVector; symModel.inputVector]);
        else
            symModel.symVarFormulas = subs(symModel.symVarFormulas,[symModel.symStateNames;symModel.symParameterNames],[symModel.stateVector; symModel.parameterVector]);
        end
        
        symModel.RHSold = '';
        while ~strcmp(char(symModel.RHS),char(symModel.RHSold))
            symModel.RHSold = symModel.RHS;
            symModel.RHS    = subs(symModel.RHS,symModel.symVarNames,symModel.symVarFormulas);
            symModel.dRHSdx = subs(symModel.dRHSdx,symModel.symVarNames,symModel.symVarFormulas);
            symModel.dRHSdp = subs(symModel.dRHSdp,symModel.symVarNames,symModel.symVarFormulas);
            symModel.d2RHSdtdx = subs(symModel.d2RHSdtdx,symModel.symVarNames,symModel.symVarFormulas);
            symModel.d2RHSdtdp = subs(symModel.d2RHSdtdp,symModel.symVarNames,symModel.symVarFormulas);            
        end
        symModel.dRHSdt = symModel.dRHSdx * symModel.stateDiffVector;
        symModel.RHSold = '';
        symModel = rmfield(symModel, 'RHSold');
    end
end

