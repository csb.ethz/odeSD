function [files, handles] = createOdeSdCppRhs( symModel, targetFolder, options )
    %% Write Files
    syms odeSDtimeVariable real;
    
    initialStateString = sprintf('%#.0g, ', symModel.initStateVals);
    initialStateString = initialStateString(1:end-2);
    parameterString = sprintf('%#.0g, ', symModel.parameterValues);
    parameterString = parameterString(1:end-2);
    
    fString = cppWriteSymVector(vpa(symModel.RHS), 'f', false);
    gString = cppWriteSymVector(vpa(symModel.dRHSdt), 'dfdt', false);
    
    dfdxVector = cppWriteSymVector(vpa(symModel.dRHSdx(:)), 'dfdxVector', true);
    dfdpVector = cppWriteSymVector(vpa(symModel.dRHSdp(:)), 'dfdpVector', true);    
    dfPrimedxVector = cppWriteSymVector(vpa(symModel.d2RHSdtdx(:)), 'dfPrimedxVector', true, true);
    dfPrimedpVector = cppWriteSymVector(vpa(symModel.d2RHSdtdp(:)), 'dfPrimedpVector', true, true);    
    
    if isempty(targetFolder)
        modelFile = [symModel.name '.cpp'];
    else
        if (targetFolder(end) ~= filesep)
            targetFolder = [targetFolder filesep];
        end
        modelFile = [targetFolder symModel.name '.cpp'];
    end
    
    templateRoot = [fileparts(mfilename('fullpath')) filesep 'templates' filesep];
    copyfile([templateRoot 'cppTemplate.cpp'], modelFile);
    odeCode = fileread(modelFile);
    odeCode = strrep(odeCode, '##ModelName##', symModel.name);
    odeCode = strrep(odeCode, '##nStates##', int2str(length(symModel.initStateVals)));
    odeCode = strrep(odeCode, '##nParameters##', int2str(length(symModel.parameterValues)));
    
    odeCode = strrep(odeCode, '##InitialConditions##', initialStateString);
    odeCode = strrep(odeCode, '##Parameters##', parameterString);
    odeCode = strrep(odeCode, '##fValues##', fString);
    odeCode = strrep(odeCode, '##gValues##', gString);
        
    odeCode = strrep(odeCode, '##dfdxVector##', dfdxVector);
    odeCode = strrep(odeCode, '##dfPrimedxVector##', dfPrimedxVector);
    odeCode = strrep(odeCode, '##dfdpVector##', dfdpVector);
    odeCode = strrep(odeCode, '##dfPrimedpVector##', dfPrimedpVector);
    
    fileID = fopen(modelFile,'w');
    fprintf(fileID,'%s',odeCode);
    fclose(fileID);
    
    files = {modelFile};
    handles = {};    
end

