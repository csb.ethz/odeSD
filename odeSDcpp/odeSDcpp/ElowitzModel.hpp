#pragma once
#include <string>
#include <Eigen/Dense>
#include <Eigen/SparseCore>
#include <math.h>
#include <odeSD/DefaultSensitivityModel>



class ElowitzModel : 
	public odeSD::DefaultSensitivityModel
{
public:
	odeSD::SensitivityState initialState;


	ElowitzModel() :
		initialState(6, 8)
	{
		initialState.x << 0, 0, 0, 0, 20, 0;
		initialState.s.setZero();

		parameters = Eigen::VectorXd(8);
		parameters << 20, 2, 40, 2, 10, 0.5, 0.0005, 1;

		tspan = Eigen::VectorXd(200);
		for (Eigen::DenseIndex i = 0; i < tspan.rows(); i++)
		{
			tspan(i) = i * 1000.0 / (tspan.rows() - 1.0);
		}
		
		states = Eigen::MatrixXd(initialState.nStates, tspan.rows());
		sensitivities = Eigen::MatrixXd(initialState.nStates * initialState.nParameters, tspan.rows());
	};
	//virtual ~ODEmodel();

	odeSD::SensitivityState& getInitialState() 
	{
		return initialState;
	}

	// time, current state, f, f'
	void calculateRHS(odeSD::State& state) 
	{
		Eigen::VectorXd& p = parameters;
		/***********************************************
		* 1st DERIVATIVE EQUATIONS (f)
		**********************************************/

		state.f[0] = (state.x[0] * -6.931471809999999E-1) / p[4] + (p[0] * state.x[3] * 6.931471809999999E-1) / p[3];
		state.f[1] = (state.x[1] * -6.931471809999999E-1) / p[4] + (p[0] * state.x[4] * 6.931471809999999E-1) / p[3];
		state.f[2] = (state.x[2] * -6.931471809999999E-1) / p[4] + (p[0] * state.x[5] * 6.931471809999999E-1) / p[3];
		state.f[3] = (state.x[3] * -6.931471805599453E-1) / p[3] + p[6] * 6.0E1 + ((p[5] * 6.0E1 - p[6] * 6.0E1)*pow(p[2], p[1])) / (pow(state.x[2], p[1]) + pow(p[2], p[1]));
		state.f[4] = (state.x[4] * -6.931471805599453E-1) / p[3] + p[6] * 6.0E1 + ((p[5] * 6.0E1 - p[6] * 6.0E1)*pow(p[2], p[1])) / (pow(state.x[0], p[1]) + pow(p[2], p[1]));
		state.f[5] = (state.x[5] * -6.931471805599453E-1) / p[3] + p[6] * 6.0E1 + ((p[5] * 6.0E1 - p[6] * 6.0E1)*pow(p[2], p[1])) / (pow(state.x[1], p[1]) + pow(p[2], p[1]));

		/***********************************************
		* 2nd DERIVATIVE EQUATIONS (Jfx*f = g)
		**********************************************/

		state.fPrime[0] = (state.f[0] * -6.931471810000001E-1) / p[4] + (state.f[3] * p[0] * 6.931471810000001E-1) / p[3];
		state.fPrime[1] = (state.f[1] * -6.931471810000001E-1) / p[4] + (state.f[4] * p[0] * 6.931471810000001E-1) / p[3];
		state.fPrime[2] = (state.f[2] * -6.931471810000001E-1) / p[4] + (state.f[5] * p[0] * 6.931471810000001E-1) / p[3];
		state.fPrime[3] = (state.f[3] * -6.931471805599453E-1) / p[3] - 1.0 / pow(pow(state.x[2], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*state.f[2] * p[1] * pow(p[2], p[1])*pow(state.x[2], p[1] - 1.0);
		state.fPrime[4] = (state.f[4] * -6.931471805599453E-1) / p[3] - 1.0 / pow(pow(state.x[0], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*state.f[0] * p[1] * pow(p[2], p[1])*pow(state.x[0], p[1] - 1.0);
		state.fPrime[5] = (state.f[5] * -6.931471805599453E-1) / p[3] - 1.0 / pow(pow(state.x[1], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*state.f[1] * p[1] * pow(p[2], p[1])*pow(state.x[1], p[1] - 1.0);

	}

	virtual void calculateJac(odeSD::JacobianState& jacState, odeSD::State& state) 
	{
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;
		///***********************************************
		//* STATE JACOBIAN EQUATIONS (J)
		//**********************************************/

		jacState.dfdx.setZero();
		Eigen::Map<Eigen::VectorXd>& J = jacState.dfdxVector;


		J[0] = -6.931471809999999E-1 / p[4];
		J[4] = -1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*p[1] * pow(p[2], p[1])*pow(x[0], p[1] - 1.0);
		J[7] = -6.931471809999999E-1 / p[4];
		J[11] = -1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*p[1] * pow(p[2], p[1])*pow(x[1], p[1] - 1.0);
		J[14] = -6.931471809999999E-1 / p[4];
		J[15] = -1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*p[1] * pow(p[2], p[1])*pow(x[2], p[1] - 1.0);
		J[18] = (p[0] * 6.931471809999999E-1) / p[3];
		J[21] = -6.931471805599453E-1 / p[3];
		J[25] = (p[0] * 6.931471809999999E-1) / p[3];
		J[28] = -6.931471805599453E-1 / p[3];
		J[32] = (p[0] * 6.931471809999999E-1) / p[3];
		J[35] = -6.931471805599453E-1 / p[3];

		///***********************************************
		//* JACOBIAN OF 2nd DERIVATIVE EQUATIONS (Jgx)
		//**********************************************/

		jacState.dfPrimedx.setZero();
		Eigen::Map<Eigen::VectorXd>& dJ = jacState.dfPrimedxVector;

		dJ[4] = 1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 3.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[0] * (p[1] * p[1])*pow(p[2], p[1])*pow(x[0], p[1] * 2.0 - 2.0)*2.0 - (p[1] - 1.0)*1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[0] * p[1] * pow(p[2], p[1])*pow(x[0], p[1] - 2.0);
		dJ[11] = 1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 3.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[1] * (p[1] * p[1])*pow(p[2], p[1])*pow(x[1], p[1] * 2.0 - 2.0)*2.0 - (p[1] - 1.0)*1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[1] * p[1] * pow(p[2], p[1])*pow(x[1], p[1] - 2.0);
		dJ[15] = 1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 3.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[2] * (p[1] * p[1])*pow(p[2], p[1])*pow(x[2], p[1] * 2.0 - 2.0)*2.0 - (p[1] - 1.0)*1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[2] * p[1] * pow(p[2], p[1])*pow(x[2], p[1] - 2.0);

		jacState.dfPrimedx += jacState.dfdx * jacState.dfdx;
	}

	virtual void calculateJacAndSensitivities(odeSD::JacobianState& jacState, odeSD::SensitivityState& state) 
	{
		calculateJac(jacState, state);
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		// Sensitivities
		
		state.dfdp.setZero();
		Eigen::Map<Eigen::VectorXd>& dfdp = state.dfdpVector;

		dfdp[0] = (x[3] * 6.931471809999999E-1) / p[3];
		dfdp[1] = (x[4] * 6.931471809999999E-1) / p[3];
		dfdp[2] = (x[5] * 6.931471809999999E-1) / p[3];
		dfdp[9] = (log(p[2])*(p[5] * 6.0E1 - p[6] * 6.0E1)*pow(p[2], p[1])) / (pow(x[2], p[1]) + pow(p[2], p[1])) - 1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*(log(p[2])*pow(p[2], p[1]) + log(x[2])*pow(x[2], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*pow(p[2], p[1]);
		dfdp[10] = (log(p[2])*(p[5] * 6.0E1 - p[6] * 6.0E1)*pow(p[2], p[1])) / (pow(x[0], p[1]) + pow(p[2], p[1])) - 1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*(log(p[2])*pow(p[2], p[1]) + log(x[0])*pow(x[0], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*pow(p[2], p[1]);
		dfdp[11] = (log(p[2])*(p[5] * 6.0E1 - p[6] * 6.0E1)*pow(p[2], p[1])) / (pow(x[1], p[1]) + pow(p[2], p[1])) - 1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*(log(p[2])*pow(p[2], p[1]) + log(x[1])*pow(x[1], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*pow(p[2], p[1]);
		dfdp[15] = ((p[5] * 6.0E1 - p[6] * 6.0E1)*p[1] * pow(p[2], p[1] - 1.0)) / (pow(x[2], p[1]) + pow(p[2], p[1])) - 1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*p[1] * pow(p[2], p[1] - 1.0)*pow(p[2], p[1]);
		dfdp[16] = ((p[5] * 6.0E1 - p[6] * 6.0E1)*p[1] * pow(p[2], p[1] - 1.0)) / (pow(x[0], p[1]) + pow(p[2], p[1])) - 1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*p[1] * pow(p[2], p[1] - 1.0)*pow(p[2], p[1]);
		dfdp[17] = ((p[5] * 6.0E1 - p[6] * 6.0E1)*p[1] * pow(p[2], p[1] - 1.0)) / (pow(x[1], p[1]) + pow(p[2], p[1])) - 1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*p[1] * pow(p[2], p[1] - 1.0)*pow(p[2], p[1]);
		dfdp[18] = p[0] * 1.0 / (p[3] * p[3])*x[3] * -6.931471809999999E-1;
		dfdp[19] = p[0] * 1.0 / (p[3] * p[3])*x[4] * -6.931471809999999E-1;
		dfdp[20] = p[0] * 1.0 / (p[3] * p[3])*x[5] * -6.931471809999999E-1;
		dfdp[21] = 1.0 / (p[3] * p[3])*x[3] * 6.931471805599453E-1;
		dfdp[22] = 1.0 / (p[3] * p[3])*x[4] * 6.931471805599453E-1;
		dfdp[23] = 1.0 / (p[3] * p[3])*x[5] * 6.931471805599453E-1;
		dfdp[24] = 1.0 / (p[4] * p[4])*x[0] * 6.931471809999999E-1;
		dfdp[25] = 1.0 / (p[4] * p[4])*x[1] * 6.931471809999999E-1;
		dfdp[26] = 1.0 / (p[4] * p[4])*x[2] * 6.931471809999999E-1;
		dfdp[33] = (pow(p[2], p[1])*6.0E1) / (pow(x[2], p[1]) + pow(p[2], p[1]));
		dfdp[34] = (pow(p[2], p[1])*6.0E1) / (pow(x[0], p[1]) + pow(p[2], p[1]));
		dfdp[35] = (pow(p[2], p[1])*6.0E1) / (pow(x[1], p[1]) + pow(p[2], p[1]));
		dfdp[39] = (pow(p[2], p[1])*-6.0E1) / (pow(x[2], p[1]) + pow(p[2], p[1])) + 6.0E1;
		dfdp[40] = (pow(p[2], p[1])*-6.0E1) / (pow(x[0], p[1]) + pow(p[2], p[1])) + 6.0E1;
		dfdp[41] = (pow(p[2], p[1])*-6.0E1) / (pow(x[1], p[1]) + pow(p[2], p[1])) + 6.0E1;

		state.dfPrimedp.setZero();
		Eigen::Map<Eigen::VectorXd>& dfPrimedp = state.dfPrimedpVector;

		dfPrimedp[0] = (f[3] * 6.931471809999999E-1) / p[3];
		dfPrimedp[1] = (f[4] * 6.931471809999999E-1) / p[3];
		dfPrimedp[2] = (f[5] * 6.931471809999999E-1) / p[3];
		dfPrimedp[9] = -1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[2] * pow(p[2], p[1])*pow(x[2], p[1] - 1.0) - log(p[2])*1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[2] * p[1] * pow(p[2], p[1])*pow(x[2], p[1] - 1.0) - log(x[2])*1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[2] * p[1] * pow(p[2], p[1])*pow(x[2], p[1] - 1.0) + 1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 3.0)*(log(p[2])*pow(p[2], p[1]) + log(x[2])*pow(x[2], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[2] * p[1] * pow(p[2], p[1])*pow(x[2], p[1] - 1.0)*2.0;
		dfPrimedp[10] = -1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[0] * pow(p[2], p[1])*pow(x[0], p[1] - 1.0) - log(p[2])*1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[0] * p[1] * pow(p[2], p[1])*pow(x[0], p[1] - 1.0) - log(x[0])*1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[0] * p[1] * pow(p[2], p[1])*pow(x[0], p[1] - 1.0) + 1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 3.0)*(log(p[2])*pow(p[2], p[1]) + log(x[0])*pow(x[0], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[0] * p[1] * pow(p[2], p[1])*pow(x[0], p[1] - 1.0)*2.0;
		dfPrimedp[11] = -1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[1] * pow(p[2], p[1])*pow(x[1], p[1] - 1.0) - log(p[2])*1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[1] * p[1] * pow(p[2], p[1])*pow(x[1], p[1] - 1.0) - log(x[1])*1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[1] * p[1] * pow(p[2], p[1])*pow(x[1], p[1] - 1.0) + 1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 3.0)*(log(p[2])*pow(p[2], p[1]) + log(x[1])*pow(x[1], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[1] * p[1] * pow(p[2], p[1])*pow(x[1], p[1] - 1.0)*2.0;
		dfPrimedp[15] = -1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[2] * (p[1] * p[1])*pow(p[2], p[1] - 1.0)*pow(x[2], p[1] - 1.0) + 1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 3.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[2] * (p[1] * p[1])*pow(p[2], p[1] - 1.0)*pow(p[2], p[1])*pow(x[2], p[1] - 1.0)*2.0;
		dfPrimedp[16] = -1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[0] * (p[1] * p[1])*pow(p[2], p[1] - 1.0)*pow(x[0], p[1] - 1.0) + 1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 3.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[0] * (p[1] * p[1])*pow(p[2], p[1] - 1.0)*pow(p[2], p[1])*pow(x[0], p[1] - 1.0)*2.0;
		dfPrimedp[17] = -1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[1] * (p[1] * p[1])*pow(p[2], p[1] - 1.0)*pow(x[1], p[1] - 1.0) + 1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 3.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*f[1] * (p[1] * p[1])*pow(p[2], p[1] - 1.0)*pow(p[2], p[1])*pow(x[1], p[1] - 1.0)*2.0;
		dfPrimedp[18] = f[3] * p[0] * 1.0 / (p[3] * p[3])*-6.931471809999999E-1;
		dfPrimedp[19] = f[4] * p[0] * 1.0 / (p[3] * p[3])*-6.931471809999999E-1;
		dfPrimedp[20] = f[5] * p[0] * 1.0 / (p[3] * p[3])*-6.931471809999999E-1;
		dfPrimedp[21] = f[3] * 1.0 / (p[3] * p[3])*6.931471805599453E-1;
		dfPrimedp[22] = f[4] * 1.0 / (p[3] * p[3])*6.931471805599453E-1;
		dfPrimedp[23] = f[5] * 1.0 / (p[3] * p[3])*6.931471805599453E-1;
		dfPrimedp[24] = f[0] * 1.0 / (p[4] * p[4])*6.931471809999999E-1;
		dfPrimedp[25] = f[1] * 1.0 / (p[4] * p[4])*6.931471809999999E-1;
		dfPrimedp[26] = f[2] * 1.0 / (p[4] * p[4])*6.931471809999999E-1;
		dfPrimedp[33] = 1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*f[2] * p[1] * pow(p[2], p[1])*pow(x[2], p[1] - 1.0)*-6.0E1;
		dfPrimedp[34] = 1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*f[0] * p[1] * pow(p[2], p[1])*pow(x[0], p[1] - 1.0)*-6.0E1;
		dfPrimedp[35] = 1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*f[1] * p[1] * pow(p[2], p[1])*pow(x[1], p[1] - 1.0)*-6.0E1;
		dfPrimedp[39] = 1.0 / pow(pow(x[2], p[1]) + pow(p[2], p[1]), 2.0)*f[2] * p[1] * pow(p[2], p[1])*pow(x[2], p[1] - 1.0)*6.0E1;
		dfPrimedp[40] = 1.0 / pow(pow(x[0], p[1]) + pow(p[2], p[1]), 2.0)*f[0] * p[1] * pow(p[2], p[1])*pow(x[0], p[1] - 1.0)*6.0E1;
		dfPrimedp[41] = 1.0 / pow(pow(x[1], p[1]) + pow(p[2], p[1]), 2.0)*f[1] * p[1] * pow(p[2], p[1])*pow(x[1], p[1] - 1.0)*6.0E1;

		state.dfPrimedp += jacState.dfdx * state.dfdp;

	}
};
