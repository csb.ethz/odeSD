/******************************************************************************* 
 * This file is part of odeSD.
 ******************************************************************************/ 


/** Initial conditions. */ 
#define Elowitz2000_6states_Nstates 6 
extern const int Elowitz2000_6states_Nparams; 
extern const double Elowitz2000_6states_x0[]; 
extern const double Elowitz2000_6states_p[]; 
extern const char *Elowitz2000_6states_name; 

 /** Right-hand sides. */ 
int Elowitz2000_6states_jac ( double t , const double *x , const double *f , const double *p , void *varargin , double *J , double *dJ , double *dfdp , double *d2fdtdp );
int Elowitz2000_6states_f ( double t , const double *x , const double *p , void *varargin , double *f , double *dfdt , double *J , double *dJ , double *dfdp , double *d2fdtdp );
int Elowitz2000_6states_dsdt ( double t , const double *x , const double *s , const double *p , void *varargin , double *dsdt );

