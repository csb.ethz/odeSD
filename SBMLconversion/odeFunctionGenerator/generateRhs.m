function [files, handles, symModel] = generateRhs( inputModel, outputTypes, targetFolder, options )
    
    generatorFunctionFiles = dir([fileparts(mfilename('fullpath')) filesep 'createOdeSd*Rhs.m']);
    generatorTypes = regexp({generatorFunctionFiles.name}, 'createOdeSd(.+?)Rhs\.m', 'tokens');
    
    nGenerators = length(generatorTypes);
    for i=1:nGenerators
        generatorTypes(i) = generatorTypes{i}{1};
    end
    
    if nargin < 2
        usageError();
    end
    
    if ~exist('targetFolder', 'var')
        targetFolder = '';
    end
    
    if ~exist('options', 'var')
        options = struct();
    end
    
    
    if ischar(outputTypes)
        generator = findGenerator(outputTypes);
        symModel = generateSymbolicRepresentation(inputModel, options);
        [files, handles] = generator(symModel, targetFolder, options);
    elseif iscell(outputTypes)
        generators = {};
        for outputType = outputTypes
            generators{end+1} = findGenerator(outputType{1});
        end
        symModel = generateSymbolicRepresentation(inputModel, options);
        files = {};
        handles = {};
        for generator = generators
            generator = generator{1};
            [newFiles, newHandles] = generator(symModel, targetFolder, options);
            files = [files newFiles];
            handles = [handles newHandles];
        end
    else
        usageError();
    end
    
    function usageError()
        error(sprintf(['Usage: generateRhs( inputModel, outputTypes, [targetFolder], [options]) \n' ...
            'inputModel must be a path to an SBML model file or an IQMmodel object.\n' ...
            'outputTypes must be a string or a cell array of the following code generators: ' fastStrJoin(generatorTypes, ', ') '.']));
    end

    function generator = findGenerator(outputType)
        generatorIndex = strcmpi(outputType, generatorTypes);
        
        if ~any(generatorIndex)
            usageError();
        end
        
        generator = generatorTypes{generatorIndex};
        generator = str2func(['createOdeSd' generator 'Rhs']);
    end

end

