/******************************************************************************* 
 * This file is part of odeSD.
 ******************************************************************************/ 


/** Standard header files. */ 
#include <stdlib.h> 
#include <string.h> 
#include <math.h> 

/** Library header files. */ 
#include "clapack.h"
#include "cblas.h"

/** Model header */
#include "Elowitz2000_6states.h"

/** LW: Patch for Visual Studio */
#ifdef WIN32
#define alloca _alloca
#endif

/** Initial conditions. */ 

const int Elowitz2000_6states_Nparams = 8; 
const double Elowitz2000_6states_x0[6] = {0,0,0,0,20,0};
const double Elowitz2000_6states_p[8] = {20,2,40,2,10,0.5,0.0005,1};
const char *Elowitz2000_6states_name = "Elowitz2000_6states";



/** 
* Sensitivities right-hand side for ODE model
*/ 

int Elowitz2000_6states_dsdt ( double t , const double *x , const double *s , const double *p , void *varargin , double *dsdt )
{ 

	double *J = (double *)alloca( sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nstates ); 

/*********************************************** 
* STATE JACOBIAN EQUATIONS (J) 
**********************************************/ 
	memset(J, 0, sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nstates);

	 // bzero( J , sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nstates ); 
	 J[0] =  -6.931471809999999E-1/p[4]; 
	 J[4] =  -1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1])*pow(x[0],p[1]-1.0); 
	 J[7] =  -6.931471809999999E-1/p[4]; 
	 J[11] =  -1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1])*pow(x[1],p[1]-1.0); 
	 J[14] =  -6.931471809999999E-1/p[4]; 
	 J[15] =  -1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1])*pow(x[2],p[1]-1.0); 
	 J[18] =  (p[0]*6.931471809999999E-1)/p[3]; 
	 J[21] =  -6.931471805599453E-1/p[3]; 
	 J[25] =  (p[0]*6.931471809999999E-1)/p[3]; 
	 J[28] =  -6.931471805599453E-1/p[3]; 
	 J[32] =  (p[0]*6.931471809999999E-1)/p[3]; 
	 J[35] =  -6.931471805599453E-1/p[3]; 


/*********************************************** 
* PARAMETER JACOBIAN EQUATIONS (dfdp)  
**********************************************/ 
	 memset(dsdt, 0, sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nparams);
	 // bzero( dsdt , sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nparams ); 
	 dsdt[0] =  (x[3]*6.931471809999999E-1)/p[3];
	 dsdt[1] =  (x[4]*6.931471809999999E-1)/p[3];
	 dsdt[2] =  (x[5]*6.931471809999999E-1)/p[3];
	 dsdt[9] =  (log(p[2])*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]))/(pow(x[2],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(log(p[2])*pow(p[2],p[1])+log(x[2])*pow(x[2],p[1]))*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]);
	 dsdt[10] =  (log(p[2])*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]))/(pow(x[0],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(log(p[2])*pow(p[2],p[1])+log(x[0])*pow(x[0],p[1]))*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]);
	 dsdt[11] =  (log(p[2])*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]))/(pow(x[1],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(log(p[2])*pow(p[2],p[1])+log(x[1])*pow(x[1],p[1]))*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]);
	 dsdt[15] =  ((p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0))/(pow(x[2],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0)*pow(p[2],p[1]);
	 dsdt[16] =  ((p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0))/(pow(x[0],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0)*pow(p[2],p[1]);
	 dsdt[17] =  ((p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0))/(pow(x[1],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0)*pow(p[2],p[1]);
	 dsdt[18] =  p[0]*1.0/(p[3]*p[3])*x[3]*-6.931471809999999E-1;
	 dsdt[19] =  p[0]*1.0/(p[3]*p[3])*x[4]*-6.931471809999999E-1;
	 dsdt[20] =  p[0]*1.0/(p[3]*p[3])*x[5]*-6.931471809999999E-1;
	 dsdt[21] =  1.0/(p[3]*p[3])*x[3]*6.931471805599453E-1;
	 dsdt[22] =  1.0/(p[3]*p[3])*x[4]*6.931471805599453E-1;
	 dsdt[23] =  1.0/(p[3]*p[3])*x[5]*6.931471805599453E-1;
	 dsdt[24] =  1.0/(p[4]*p[4])*x[0]*6.931471809999999E-1;
	 dsdt[25] =  1.0/(p[4]*p[4])*x[1]*6.931471809999999E-1;
	 dsdt[26] =  1.0/(p[4]*p[4])*x[2]*6.931471809999999E-1;
	 dsdt[33] =  (pow(p[2],p[1])*6.0E1)/(pow(x[2],p[1])+pow(p[2],p[1]));
	 dsdt[34] =  (pow(p[2],p[1])*6.0E1)/(pow(x[0],p[1])+pow(p[2],p[1]));
	 dsdt[35] =  (pow(p[2],p[1])*6.0E1)/(pow(x[1],p[1])+pow(p[2],p[1]));
	 dsdt[39] =  (pow(p[2],p[1])*-6.0E1)/(pow(x[2],p[1])+pow(p[2],p[1]))+6.0E1;
	 dsdt[40] =  (pow(p[2],p[1])*-6.0E1)/(pow(x[0],p[1])+pow(p[2],p[1]))+6.0E1;
	 dsdt[41] =  (pow(p[2],p[1])*-6.0E1)/(pow(x[1],p[1])+pow(p[2],p[1]))+6.0E1;

	 cblas_dgemm( CblasColMajor , CblasNoTrans , CblasNoTrans , 
	  Elowitz2000_6states_Nstates , Elowitz2000_6states_Nparams , Elowitz2000_6states_Nstates , 1.0 ,  
	  J, Elowitz2000_6states_Nstates , s , Elowitz2000_6states_Nstates , 
	  1.0 , dsdt , Elowitz2000_6states_Nstates );  

	 /* Happiness is a warm zero. */ 

	 return 0; 
} 


/** 
* Jacobian Right-hand side for ODE model
*/ 

int Elowitz2000_6states_jac ( double t , const double *x , const double *f , const double *p , void *varargin , double *J , double *dJ , double *dfdp , double *d2fdtdp ) 
{ 

	/*********************************************** 
	* STATE JACOBIAN EQUATIONS (J) 
	**********************************************/ 

	if ( J != NULL || dJ != NULL || d2fdtdp != NULL ) { 
	 // bzero( J , sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nstates ); 
		 memset(J, 0, sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nstates);
		 J[0] =  -6.931471809999999E-1/p[4]; 
		 J[4] =  -1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1])*pow(x[0],p[1]-1.0); 
		 J[7] =  -6.931471809999999E-1/p[4]; 
		 J[11] =  -1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1])*pow(x[1],p[1]-1.0); 
		 J[14] =  -6.931471809999999E-1/p[4]; 
		 J[15] =  -1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1])*pow(x[2],p[1]-1.0); 
		 J[18] =  (p[0]*6.931471809999999E-1)/p[3]; 
		 J[21] =  -6.931471805599453E-1/p[3]; 
		 J[25] =  (p[0]*6.931471809999999E-1)/p[3]; 
		 J[28] =  -6.931471805599453E-1/p[3]; 
		 J[32] =  (p[0]*6.931471809999999E-1)/p[3]; 
		 J[35] =  -6.931471805599453E-1/p[3]; 
	 } 


	/*********************************************** 
	* JACOBIAN OF 2nd DERIVATIVE EQUATIONS (Jgx) 
	**********************************************/ 

	 if ( dJ != NULL ) 
	 { 
		 memset(dJ, 0, sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nstates);

		 //bzero( dJ , sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nstates ); 
		 dJ[4] =  1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),3.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[0]*(p[1]*p[1])*pow(p[2],p[1])*pow(x[0],p[1]*2.0-2.0)*2.0-(p[1]-1.0)*1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[0]*p[1]*pow(p[2],p[1])*pow(x[0],p[1]-2.0); 
		 dJ[11] =  1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),3.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[1]*(p[1]*p[1])*pow(p[2],p[1])*pow(x[1],p[1]*2.0-2.0)*2.0-(p[1]-1.0)*1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[1]*p[1]*pow(p[2],p[1])*pow(x[1],p[1]-2.0); 
		 dJ[15] =  1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),3.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[2]*(p[1]*p[1])*pow(p[2],p[1])*pow(x[2],p[1]*2.0-2.0)*2.0-(p[1]-1.0)*1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[2]*p[1]*pow(p[2],p[1])*pow(x[2],p[1]-2.0); 

		 cblas_dgemm( CblasColMajor , CblasNoTrans , CblasNoTrans , Elowitz2000_6states_Nstates , Elowitz2000_6states_Nstates , 
		  Elowitz2000_6states_Nstates , 1.0, J , 
		  Elowitz2000_6states_Nstates , J , Elowitz2000_6states_Nstates , 
		  1.0 , dJ , Elowitz2000_6states_Nstates );  

	 } 


	/*********************************************** 
	* PARAMETER JACOBIAN EQUATIONS (dfdp)  
	**********************************************/ 

	 if ( dfdp != NULL || d2fdtdp != NULL )
	 { 
		 memset(dfdp, 0, sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nparams);
		 //  bzero( dfdp , sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nparams ); 
		 dfdp[0] =  (x[3]*6.931471809999999E-1)/p[3];
		 dfdp[1] =  (x[4]*6.931471809999999E-1)/p[3];
		 dfdp[2] =  (x[5]*6.931471809999999E-1)/p[3];
		 dfdp[9] =  (log(p[2])*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]))/(pow(x[2],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(log(p[2])*pow(p[2],p[1])+log(x[2])*pow(x[2],p[1]))*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]);
		 dfdp[10] =  (log(p[2])*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]))/(pow(x[0],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(log(p[2])*pow(p[2],p[1])+log(x[0])*pow(x[0],p[1]))*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]);
		 dfdp[11] =  (log(p[2])*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]))/(pow(x[1],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(log(p[2])*pow(p[2],p[1])+log(x[1])*pow(x[1],p[1]))*(p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]);
		 dfdp[15] =  ((p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0))/(pow(x[2],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0)*pow(p[2],p[1]);
		 dfdp[16] =  ((p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0))/(pow(x[0],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0)*pow(p[2],p[1]);
		 dfdp[17] =  ((p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0))/(pow(x[1],p[1])+pow(p[2],p[1]))-1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*p[1]*pow(p[2],p[1]-1.0)*pow(p[2],p[1]);
		 dfdp[18] =  p[0]*1.0/(p[3]*p[3])*x[3]*-6.931471809999999E-1;
		 dfdp[19] =  p[0]*1.0/(p[3]*p[3])*x[4]*-6.931471809999999E-1;
		 dfdp[20] =  p[0]*1.0/(p[3]*p[3])*x[5]*-6.931471809999999E-1;
		 dfdp[21] =  1.0/(p[3]*p[3])*x[3]*6.931471805599453E-1;
		 dfdp[22] =  1.0/(p[3]*p[3])*x[4]*6.931471805599453E-1;
		 dfdp[23] =  1.0/(p[3]*p[3])*x[5]*6.931471805599453E-1;
		 dfdp[24] =  1.0/(p[4]*p[4])*x[0]*6.931471809999999E-1;
		 dfdp[25] =  1.0/(p[4]*p[4])*x[1]*6.931471809999999E-1;
		 dfdp[26] =  1.0/(p[4]*p[4])*x[2]*6.931471809999999E-1;
		 dfdp[33] =  (pow(p[2],p[1])*6.0E1)/(pow(x[2],p[1])+pow(p[2],p[1]));
		 dfdp[34] =  (pow(p[2],p[1])*6.0E1)/(pow(x[0],p[1])+pow(p[2],p[1]));
		 dfdp[35] =  (pow(p[2],p[1])*6.0E1)/(pow(x[1],p[1])+pow(p[2],p[1]));
		 dfdp[39] =  (pow(p[2],p[1])*-6.0E1)/(pow(x[2],p[1])+pow(p[2],p[1]))+6.0E1;
		 dfdp[40] =  (pow(p[2],p[1])*-6.0E1)/(pow(x[0],p[1])+pow(p[2],p[1]))+6.0E1;
		 dfdp[41] =  (pow(p[2],p[1])*-6.0E1)/(pow(x[1],p[1])+pow(p[2],p[1]))+6.0E1;
	} 

	/*********************************************** 
	* 2nd derivative PARAMETER JACOBIAN EQUATIONS (Jgp) 
	**********************************************/ 

	if ( d2fdtdp != NULL ) 
	{ 
		memset(d2fdtdp, 0, sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nparams);
		// bzero( d2fdtdp , sizeof(double) * Elowitz2000_6states_Nstates * Elowitz2000_6states_Nparams ); 
		d2fdtdp[0] =  (f[3]*6.931471809999999E-1)/p[3]; 
		d2fdtdp[1] =  (f[4]*6.931471809999999E-1)/p[3]; 
		d2fdtdp[2] =  (f[5]*6.931471809999999E-1)/p[3]; 
		d2fdtdp[9] =  -1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[2]*pow(p[2],p[1])*pow(x[2],p[1]-1.0)-log(p[2])*1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[2]*p[1]*pow(p[2],p[1])*pow(x[2],p[1]-1.0)-log(x[2])*1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[2]*p[1]*pow(p[2],p[1])*pow(x[2],p[1]-1.0)+1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),3.0)*(log(p[2])*pow(p[2],p[1])+log(x[2])*pow(x[2],p[1]))*(p[5]*6.0E1-p[6]*6.0E1)*f[2]*p[1]*pow(p[2],p[1])*pow(x[2],p[1]-1.0)*2.0; 
		d2fdtdp[10] =  -1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[0]*pow(p[2],p[1])*pow(x[0],p[1]-1.0)-log(p[2])*1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[0]*p[1]*pow(p[2],p[1])*pow(x[0],p[1]-1.0)-log(x[0])*1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[0]*p[1]*pow(p[2],p[1])*pow(x[0],p[1]-1.0)+1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),3.0)*(log(p[2])*pow(p[2],p[1])+log(x[0])*pow(x[0],p[1]))*(p[5]*6.0E1-p[6]*6.0E1)*f[0]*p[1]*pow(p[2],p[1])*pow(x[0],p[1]-1.0)*2.0; 
		d2fdtdp[11] =  -1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[1]*pow(p[2],p[1])*pow(x[1],p[1]-1.0)-log(p[2])*1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[1]*p[1]*pow(p[2],p[1])*pow(x[1],p[1]-1.0)-log(x[1])*1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[1]*p[1]*pow(p[2],p[1])*pow(x[1],p[1]-1.0)+1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),3.0)*(log(p[2])*pow(p[2],p[1])+log(x[1])*pow(x[1],p[1]))*(p[5]*6.0E1-p[6]*6.0E1)*f[1]*p[1]*pow(p[2],p[1])*pow(x[1],p[1]-1.0)*2.0; 
		d2fdtdp[15] =  -1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[2]*(p[1]*p[1])*pow(p[2],p[1]-1.0)*pow(x[2],p[1]-1.0)+1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),3.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[2]*(p[1]*p[1])*pow(p[2],p[1]-1.0)*pow(p[2],p[1])*pow(x[2],p[1]-1.0)*2.0; 
		d2fdtdp[16] =  -1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[0]*(p[1]*p[1])*pow(p[2],p[1]-1.0)*pow(x[0],p[1]-1.0)+1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),3.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[0]*(p[1]*p[1])*pow(p[2],p[1]-1.0)*pow(p[2],p[1])*pow(x[0],p[1]-1.0)*2.0; 
		d2fdtdp[17] =  -1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[1]*(p[1]*p[1])*pow(p[2],p[1]-1.0)*pow(x[1],p[1]-1.0)+1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),3.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[1]*(p[1]*p[1])*pow(p[2],p[1]-1.0)*pow(p[2],p[1])*pow(x[1],p[1]-1.0)*2.0; 
		d2fdtdp[18] =  f[3]*p[0]*1.0/(p[3]*p[3])*-6.931471809999999E-1; 
		d2fdtdp[19] =  f[4]*p[0]*1.0/(p[3]*p[3])*-6.931471809999999E-1; 
		d2fdtdp[20] =  f[5]*p[0]*1.0/(p[3]*p[3])*-6.931471809999999E-1; 
		d2fdtdp[21] =  f[3]*1.0/(p[3]*p[3])*6.931471805599453E-1; 
		d2fdtdp[22] =  f[4]*1.0/(p[3]*p[3])*6.931471805599453E-1; 
		d2fdtdp[23] =  f[5]*1.0/(p[3]*p[3])*6.931471805599453E-1; 
		d2fdtdp[24] =  f[0]*1.0/(p[4]*p[4])*6.931471809999999E-1; 
		d2fdtdp[25] =  f[1]*1.0/(p[4]*p[4])*6.931471809999999E-1; 
		d2fdtdp[26] =  f[2]*1.0/(p[4]*p[4])*6.931471809999999E-1; 
		d2fdtdp[33] =  1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*f[2]*p[1]*pow(p[2],p[1])*pow(x[2],p[1]-1.0)*-6.0E1; 
		d2fdtdp[34] =  1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*f[0]*p[1]*pow(p[2],p[1])*pow(x[0],p[1]-1.0)*-6.0E1; 
		d2fdtdp[35] =  1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*f[1]*p[1]*pow(p[2],p[1])*pow(x[1],p[1]-1.0)*-6.0E1; 
		d2fdtdp[39] =  1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*f[2]*p[1]*pow(p[2],p[1])*pow(x[2],p[1]-1.0)*6.0E1; 
		d2fdtdp[40] =  1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*f[0]*p[1]*pow(p[2],p[1])*pow(x[0],p[1]-1.0)*6.0E1; 
		d2fdtdp[41] =  1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*f[1]*p[1]*pow(p[2],p[1])*pow(x[1],p[1]-1.0)*6.0E1; 

		cblas_dgemm( CblasColMajor , CblasNoTrans , CblasNoTrans , Elowitz2000_6states_Nstates , Elowitz2000_6states_Nparams , 
		Elowitz2000_6states_Nstates , 1.0, J , 
		Elowitz2000_6states_Nstates , dfdp , Elowitz2000_6states_Nstates , 
		1.0 , d2fdtdp , Elowitz2000_6states_Nstates );  

	} 


 /* All is well that ends well... */ 
 return 0; 

} 


/** 
* Right-hand side for ODE model
*/ 

int Elowitz2000_6states_f ( double t , const double *x , const double *p , void *varargin , double *f , double *dfdt , double *J , double *dJ , double *dfdp , double *d2fdtdp )
{ 

	 int res = 0; 

/*********************************************** 
* 1st DERIVATIVE EQUATIONS (f) 
**********************************************/ 

	 f[0]= (x[0]*-6.931471809999999E-1)/p[4]+(p[0]*x[3]*6.931471809999999E-1)/p[3];
	 f[1]= (x[1]*-6.931471809999999E-1)/p[4]+(p[0]*x[4]*6.931471809999999E-1)/p[3];
	 f[2]= (x[2]*-6.931471809999999E-1)/p[4]+(p[0]*x[5]*6.931471809999999E-1)/p[3];
	 f[3]= (x[3]*-6.931471805599453E-1)/p[3]+p[6]*6.0E1+((p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]))/(pow(x[2],p[1])+pow(p[2],p[1]));
	 f[4]= (x[4]*-6.931471805599453E-1)/p[3]+p[6]*6.0E1+((p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]))/(pow(x[0],p[1])+pow(p[2],p[1]));
	 f[5]= (x[5]*-6.931471805599453E-1)/p[3]+p[6]*6.0E1+((p[5]*6.0E1-p[6]*6.0E1)*pow(p[2],p[1]))/(pow(x[1],p[1])+pow(p[2],p[1]));

/*********************************************** 
* 2nd DERIVATIVE EQUATIONS (Jfx*f = g)  
**********************************************/ 

	if ( dfdt != NULL ) 
	{ 
		dfdt[0]= (f[0]*-6.931471810000001E-1)/p[4]+(f[3]*p[0]*6.931471810000001E-1)/p[3];
		dfdt[1]= (f[1]*-6.931471810000001E-1)/p[4]+(f[4]*p[0]*6.931471810000001E-1)/p[3];
		dfdt[2]= (f[2]*-6.931471810000001E-1)/p[4]+(f[5]*p[0]*6.931471810000001E-1)/p[3];
		dfdt[3]= (f[3]*-6.931471805599453E-1)/p[3]-1.0/pow(pow(x[2],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[2]*p[1]*pow(p[2],p[1])*pow(x[2],p[1]-1.0);
		dfdt[4]= (f[4]*-6.931471805599453E-1)/p[3]-1.0/pow(pow(x[0],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[0]*p[1]*pow(p[2],p[1])*pow(x[0],p[1]-1.0);
		dfdt[5]= (f[5]*-6.931471805599453E-1)/p[3]-1.0/pow(pow(x[1],p[1])+pow(p[2],p[1]),2.0)*(p[5]*6.0E1-p[6]*6.0E1)*f[1]*p[1]*pow(p[2],p[1])*pow(x[1],p[1]-1.0);
	} 

/* Jacobians and stuff. */ 
 	 if ( ( res = Elowitz2000_6states_jac( t , x , f , p , varargin , J , dJ , dfdp , d2fdtdp ) ) < 0 ) 
 	 return res; 

/* All is well... */ 
		 return 0; 

} 

