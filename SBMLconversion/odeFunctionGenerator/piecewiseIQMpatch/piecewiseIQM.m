function [results] = piecewiseIQM(varargin)
% piecewiseIQM: This function implements support for the SBML / MATHML
% piecewise operator.Mdified by Lukas Widmer. 
% 
% USAGE:
% ======
% [result] = piecewiseIQM(resultiftrue1,decision1,resultiftrue2,decision2,...,resultiftruen,decisionn)   
% [result] = piecewiseIQM(resultiftrue1,decision1,resultiftrue2,decision2,...,resultiftruen,decisionn,defaultresult)    
%
% decision1,...,decisionn: logical argument, e.g. returned from a comparison
% result1,...,resultn: returnvalue in case the corresponding decision is
%   evaluated to be true
% defaultresult: if none of the decisions are true this defaultresult is returned.
%
% Output Arguments:
% =================
% result: the result corresponding to the decision that is true. if no
%   decision is true and no defaultresult i given an error will occurr.

% <<<COPYRIGHTSTATEMENT - IQM TOOLS LITE>>>


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETERMINE THE OUTPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

condition = char(varargin{2});

if nargin == 3
    if ischar(varargin{3})
        other = varargin{3};
    elseif isnumeric(varargin{3})
        other = num2str(varargin{3});
    else
        other = char(varargin{3});
    end
end
currentCondition = '';


while ~strcmp(currentCondition, condition)
    currentCondition = condition;
    condition = regexprep(condition, '([^<>=]+)\s+<=\s+([^<>=]+)', '(0.5 + 0.5*tanh(($2 - ($1))*1e2))');
    condition = regexprep(condition, '([^<>=]+)\s+>=\s+([^<>=]+)', '(0.5 + 0.5*tanh(($1 - ($2))*1e2))');
    condition = regexprep(condition, '([^<>=]+)\s+<\s+([^<>=]+)', '(0.5 + 0.5*tanh(($2 - ($1))*1e2))');
    condition = regexprep(condition, '([^<>=]+)\s+>\s+([^<>=]+)', '(0.5 + 0.5*tanh(($1 - ($2))*1e2))');
end

if varargin{1} ~= 0
    results = ['((' condition ') .* (' char(sym(varargin{1})) '))'];
    
    
    if nargin == 3
        results = ['(' results ') + ((1-(' condition ')) .* (' other '))'];
    end
else
    results = ['((1-(' condition ')) .* (' other '))'];
end

return
   