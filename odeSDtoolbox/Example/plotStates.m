function plotStates( T, X, S )
    %%
    % plot the states
    figure (1); 
    plot (T ,X , '.-');
    title('States');
    hold on;

    % plot the sensitivities of the 1st parameter w.r.t all states
    figure (2);
    plot (T , squeeze( S (: ,1 ,:) ) , '.- ' );
    title('Sensitivities');
    hold on;

end

