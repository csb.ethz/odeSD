#pragma once
#include <string>
#include <Eigen/Dense>
#include <odeSD/DefaultSensitivityModel>

class Elowitz2000_6statesModel :
	public odeSD::DefaultSensitivityModel
{
public:
	odeSD::SensitivityState initialState;

	virtual void observeIntermediateSensitivityState(Eigen::DenseIndex stepIndex, const odeSD::SensitivityState& state)
	{
		return;
	}

	virtual void observeIntermediateState(Eigen::DenseIndex stepIndex, const odeSD::State& state)
	{
		return;
	}

	Elowitz2000_6statesModel() :
		initialState(6, 8)
	{
		initialState.x << 0.0, 0.0, 0.0, 0.0, 2.e+01, 0.0;

		initialState.s.setZero();

		parameters = Eigen::VectorXd(initialState.nParameters);
		parameters << 2.e+01, 2., 4.e+01, 2.0, 1.e+01, 0.5, 0.0005, 1.;
		// To test for negative states:
		// parameters << 20.645967689217826546, 2.0, 40.0, 1.6095162819807864452, 10.197941159529236188, -0.41956636841400063442, -1.6733006355436785828, 1.0;

		tspan = Eigen::VectorXd(200);
		for (Eigen::DenseIndex i = 0; i < tspan.rows(); i++)
		{
			tspan(i) = i * 1000.0 / (tspan.rows() - 1.0);
		}

		states = Eigen::MatrixXd(initialState.nStates, tspan.rows());
		sensitivities = Eigen::MatrixXd(initialState.nStates * initialState.nParameters, tspan.rows());
	}

	odeSD::SensitivityState& getInitialState()
	{
		return initialState;
	}

	// time, current state, f, f'
	virtual void calculateRHS(odeSD::State& state)
	{
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& dfdt = state.fPrime;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		/***********************************************
		* 1st DERIVATIVE EQUATIONS (f)
		**********************************************/

		f[0] = (x[0] * -6.931471805599453E-1) / (p[4] * p[7]) + (p[0] * x[3] * 6.931471805599453E-1) / (p[3] * p[7]);
		f[1] = (x[1] * -6.931471805599453E-1) / (p[4] * p[7]) + (p[0] * x[4] * 6.931471805599453E-1) / (p[3] * p[7]);
		f[2] = (x[2] * -6.931471805599453E-1) / (p[4] * p[7]) + (p[0] * x[5] * 6.931471805599453E-1) / (p[3] * p[7]);
		f[3] = p[6] * 6.0E1 + (pow(p[2], p[1])*(p[5] * 6.0E1 - p[6] * 6.0E1)) / (pow(p[2], p[1]) + pow(x[2] / p[7], p[1])) - (x[3] * 6.931471805599453E-1) / (p[3] * p[7]);
		f[4] = p[6] * 6.0E1 + (pow(p[2], p[1])*(p[5] * 6.0E1 - p[6] * 6.0E1)) / (pow(p[2], p[1]) + pow(x[0] / p[7], p[1])) - (x[4] * 6.931471805599453E-1) / (p[3] * p[7]);
		f[5] = p[6] * 6.0E1 + (pow(p[2], p[1])*(p[5] * 6.0E1 - p[6] * 6.0E1)) / (pow(p[2], p[1]) + pow(x[1] / p[7], p[1])) - (x[5] * 6.931471805599453E-1) / (p[3] * p[7]);


		/***********************************************
		* 2nd DERIVATIVE EQUATIONS (Jfx*f = g)
		**********************************************/

		dfdt[0] = (f[0] * -6.931471805599453E-1) / (p[4] * p[7]) + (f[3] * p[0] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfdt[1] = (f[1] * -6.931471805599453E-1) / (p[4] * p[7]) + (f[4] * p[0] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfdt[2] = (f[2] * -6.931471805599453E-1) / (p[4] * p[7]) + (f[5] * p[0] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfdt[3] = (f[3] * -6.931471805599453E-1) / (p[3] * p[7]) - (f[2] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0) / p[7];
		dfdt[4] = (f[4] * -6.931471805599453E-1) / (p[3] * p[7]) - (f[0] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0) / p[7];
		dfdt[5] = (f[5] * -6.931471805599453E-1) / (p[3] * p[7]) - (f[1] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0) / p[7];

	}

	virtual void calculateJac(odeSD::JacobianState& jacState, odeSD::State& state)
	{
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		jacState.dfdx.setZero();
		Eigen::Map<Eigen::VectorXd>& dfdxVector = jacState.dfdxVector;

		dfdxVector[0] = -6.931471805599453E-1 / (p[4] * p[7]);
		dfdxVector[4] = (p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0) / p[7];
		dfdxVector[7] = -6.931471805599453E-1 / (p[4] * p[7]);
		dfdxVector[11] = (p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0) / p[7];
		dfdxVector[14] = -6.931471805599453E-1 / (p[4] * p[7]);
		dfdxVector[15] = (p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0) / p[7];
		dfdxVector[18] = (p[0] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfdxVector[21] = -6.931471805599453E-1 / (p[3] * p[7]);
		dfdxVector[25] = (p[0] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfdxVector[28] = -6.931471805599453E-1 / (p[3] * p[7]);
		dfdxVector[32] = (p[0] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfdxVector[35] = -6.931471805599453E-1 / (p[3] * p[7]);


		jacState.dfPrimedx = jacState.dfdx * jacState.dfdx;
		Eigen::Map<Eigen::VectorXd>& dfPrimedxVector = jacState.dfPrimedxVector;

		dfPrimedxVector[4] += f[0] * (p[1] * p[1])*pow(p[2], p[1])*1.0 / (p[7] * p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 3.0)*pow(x[0] / p[7], p[1] * 2.0 - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0 - f[0] * p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*(p[1] - 1.0)*pow(x[0] / p[7], p[1] - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0;
		dfPrimedxVector[11] += f[1] * (p[1] * p[1])*pow(p[2], p[1])*1.0 / (p[7] * p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 3.0)*pow(x[1] / p[7], p[1] * 2.0 - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0 - f[1] * p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*(p[1] - 1.0)*pow(x[1] / p[7], p[1] - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0;
		dfPrimedxVector[15] += f[2] * (p[1] * p[1])*pow(p[2], p[1])*1.0 / (p[7] * p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 3.0)*pow(x[2] / p[7], p[1] * 2.0 - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0 - f[2] * p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*(p[1] - 1.0)*pow(x[2] / p[7], p[1] - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0;

	}

	virtual void calculateJacAndSensitivities(odeSD::JacobianState& jacState, odeSD::SensitivityState& state)
	{
		calculateJac(jacState, state);
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		state.dfdp.setZero();
		Eigen::Map<Eigen::VectorXd>& dfdpVector = state.dfdpVector;

		dfdpVector[0] = (x[3] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfdpVector[1] = (x[4] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfdpVector[2] = (x[5] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfdpVector[9] = pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*(pow(p[2], p[1])*log(p[2]) + log(x[2] / p[7])*pow(x[2] / p[7], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0 + (pow(p[2], p[1])*log(p[2])*(p[5] * 6.0E1 - p[6] * 6.0E1)) / (pow(p[2], p[1]) + pow(x[2] / p[7], p[1]));
		dfdpVector[10] = pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*(pow(p[2], p[1])*log(p[2]) + log(x[0] / p[7])*pow(x[0] / p[7], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0 + (pow(p[2], p[1])*log(p[2])*(p[5] * 6.0E1 - p[6] * 6.0E1)) / (pow(p[2], p[1]) + pow(x[0] / p[7], p[1]));
		dfdpVector[11] = pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*(pow(p[2], p[1])*log(p[2]) + log(x[1] / p[7])*pow(x[1] / p[7], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0 + (pow(p[2], p[1])*log(p[2])*(p[5] * 6.0E1 - p[6] * 6.0E1)) / (pow(p[2], p[1]) + pow(x[1] / p[7], p[1]));
		dfdpVector[15] = (p[1] * pow(p[2], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)) / (pow(p[2], p[1]) + pow(x[2] / p[7], p[1])) - p[1] * pow(p[2], p[1])*pow(p[2], p[1] - 1.0)*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0;
		dfdpVector[16] = (p[1] * pow(p[2], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)) / (pow(p[2], p[1]) + pow(x[0] / p[7], p[1])) - p[1] * pow(p[2], p[1])*pow(p[2], p[1] - 1.0)*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0;
		dfdpVector[17] = (p[1] * pow(p[2], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)) / (pow(p[2], p[1]) + pow(x[1] / p[7], p[1])) - p[1] * pow(p[2], p[1])*pow(p[2], p[1] - 1.0)*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0;
		dfdpVector[18] = (p[0] * 1.0 / (p[3] * p[3])*x[3] * -6.931471805599453E-1) / p[7];
		dfdpVector[19] = (p[0] * 1.0 / (p[3] * p[3])*x[4] * -6.931471805599453E-1) / p[7];
		dfdpVector[20] = (p[0] * 1.0 / (p[3] * p[3])*x[5] * -6.931471805599453E-1) / p[7];
		dfdpVector[21] = (1.0 / (p[3] * p[3])*x[3] * 6.931471805599453E-1) / p[7];
		dfdpVector[22] = (1.0 / (p[3] * p[3])*x[4] * 6.931471805599453E-1) / p[7];
		dfdpVector[23] = (1.0 / (p[3] * p[3])*x[5] * 6.931471805599453E-1) / p[7];
		dfdpVector[24] = (1.0 / (p[4] * p[4])*x[0] * 6.931471805599453E-1) / p[7];
		dfdpVector[25] = (1.0 / (p[4] * p[4])*x[1] * 6.931471805599453E-1) / p[7];
		dfdpVector[26] = (1.0 / (p[4] * p[4])*x[2] * 6.931471805599453E-1) / p[7];
		dfdpVector[33] = (pow(p[2], p[1])*6.0E1) / (pow(p[2], p[1]) + pow(x[2] / p[7], p[1]));
		dfdpVector[34] = (pow(p[2], p[1])*6.0E1) / (pow(p[2], p[1]) + pow(x[0] / p[7], p[1]));
		dfdpVector[35] = (pow(p[2], p[1])*6.0E1) / (pow(p[2], p[1]) + pow(x[1] / p[7], p[1]));
		dfdpVector[39] = (pow(p[2], p[1])*-6.0E1) / (pow(p[2], p[1]) + pow(x[2] / p[7], p[1])) + 6.0E1;
		dfdpVector[40] = (pow(p[2], p[1])*-6.0E1) / (pow(p[2], p[1]) + pow(x[0] / p[7], p[1])) + 6.0E1;
		dfdpVector[41] = (pow(p[2], p[1])*-6.0E1) / (pow(p[2], p[1]) + pow(x[1] / p[7], p[1])) + 6.0E1;
		dfdpVector[42] = (1.0 / (p[7] * p[7])*x[0] * 6.931471805599453E-1) / p[4] - (p[0] * 1.0 / (p[7] * p[7])*x[3] * 6.931471805599453E-1) / p[3];
		dfdpVector[43] = (1.0 / (p[7] * p[7])*x[1] * 6.931471805599453E-1) / p[4] - (p[0] * 1.0 / (p[7] * p[7])*x[4] * 6.931471805599453E-1) / p[3];
		dfdpVector[44] = (1.0 / (p[7] * p[7])*x[2] * 6.931471805599453E-1) / p[4] - (p[0] * 1.0 / (p[7] * p[7])*x[5] * 6.931471805599453E-1) / p[3];
		dfdpVector[45] = (1.0 / (p[7] * p[7])*x[3] * 6.931471805599453E-1) / p[3] + p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7])*x[2] * 1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1);
		dfdpVector[46] = (1.0 / (p[7] * p[7])*x[4] * 6.931471805599453E-1) / p[3] + p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7])*x[0] * 1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1);
		dfdpVector[47] = (1.0 / (p[7] * p[7])*x[5] * 6.931471805599453E-1) / p[3] + p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7])*x[1] * 1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1);


		state.dfPrimedp = jacState.dfdx * state.dfdp;
		Eigen::Map<Eigen::VectorXd>& dfPrimedpVector = state.dfPrimedpVector;

		dfPrimedpVector[0] += (f[3] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfPrimedpVector[1] += (f[4] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfPrimedpVector[2] += (f[5] * 6.931471805599453E-1) / (p[3] * p[7]);
		dfPrimedpVector[9] += (f[2] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0) / p[7] - (f[2] * p[1] * pow(p[2], p[1])*log(p[2])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0) / p[7] - (f[2] * p[1] * pow(p[2], p[1])*log(x[2] / p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0) / p[7] + (f[2] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 3.0)*pow(x[2] / p[7], p[1] - 1.0)*(pow(p[2], p[1])*log(p[2]) + log(x[2] / p[7])*pow(x[2] / p[7], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0) / p[7];
		dfPrimedpVector[10] += (f[0] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0) / p[7] - (f[0] * p[1] * pow(p[2], p[1])*log(p[2])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0) / p[7] - (f[0] * p[1] * pow(p[2], p[1])*log(x[0] / p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0) / p[7] + (f[0] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 3.0)*pow(x[0] / p[7], p[1] - 1.0)*(pow(p[2], p[1])*log(p[2]) + log(x[0] / p[7])*pow(x[0] / p[7], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0) / p[7];
		dfPrimedpVector[11] += (f[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0) / p[7] - (f[1] * p[1] * pow(p[2], p[1])*log(p[2])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0) / p[7] - (f[1] * p[1] * pow(p[2], p[1])*log(x[1] / p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*1.0) / p[7] + (f[1] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 3.0)*pow(x[1] / p[7], p[1] - 1.0)*(pow(p[2], p[1])*log(p[2]) + log(x[1] / p[7])*pow(x[1] / p[7], p[1]))*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0) / p[7];
		dfPrimedpVector[15] += (f[2] * (p[1] * p[1])*pow(p[2], p[1] - 1.0)*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0) / p[7] + (f[2] * (p[1] * p[1])*pow(p[2], p[1])*pow(p[2], p[1] - 1.0)*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 3.0)*pow(x[2] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0) / p[7];
		dfPrimedpVector[16] += (f[0] * (p[1] * p[1])*pow(p[2], p[1] - 1.0)*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0) / p[7] + (f[0] * (p[1] * p[1])*pow(p[2], p[1])*pow(p[2], p[1] - 1.0)*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 3.0)*pow(x[0] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0) / p[7];
		dfPrimedpVector[17] += (f[1] * (p[1] * p[1])*pow(p[2], p[1] - 1.0)*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*-1.0) / p[7] + (f[1] * (p[1] * p[1])*pow(p[2], p[1])*pow(p[2], p[1] - 1.0)*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 3.0)*pow(x[1] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0) / p[7];
		dfPrimedpVector[18] += (f[3] * p[0] * 1.0 / (p[3] * p[3])*-6.931471805599453E-1) / p[7];
		dfPrimedpVector[19] += (f[4] * p[0] * 1.0 / (p[3] * p[3])*-6.931471805599453E-1) / p[7];
		dfPrimedpVector[20] += (f[5] * p[0] * 1.0 / (p[3] * p[3])*-6.931471805599453E-1) / p[7];
		dfPrimedpVector[21] += (f[3] * 1.0 / (p[3] * p[3])*6.931471805599453E-1) / p[7];
		dfPrimedpVector[22] += (f[4] * 1.0 / (p[3] * p[3])*6.931471805599453E-1) / p[7];
		dfPrimedpVector[23] += (f[5] * 1.0 / (p[3] * p[3])*6.931471805599453E-1) / p[7];
		dfPrimedpVector[24] += (f[0] * 1.0 / (p[4] * p[4])*6.931471805599453E-1) / p[7];
		dfPrimedpVector[25] += (f[1] * 1.0 / (p[4] * p[4])*6.931471805599453E-1) / p[7];
		dfPrimedpVector[26] += (f[2] * 1.0 / (p[4] * p[4])*6.931471805599453E-1) / p[7];
		dfPrimedpVector[33] += (f[2] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*-6.0E1) / p[7];
		dfPrimedpVector[34] += (f[0] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*-6.0E1) / p[7];
		dfPrimedpVector[35] += (f[1] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*-6.0E1) / p[7];
		dfPrimedpVector[39] += (f[2] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*6.0E1) / p[7];
		dfPrimedpVector[40] += (f[0] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*6.0E1) / p[7];
		dfPrimedpVector[41] += (f[1] * p[1] * pow(p[2], p[1])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*6.0E1) / p[7];
		dfPrimedpVector[42] += (f[0] * 1.0 / (p[7] * p[7])*6.931471805599453E-1) / p[4] - (f[3] * p[0] * 1.0 / (p[7] * p[7])*6.931471805599453E-1) / p[3];
		dfPrimedpVector[43] += (f[1] * 1.0 / (p[7] * p[7])*6.931471805599453E-1) / p[4] - (f[4] * p[0] * 1.0 / (p[7] * p[7])*6.931471805599453E-1) / p[3];
		dfPrimedpVector[44] += (f[2] * 1.0 / (p[7] * p[7])*6.931471805599453E-1) / p[4] - (f[5] * p[0] * 1.0 / (p[7] * p[7])*6.931471805599453E-1) / p[3];
		dfPrimedpVector[45] += (f[3] * 1.0 / (p[7] * p[7])*6.931471805599453E-1) / p[3] + f[2] * p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*pow(x[2] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1) - f[2] * (p[1] * p[1])*pow(p[2], p[1])*1.0 / (p[7] * p[7] * p[7])*x[2] * 1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 3.0)*pow(x[2] / p[7], p[1] * 2.0 - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0 + f[2] * p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7] * p[7])*x[2] * 1.0 / pow(pow(p[2], p[1]) + pow(x[2] / p[7], p[1]), 2.0)*(p[1] - 1.0)*pow(x[2] / p[7], p[1] - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1);
		dfPrimedpVector[46] += (f[4] * 1.0 / (p[7] * p[7])*6.931471805599453E-1) / p[3] + f[0] * p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*pow(x[0] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1) - f[0] * (p[1] * p[1])*pow(p[2], p[1])*1.0 / (p[7] * p[7] * p[7])*x[0] * 1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 3.0)*pow(x[0] / p[7], p[1] * 2.0 - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0 + f[0] * p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7] * p[7])*x[0] * 1.0 / pow(pow(p[2], p[1]) + pow(x[0] / p[7], p[1]), 2.0)*(p[1] - 1.0)*pow(x[0] / p[7], p[1] - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1);
		dfPrimedpVector[47] += (f[5] * 1.0 / (p[7] * p[7])*6.931471805599453E-1) / p[3] + f[1] * p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7])*1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*pow(x[1] / p[7], p[1] - 1.0)*(p[5] * 6.0E1 - p[6] * 6.0E1) - f[1] * (p[1] * p[1])*pow(p[2], p[1])*1.0 / (p[7] * p[7] * p[7])*x[1] * 1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 3.0)*pow(x[1] / p[7], p[1] * 2.0 - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1)*2.0 + f[1] * p[1] * pow(p[2], p[1])*1.0 / (p[7] * p[7] * p[7])*x[1] * 1.0 / pow(pow(p[2], p[1]) + pow(x[1] / p[7], p[1]), 2.0)*(p[1] - 1.0)*pow(x[1] / p[7], p[1] - 2.0)*(p[5] * 6.0E1 - p[6] * 6.0E1);

	}


};