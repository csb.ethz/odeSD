function cellNames = matlabIndexedArrayAsCell( arrayName, minIndex, maxIndex )
    cellNames = cell(maxIndex - minIndex + 1, 1);
    for i = minIndex:maxIndex
        cellNames{i} = [arrayName '(' int2str(i) ')'];
    end
end

