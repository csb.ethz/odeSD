function assignIt (names, symVars)
    for j = 1:length(names)
        assignin('caller', names{j}, symVars(j));
    end
end