%% TEST Final (Asymmetric)
folderName = 'ExampleFolder'; %has to be '', not ""
modelName = 'exampleModel'; %has to be '', not ""
symbolic = symbolicModel();
initialValues = ones(size(symbolic.states));
parameterValues = ones(size(symbolic.symParameters));

%% CODE Asymetric 
%requires odeSD installation
%https://csb.ethz.ch/tools/software/odesd.html

if ~exist(folderName, 'dir')
    [~,~,~] = mkdir(folderName);
    model = generateSymbolicRepresentation(symbolic.RHS, modelName,...
        symbolic.states, initialValues,...
        symbolic.symParameters, parameterValues);
    createOdeSdMatlabRhs(model, folderName);
end