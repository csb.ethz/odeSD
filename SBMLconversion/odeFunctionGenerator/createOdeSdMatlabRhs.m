function [files, handles] = createOdeSdMatlabRhs( symModel, targetFolder, options )
    stateVectorIndexed       = matlabIndexedArray('x', length(symModel.stateNames));
    parameterVectorIndexed   = matlabIndexedArray('p', length(symModel.parameterNames));
    stateDiffVectorIndexed   = matlabIndexedArray('f', length(symModel.stateNames));
    
    inputDefinition = 'additionalArgs = varargin(4:end);';
    jacInputDefinitionODESD = '';
    jacInputDefinitionMATLAB = '';
    RHSvariableVector = [symModel.stateVector;symModel.parameterVector];
    RHSvariableVectorIndexed = [stateVectorIndexed;parameterVectorIndexed];
    if isfield(symModel, 'inputNames')
        inputVectorIndexed   = matlabIndexedArray('in', length(symModel.inputNames));
        inputDefinition      = sprintf('in = varargin{4};\n\t\t\tadditionalArgs = varargin(4:end);');
        RHSvariableVector = [RHSvariableVector; symModel.inputVector];
        RHSvariableVectorIndexed = [RHSvariableVectorIndexed; inputVectorIndexed];
        jacInputDefinitionODESD = 'in = varargin{5};';
        jacInputDefinitionMATLAB = 'in = varargin{4};';
    end
    

    
    
    %% Write Files
    assignIt({symModel.timeVariable}, symModel.symTimeVariable);
    
    initialStateString = ['[' fastStrJoin(cellfun(@num2str,num2cell(symModel.initStateVals),'UniformOutput',false), ';') '];'];
    parameterString = ['[' fastStrJoin(cellfun(@num2str,num2cell(symModel.parameterValues),'UniformOutput',false), ';') '];'];
    RHSindexed = subs(vpa(symModel.RHS), RHSvariableVector, RHSvariableVectorIndexed);
    dRHSdtIndexed = subs(vpa(symModel.dRHSdt), [RHSvariableVector;symModel.stateDiffVector], [RHSvariableVectorIndexed;stateDiffVectorIndexed]);
    fString = ['[' fastStrJoin(arrayfun(@char,RHSindexed,'UniformOutput',false), ';\n\t\t') '];'];
    gString = ['[' fastStrJoin(arrayfun(@char,dRHSdtIndexed,'UniformOutput',false), ';\n\t\t') '];'];
    
    if isempty(targetFolder)
        modelFile = [symModel.name '.m'];
        jacFile = ['jac_' symModel.name '.m'];
    else
        if (targetFolder(end) ~= filesep)
            targetFolder = [targetFolder filesep];
        end
        modelFile = [targetFolder symModel.name '.m'];
        jacFile = [targetFolder 'jac_' symModel.name '.m'];
    end
    
    jacName = ['jac_' symModel.name];
    
    templateRoot = [fileparts(mfilename('fullpath')) filesep 'templates' filesep];
    copyfile([templateRoot 'matlabOdeFunctionTemplate.m'], modelFile);
    odeCode = fileread(modelFile);
    
    odeCode = strrep(odeCode, '##inputVariables##', inputDefinition);
    odeCode = strrep(odeCode, '##timeVariable##', char(symModel.timeVariable));
    odeCode = strrep(odeCode, '##odeFunction##', symModel.name);
    odeCode = strrep(odeCode, '##jacFunction##', jacName);
    
    odeCode = strrep(odeCode, '##InitialConditions##', initialStateString);
    odeCode = strrep(odeCode, '##Parameters##', parameterString);
    odeCode = strrep(odeCode, '##fValues##', fString);
    odeCode = strrep(odeCode, '##gValues##', gString);
    
    fileID = fopen(modelFile,'w');
    fprintf(fileID,'%s',odeCode);
    fclose(fileID);
    
    copyfile([templateRoot 'matlabJacFunctionTemplate.m'], jacFile);
    jacCode = fileread(jacFile);
    
    jacCode = strrep(jacCode, '##inputVariablesODESD##', jacInputDefinitionODESD);
    jacCode = strrep(jacCode, '##inputVariablesMATLAB##', jacInputDefinitionMATLAB);
    
    jacCode = strrep(jacCode, '##timeVariable##', char(symModel.timeVariable));
    jacCode = strrep(jacCode, '##jacFunction##', jacName);
    
    jacCode = strrep(jacCode, '##Parameters##', parameterString);
    jacCode = strrep(jacCode, '##nStates##', int2str(length(symModel.stateVector)));
    jacCode = strrep(jacCode, '##nParameters##', int2str(length(symModel.parameterVector)));
    
    syms(['x(ind)']);
    syms(['p(ind)']);
    syms(['f(ind)']);
    if isfield(symModel, 'inputNames')
        syms(['in(ind)']);
    end
    
    assignIt(symModel.stateVectorNames, stateVectorIndexed);
    assignIt(symModel.parameterVectorNames, parameterVectorIndexed);
    assignIt(arrayfun(@char,symModel.stateDiffVector,'UniformOutput', false), stateDiffVectorIndexed);
    if isfield(symModel, 'inputNames')
        assignIt(symModel.inputVectorNames, inputVectorIndexed);
    end

    dRHSdxIndexed = eval(vpa(symModel.dRHSdx));
    dRHSdxString = matlabWriteSymMatrix(dRHSdxIndexed, 'Jfx', false);
    
    dRHSdpIndexed = eval(vpa(symModel.dRHSdp)); 
    dRHSdpString = matlabWriteSymMatrix(dRHSdpIndexed, 'Jfp', false);
        
    d2RHSdtdxIndexed = eval(vpa(symModel.d2RHSdtdx));
    d2RHSdtdxString = matlabWriteSymMatrix(d2RHSdtdxIndexed, 'Jgx', true);
  
    d2RHSdtdpIndexed = eval(vpa(symModel.d2RHSdtdp));
    d2RHSdtdpString = matlabWriteSymMatrix(d2RHSdtdpIndexed, 'Jgp', true);    
    
    jacCode = strrep(jacCode, '##Jfx##', dRHSdxString);
    jacCode = strrep(jacCode, '##Jgx##', d2RHSdtdxString);
    jacCode = strrep(jacCode, '##Jfp##', dRHSdpString);
    jacCode = strrep(jacCode, '##Jgp##', d2RHSdtdpString);
    
    fileID = fopen(jacFile,'w');
    fprintf(fileID,'%s',jacCode);
    fclose(fileID);
    
    modelHandle = str2func(symModel.name);
    jacHandle = str2func(jacName);
    
    files = {modelFile, jacFile};
    handles = {modelHandle, jacHandle};
end

