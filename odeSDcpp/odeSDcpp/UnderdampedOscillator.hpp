#pragma once
#include <string>
#include <Eigen/Dense>
#include <Eigen/SparseCore>
#include <odeSD/DefaultSensitivityModel>


// harmonic oscillator defs with 2 parameters and an analytic solution
// for the underdamped case, beta^2 < 4 * omega^2

// beta = theta[0]
// omega^2 = theta[1]
// y'' + theta[0] * y' + theta[1] * y = 0
//
// y_1' = y_2
// y_2' = -theta[0] * y_1' - theta[1] * y_1
//      = -theta[0] * y_2  - theta[1] * y_1
//
// for theta[0]^2 < 4 * theta[1] the solution is:
// y_1(0) = y(0)
// y_2(0) = y'(0)
// gamma = 0.5 * sqrt(4*theta[1] - theta[0]^2)
// A = y(0)
// B = theta[0] * y(0)/(2*gamma) + y'(0) / gamma
//
// y(t) = exp(- (theta[0]/2) * t)
//        * [A *cos(gamma * t) + B * sin(gamma*t)]
// y_1 = y
// y_2 = y'

class UnderdampedOscillator :
	public odeSD::DefaultSensitivityModel
{
public:
	odeSD::SensitivityState initialState;


	UnderdampedOscillator() :
		initialState(2, 2)
	{
		initialState.x << 0.0, 5.0;
		

		initialState.s.setZero();

		parameters = Eigen::VectorXd(initialState.nParameters);
		parameters << 0.5, 1.0;

		tspan = Eigen::VectorXd(101);
		for (Eigen::DenseIndex i = 0; i < tspan.rows(); i++)
		{
			tspan(i) = i * 25.0 / (tspan.rows() - 1.0);
		}
		states = Eigen::MatrixXd(initialState.nStates, tspan.rows());
		sensitivities = Eigen::MatrixXd(initialState.nStates * initialState.nParameters, tspan.rows());
	}

	odeSD::SensitivityState& getInitialState()
	{
		return initialState;
	}

	// time, current state, f, f'
	virtual void calculateRHS(odeSD::State& state)
	{
		// beta = theta[0]
		// omega^2 = theta[1]
		// y'' + theta[0] * y' + theta[1] * y = 0
		//
		// y_1' = y_2
		// y_2' = -theta[0] * y_1' - theta[1] * y_1
		//      = -theta[0] * y_2  - theta[1] * y_1

		state.f(0) = state.x(1);
		state.f(1) = -parameters(0) * state.x(1) - parameters(1) * state.x(0);

		state.fPrime(0) = state.f(1);
		state.fPrime(1) = -parameters(0) * state.f(1) - parameters(1) * state.f(0);
	}

	virtual void calculateJac(odeSD::JacobianState& jacState, odeSD::State& state)
	{
		jacState.dfdx.setZero();
		jacState.dfdx(0, 1) = 1.0;
		jacState.dfdx(1, 0) = -parameters(1);
		jacState.dfdx(1, 1) = -parameters(0);

		jacState.dfPrimedx.setZero();
		jacState.dfPrimedx += jacState.dfdx * jacState.dfdx;
	}


	virtual void calculateJacAndSensitivities(odeSD::JacobianState& jacState, odeSD::SensitivityState& state)
	{
		calculateJac(jacState, state);

		state.dfdp.setZero();
		state.dfdp(1, 0) = -state.x(1);
		state.dfdp(1, 1) = -state.x(0);

		state.dfPrimedp.setZero();
		state.dfPrimedp(1, 0) = -state.f(1);
		state.dfPrimedp(1, 1) = -state.f(0);
		state.dfPrimedp += jacState.dfdx * state.dfdp;
	}


	virtual ~UnderdampedOscillator()
	{
	}
};

// harmonic oscillator defs with 2 parameters and an analytic solution
// for the underdamped case, beta^2 < 4 * omega^2

// beta = theta[0]
// omega^2 = theta[1]
// y'' + theta[0] * y' + theta[1] * y = 0
//
// y_1' = y_2
// y_2' = -theta[0] * y_1' - theta[1] * y_1
//      = -theta[0] * y_2  - theta[1] * y_1
//
// for theta[0]^2 < 4 * theta[1] the solution is:
// y_1(0) = y(0)
// y_2(0) = y'(0)
// gamma = 0.5 * sqrt(4*theta[1] - theta[0]^2)
// A = y(0)
// B = theta[0] * y(0)/(2*gamma) + y'(0) / gamma
//
// y(t) = exp(- (theta[0]/2) * t)
//        * [A *cos(gamma * t) + B * sin(gamma*t)]
// y_1 = y
// y_2 = y'
