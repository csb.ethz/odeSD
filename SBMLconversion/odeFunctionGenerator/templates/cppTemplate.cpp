#pragma once
#include <string>
#include <Eigen/Dense>
#include <odeSD/DefaultSensitivityModel>

class ##ModelName##Model :
	public odeSD::DefaultSensitivityModel
{
public:
	odeSD::SensitivityState initialState;

	virtual void observeIntermediateSensitivityState(Eigen::DenseIndex stepIndex, const odeSD::SensitivityState& state)
	{
		return;
	}

	virtual void observeIntermediateState(Eigen::DenseIndex stepIndex, const odeSD::State& state)
	{
		return;
	}

	##ModelName##Model() :
		initialState(##nStates##, ##nParameters##)
	{
		initialState.x << ##InitialConditions##;
		
		initialState.s.setZero();

		parameters = Eigen::VectorXd(initialState.nParameters);
		parameters << ##Parameters##;

		tspan = Eigen::VectorXd(200);
		for (Eigen::DenseIndex i = 0; i < tspan.rows(); i++)
		{
			tspan(i) = i * ##tEnd## / (tspan.rows() - 1.0);
		}

		states = Eigen::MatrixXd(initialState.nStates, tspan.rows());
		sensitivities = Eigen::MatrixXd(initialState.nStates * initialState.nParameters, tspan.rows());
	}

	odeSD::SensitivityState& getInitialState()
	{
		return initialState;
	}
    
	// time, current state, f, f'
	virtual void calculateRHS(odeSD::State& state)
	{
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& dfdt = state.fPrime;
		Eigen::Map<Eigen::VectorXd>& x = state.x;
        
		/***********************************************
		* 1st DERIVATIVE EQUATIONS (f)
		**********************************************/

##fValues##

		/***********************************************
		* 2nd DERIVATIVE EQUATIONS (Jfx*f = g)
		**********************************************/

##gValues##
	}

	virtual void calculateJac(odeSD::JacobianState& jacState, odeSD::State& state)
	{
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		jacState.dfdx.setZero();
		Eigen::Map<Eigen::VectorXd>& dfdxVector = jacState.dfdxVector;

##dfdxVector##

		jacState.dfPrimedx = jacState.dfdx * jacState.dfdx;
		Eigen::Map<Eigen::VectorXd>& dfPrimedxVector = jacState.dfPrimedxVector;
        
##dfPrimedxVector##
	}

	virtual void calculateJacAndSensitivities(odeSD::JacobianState& jacState, odeSD::SensitivityState& state)
	{
		calculateJac(jacState, state);
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		state.dfdp.setZero();
		Eigen::Map<Eigen::VectorXd>& dfdpVector = state.dfdpVector;

##dfdpVector##

		state.dfPrimedp = jacState.dfdx * state.dfdp;
		Eigen::Map<Eigen::VectorXd>& dfPrimedpVector = state.dfPrimedpVector;

##dfPrimedpVector##
	}


};
