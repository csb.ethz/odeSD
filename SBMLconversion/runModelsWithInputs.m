clear all
clc

ModelNames = { 'Model_Llamos2.txt',...
               'Endocytosis_model.txt'};
ModelInputs = { {'t_in'}, ...
    {'mu_C0','mu_M0','tm'} };

ModelNonDifferentiableInputFunctions = { {'u_c'}, ...
    {'mu_C','mu_M','u_in'} };

ModelInputValues = { 30,...
    [ 0.00217; 0.00167;9]};
           
% Ensure we are in the correct folder
cd(fileparts(mfilename('fullpath')));

if ~exist('SBMLout', 'dir')
    mkdir('SBMLout');
end


addpath([fileparts(mfilename('fullpath')) filesep 'SBMLout']);
addpath([fileparts(mfilename('fullpath')) filesep 'odeFunctionGenerator' filesep 'piecewiseIQMpatch']);
files = {};
handles = {};
models = {};
%% Convert all models
for i = 1:length(ModelNames)
    sbmlFileName = ['IQMmodels/' ModelNames{i}];
    tic
    [currentFiles, currentHandles, currentModel] = generateRhs(sbmlFileName, {'matlab'}, 'SBMLout', struct('inputs',ModelInputs(i), 'doNotSubstitute', ModelNonDifferentiableInputFunctions(i)));
    toc
    files{end+1} = currentFiles;
    handles{end+1} = currentHandles;
    models{end+1} = currentModel;
end
%%
for i = 1
    modelName = models{i}.name;
    
    modelHandle = handles{i}{1};
    jacHandle = handles{i}{2};
    
    
    [x0, p0] = modelHandle();
    x0 = x0 + 1e-10;
    in = ModelInputValues{i};
    
    disp(modelName);
    
    tic
    [T, X] = odeSD(modelHandle, linspace(0,100,101), x0, odeset('Jacobian', jacHandle, 'RelTol', 1e-7,'MaxStep',0.1), p0, in);
    toc
    figure;
    subplot(2,1,1);
    plot(T,X);
    title(['odeSD on ' modelName]);
        
    tic
    [T, X] = ode15s(modelHandle, linspace(0,100,101), x0, odeset('Jacobian', jacHandle, 'RelTol', 1e-6), p0, in);
    toc
    subplot(2,1,2);
    plot(T,X);  
    title('ode15s');
end
