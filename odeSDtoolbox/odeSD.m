function [ T , Y , TE , YE , IE , S , SE ] = odeSD ( f , t_span , y0 , options , params , varargin )
% ODESD Solve an ODE using a second-derivative rule.
% 
%    [T,Y] = ODESD(F,TSPAN,Y0,OPTS) integrates the ordinary differential
%    equiation defined by
%
%       dY/dT = F(T,Y)
%
%    From time TSPAN(1) to TSPAN(end), using the initial values Y(TSPAN(1))=Y0.
%    The right-hand side F(T,Y) must be of the form
%
%       [ DXDT, D2XDT2, J_F, J_JF ] = F ( T, X )
%
%    where DXDT and D2XDT2 are the first and second derivatives of X with
%    respect to T and J_F and J_JF are the Jacobian matrices of these
%    two values with respect to X.
%
%    The OPTS parameter is a structure created and modified with the ODESET
%    command. It contains parameters to the integrator such as the relative
%    and absolute tolerances. ODESD requires the option "Jacobian" to be
%    set to a function of the type
%
%       [ J_F, J_JF ] = J ( T, X, P, DXDT )
%
%    which returns the same Jacobians as F. If TSPAN is of length 2, the output
%    variables T and Y will contain the time steps taken by the integrator
%    and the system variables at those times respectively. If TSPAN is of
%    length greater than 2, only the times and values at the times specified
%    in TSPAN are returned. The system variables at the kth time step are
%    stored in the kth row of Y.
%
%    [T,Y,TE,YE,IE] = ODESD(F,TSPAN,Y0,OPTS) when the 'Events' property has
%    been set in OPTS, returns the times TE at which events were triggered,
%    the system variables YE at those times, as well as the indices IE of the
%    variables that triggered the events. For a detailed description of
%    the 'Events' property, see the documentation for ODE15S.
%
%    [T,Y,TE,YE,IE,S,SE] = ODESD(F,TSPAN,Y0,OPTS,P) computes the parameter
%    sensitivities S, i.e. the derivatives of Y with respect to the parameters
%    P. If the output S is requested, the right-hand side F must be of the form
%
%       [ DXDT, D2XDT2, J_F, J_JF , DFDP , D2FDTDP ] = F ( T, X, P )
% 
%    where DFDP ad D2FDTDP are matrices containing the derivatives of 
%    DXDT and D2XDT2 (columns), respectively, with respect to each parameter
%    P (rows). If any events are specified via the 'Events' property in
%    OPTS, the values of the parameter sensitivities at the events are sored
%    in the ouput variable SE. The parameter sensitivities S and SE are stored
%    in a three dimensional array in which S(I,J,K) contains the derivative
%    of the Ith system variable with respect to the Jth parameter at the 
%    Kth time step. 
%
%    Reference:
%      P. Gonnet, S. Dimopoulos, L. Widmer and J. Stelling, "A Specialized ODE
%      Integrator for the Efficient Computation of Parameter Sensitivities",
%      Submitted to BMC Systems Biology, 2011.
%
%    See also 
%      other ODE solvers:    ODE15S, ODE23S, ODE23T, ODE23TB, ODE45, ODE23
%      implicit ODEs:        ODE15I
%      options handling:     ODESET, ODEGET
%      output functions:     ODEPLOT, ODEPHAS2, ODEPHAS3, ODEPRINT
%      function handles:     FUNCTION_HANDLE

% This file is part of odeSD.
% Coypright (c) 2011 Pedro Gonnet (gonnet@maths.ox.ac.uk), Lukas Widmer,
% Sotiris Dimopoulos and J??rg Stelling.
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

 

    
    %% Solver Initalization
    % Parameters for interpolation polynomial
    a1 = 1/2;
    a2 = 1/2;
    b1 = 1/12;
    b2 = -1/12;
    

    %% Options:
    if nargin < 4, options = []; end;
    RelTol = odeget(options,'RelTol',1.0e-6,'fast');
    AbsTol = odeget(options,'AbsTol',1.0e-6,'fast');
    NormControl = strcmp(odeget(options,'NormControl',[],'fast'),'on');
    NonNegative = odeget(options,'NonNegative',[],'fast');
    OutputFcn = odeget(options,'OutputFcn',[],'fast');
    OutputSel = odeget(options,'OutputSel',(1:length(y0)),'fast');
    Refine = odeget(options,'Refine',1,'fast');
    Stats = strcmp(odeget(options,'Stats','off','fast'),'on');
    h = odeget(options,'InitialStep',(t_span(end)-t_span(1)) / 100,'fast');
    MaxStep = odeget(options,'MaxStep',Inf,'fast');
    Events = odeget(options,'Events',[],'fast');
    maxRuntime = odeget(options, 'maxRuntime', [], 'fast'); %extra to limit the solver runtime
    df = odeget(options,'Jacobian',[],'fast');
    if ischar(df), df = str2func(df); end;
    
    useKLU = false; %(exist('klu', 'file') == 3);
    if useKLU
        kluOpts = struct('ordering', 0);
    end
    
    %% Did we get a Jacobian?
    if isempty( df )
        error('odeSD:NoJacobian','The fourth parameter ''options'' must specify a Jacobian function!');
    end
    
    %% Set up warnings
    warnSing = warning( 'off' , 'MATLAB:singularMatrix' );
    warnAlmostSing = warning( 'off' , 'MATLAB:nearlySingularMatrix' );
    
    
    %% Set up time steps
    % Start and end time
    t_start = t_span(1);
    t_end = t_span(end);
    t_len = length(t_span);
    tid = 2;


    %% Set up stats
    stats_steps_ok = 0;
    stats_steps_failed = 0;
    stats_iterations_failed = 0;
    stats_iterations_ok = 0;
    stats_iteration_steps = 0;
    stats_inversions = 0;
   
    %% Start Point

    % output function?
    if ~isempty(OutputFcn), OutputFcn( t_span , y0(OutputSel) , 'init' ); end;
    
    % Set up start point
    t = t_start;
    y = y0;
    
    % Events?
    TE = []; YE = []; IE = [];
    if ~isempty(Events)
        vals_old = Events( t , y );
    end;
            
    %% Init some useful variables
    n = length(y);
    if nargin >= 5, nparam = length(params); else nparam = 0; end;
    doparams = nargout > 5;
	if n > 100, Jeye = speye(n); else Jeye = eye(n); end;
    max_iter = 50;
    alpha = zeros( n , 5 );

    % Insert start point into solution matrices
    T = zeros(100,1); T(1) = t;
    Y = zeros(100,n); Y(1,:) = y';
    nrsteps = 1; nrpoints = 1;
    
    %% Loop Initialization
    if nparam > 0
        [dy, dfdy, J_new , dJ_new , dfdp , d2fdtdp] = f( t , y , params , varargin{:} );
        if any(~isfinite(dy)) || any(~isfinite(dfdy))
            error('odeSD:NaN',[ 'There are non-finite (+/-Inf or NaN) values in the right-hand side. ' ...
                  'Please make sure that your initial values, parameters or righ-hand side ' ...
                  'functions are correct.' ]);
        end
        if doparams
            if isstruct(options) && isfield(options, 'initialSensitivities')
                if any(size(options.initialSensitivities) ~= [n nparam])
                    error('odeSD:wrongSensitivitySize', 'The initial sensitivity matrix size must be nStates x nParameters.');
                end
                s = options.initialSensitivities;
            else
                s = zeros(n, nparam);
            end
            
            dsdt = dfdp;
            d2sdt2 = d2fdtdp;
            S = reshape( s , n , nparam , 1 ); SE = [];
        end
    else
        [ dy, dfdy, J_new, dJ_new ] = f( t , y , varargin{:} );
        if any(~isfinite(dy)) || any(~isfinite(dfdy))
            error('odeSD:NaN',[ 'There are non-finite (+/-Inf or NaN) values in the right-hand side. ' ...
                  'Please make sure that your initial values or righ-hand side functions are correct.' ]);
        end
    end
    if issparse( J_new ), Jeye = speye(n); else Jeye = eye(n); end;
    recompJ = false;
    ageJ = 0; 
    extrap_coeff = [ dfdy*h^2/2 - h*dy + y , (dy - dfdy*h)*h , dfdy*h^2/2 , zeros(length(y),1) ];
    b=1;
    h_LU = 0; ageLU = Inf;
    
    
    
    %% Make sure the first step is sane
    if ~isempty( NonNegative )
    
        % loop...
        while true
        
            % compute y_new
            y_new = extrap_coeff * ( 1 + b ).^(0:3)';
        
            % are all values positive?
            if all( y_new( NonNegative ) >= 0 )
                break;
                
            % otherwise, scale the step...
            else
                h = 0.7 * h;
                b = 0.7 * b;
            end;
            
            % is h still reasonable?
            if h < eps*t || h < eps*(t_end-t)
                error('odeSD:MinStep','minimal stepsize reached (h=%e), aborting!',h);
            end;
        
        end; % loop
    
    end;

    
    %% Main Loop
    
    startTime = tic(); %extra to limit the solver runtime
    checkRuntime = isstruct(options) && isfield(options, 'maxRuntime'); %extra to limit the solver runtime
    
    while ( t < t_end )
        
        if checkRuntime && toc(startTime) > maxRuntime %extra to limit the solver runtime
            error('odeSD:outOfTime','Runtime limit exceeded.'); %extra to limit the solver runtime
        end %extra to limit the solver runtime
        
        % truncate the step size for the final step?
        if t + h >= t_end
            b = b * (t_end - t) / h;
            h = t_end - t;
        end
        if h > MaxStep
            b = b * MaxStep/h; 
            h = MaxStep;
        end;
                
        %% Iteration
        % Interpolation using the last two points and their derivatives:
        y_new = extrap_coeff * (1+b).^(0:3)';
        
        % LW Patch: Check for negative values
        if ~isempty(NonNegative)
            nid = NonNegative( y_new( NonNegative ) < 0 );
            y_new(nid) = 0;
        end;
        
        % Newton loop
        iter = 1;
        % recompJ = recompJ || ( ageJ > 0 );
        recompLU = ( h ~= h_LU ); % ( abs((h-h_LU)/h_LU) > 0.05 ); % || ( ageLU > 10 ); 
        tol = norm(y_new,Inf) * eps * n * 10;
        while ( 1 )
            
            % does h make any sense?
            if h < eps*t || h < eps*(t_end-t)
                error('odeSD:MinStep','minimal stepsize reached (h=%e), aborting!',h);
            end;
            
            % increase the step counter
            stats_iteration_steps = stats_iteration_steps + 1;
            
            % compute dy_new and dfdy_new
            if nparam > 0
                [dy_new, dfdy_new] = f( t+h , y_new , params , varargin{:} );
            else
                [dy_new, dfdy_new] = f( t+h , y_new , varargin{:} );
            end;
            
            % compute the fit error
            err = y + h*(a1*dy + a2*dy_new) + h^2*(b1*dfdy + b2*dfdy_new) - y_new;
            % relwt = max( 1 ./ max( abs( y ) , abs( y_new ) ) , RelTol / AbsTol );
            relwt = 1 ./ max( max( abs( y ) , abs( y_new ) ) , tol );
            relerr = norm( err .* relwt , Inf );
            
            % have we converged?
            % if ( norm(err,Inf) < tol )
            if ( relerr < eps * 10 )
                if ~exist('J_F', 'var')
                    if nparam > 0
                        [J_new, dJ_new] = df( t+h , y_new , dy_new, params, varargin{:} );
                    else
                        [J_new, dJ_new] = df( t+h , y_new , dy_new, varargin{:} );
                    end;
                    ageJ = 0;
                    J_F = h*a2*J_new + h^2*b2*dJ_new - Jeye;
                    
                    if issparse(J_F)
                        
                        if useKLU
                            LU = klu(J_F, kluOpts);
                        else
                            [L, U, P, Q, R] = lu(J_F); %LW PATCH
                        end
                        
                    else
                        [L, U, p] = lu(J_F,'vector');
                    end;
                    %disp(sprintf('odeSD: re-computing LU-decomposition at t=%e, h=%e',t,h));
                    h_LU = h; ageLU = 0;
                    stats_inversions = stats_inversions + 1;
                end;
                    
                %disp(sprintf('odeSD: step with t=%e, h=%e, relerr=%e, iter=%i',t,h,relerr,iter));
                break;
            end;
            
            stepped = false;
            while ( true )
            
                % compute the rhs with or without the jacobian
                if iter == 1 && ( recompJ || recompLU )
                    if recompJ
                        %disp(sprintf('odeSD: re-computing jacobian at t=%e, h=%e',t,h));
                        % J_old = J_new; dJ_old = dJ_new;
                        if nparam > 0
                            [J_new, dJ_new] = df( t+h , y_new , dy_new, params, varargin{:} );
                        else
                            [J_new, dJ_new] = df( t+h , y_new , dy_new, varargin{:} );
                        end;
                        ageJ = 0;
                    end;
                    J_F = h*a2*J_new + h^2*b2*dJ_new - Jeye;
                    % condJF = cond(J_F);
                    
                    if issparse(J_F)
                        
                        if useKLU
                            LU = klu(J_F, kluOpts);
                        else
                            [L, U, P, Q, R] = lu(J_F); %LW PATCH
                        end
                        
                    else
                        [L, U, p] = lu(J_F,'vector');
                    end;
                    %disp(sprintf('odeSD: re-computing LU-decomposition at t=%e, h=%e',t,h));
                    h_LU = h; ageLU = 0;
                    stats_inversions = stats_inversions + 1;
                end
                
%                 if condJF > 1e9 % TODO: Test for ill-conditioned systems and warn the user
%                     recompJ = true;
%                     h = 0.1*h;
%                     b = b * 0.1;
%                     y_new = extrap_coeff * (1+b).^(0:3)';
%                     
%                     warning('Ill-conditioned at %g!', t);
%                     break;
%                         
%                 end

                % compute the step
                if issparse(J_new)
                    if useKLU
                        delta = klu(LU, '\', err, kluOpts);
                    else
                        delta = Q * (U \ (L \ (P * (R \ err))));
                    end
                    
                else
                    delta = U \ ( L \ err(p) );
                end;
                ndelta = norm( delta .* relwt , Inf );

                % compute the convergence rate
                if iter > 1
                    r = ndelta ./ ndelta_old;
                else
                    r = 1.0;
                end;

                % check for slow convergence
                % if ~firststep && ( log(norm(y_new)/ndelta_old) / log(norm(y_new)/ndelta) < 1.0 || iter > 10 )
                if ( iter > 1 && ( ndelta > 0.9*ndelta_old || ndelta * r^(max_iter-iter) > 0.5*RelTol ) ) || ( iter > max_iter )
                    %disp(sprintf('odeSD: slow convergence at t=%e, h=%e, r=%e, ||delta||=%e, iter=%i',t,h,r,ndelta,iter));
                    iter = 1;
                    stats_iterations_failed = stats_iterations_failed + 1;
                    if recompJ
                        h = 0.7*h;
                        b = b * 0.7;
                        y_new = extrap_coeff * (1+b).^(0:3)';
                        % LW Patch: Check for negative values
                        if ~isempty(NonNegative)
                            nid = NonNegative( y_new( NonNegative ) < 0 );
                            y_new(nid) = 0;
                        end;
                        break;
                    else
                        recompJ = true;
                        continue;
                    end;
                end;
                
                % nothing good or bad happened, update y_new and just break
                y_new = y_new - delta;
                
                % LW Patch: Check for negative values
                if ~isempty(NonNegative)
                    nid = NonNegative( y_new( NonNegative ) < 0 );
                    y_new(nid) = 0;
                end;
                stepped = true;
                break;
            
            end;
            
            % if nothing failed...
            if stepped

                % have we converged?
                if ( iter > 1 && r / ( 1 - r ) * ndelta < 0.5*RelTol ) || ( iter == 1 && ndelta < 0.5*RelTol )
                    %disp(sprintf('odeSD: newton iteration converged at t=%e, h=%e (ndelta=%e, relerr=%e, r=%e ,iter=%i)',t,h,ndelta,relerr,r,iter));
                    % [dy_new, dfdy_new] = f( t+h , y_new , varargin{:} );
                    stats_iterations_ok = stats_iterations_ok + 1;
                    dy_new = dy_new - J_new*delta;
                    dfdy_new = dfdy_new - dJ_new*delta;
                    break;
                end;

                % patch up some things before we go into the next iteration
                ndelta_old = ndelta;
                iter = iter + 1;
                
            end;
            
        end; % Newton iteration loop
        
        % reset the iteration flags
        recompJ = false;
        ageJ = ageJ + 1;
        ageLU = ageLU + 1;

         
        %% set negative values of y_new to zero
        if ~isempty(NonNegative)
            nid = NonNegative( y_new( NonNegative ) < 0 );
            y_new(nid) = 0;
        end;
                
        %% Choose the right error estimator
        if t == t_start
        
            % Error Estimation using Chebyshev Polynomials,
            % computes the absolute sum of the Chebyshev coefficients of the
            % difference between a degree 3 and a degree 5 interpolation.
            h2 = h * h; 
            t9 = 3.0/256.0*h;
            t8 = h2/512.0;
            t11 = (dy_new+dy)*h;
            t10 = (dy-dy_new)*h;
            t7 = (dfdy+dfdy_new)*h2;
            t6 = (dfdy-dfdy_new)*h2;
            t5 = dy_new*t9;
            t4 = dfdy_new*t8;
            t2 = dy*t9;
            t1 = dfdy*t8;
            t19 = abs(t2-t5+3.0/512.0*t7);
            t23 = abs(t2-t4+t5+t1-3.0/128.0*y_new+3.0/128.0*y);
            t27 = abs(t10/64.0+t7/128.0);
            t33 = abs(9.0/512.0*t11-9.0/256.0*y_new+9.0/256.0*y+3.0/1024.0*t6);
            t36 = abs(t1+t4+t10/256.0);
            t42 = abs(t6/1024.0+3.0/512.0*t11-3.0/256.0*y_new+3.0/256.0*y);
            err = t19+t23+t27+t33+t36+t42;
            
        else
        
            % Error estimation using step extrapolation
            t8 = h^2; t5 = h_last^2; t17 = 1/((t5+t8+2*h*h_last)*h_last^2); t2 = t5^2; t4 = t5*h_last; t18 = t8*t5;
            t22 = 2/5*t18+4/3*h*t4+1/2*t2; t6 = t8^2; t21 = h*t2; t19 = -1/5*t8; t16 = t8*t4; t11 = h*t8; t15 = t5*t11;
            w_dy = (h+((t11-2*t4)*t19+(1/6*t6-t22)*h)*t17);
            w_dfdy = (1/2*t8+((-t11*h_last-2*t18)*t19+(-4/5*t16-3/4*t15-1/6*t6*h_last-5/12*t21)*h)*t17);
            w_dy_new = ((-2/5*t16+t22*h)*t17);
            w_dfdy_new = ((1/5*t6*t5+(-2/15*t16-1/4*t15-1/12*t21)*h)*t17);
            w_dy_last = (1/30*t11*t8*t17);
            F = -y_new + y + w_dy*dy + w_dfdy*dfdy + w_dy_new*dy_new + w_dfdy_new*dfdy_new + w_dy_last*dy_last;
            % w1 = 1/30*h*(12*h^2+15*h_last^2+28*h*h_last)/(h^2+2*h*h_last+h_last^2);
            % w2 = -1/60*(3*h+5*h_last)*h^2/(h+h_last);
            % [dy_new, dfdy_new, J_new, dJ_new] = f( t+h , y_new , varargin{:} );
            % err = 2 * abs( ( -Jeye + w1*J_new + w2*dJ_new ) \ F );
            if issparse(J_F)
                if useKLU
                    err = abs(klu(LU, '\', F, kluOpts));
                else
                    err = abs( Q * (U \ (L \ (P * (R \ F)))) );
                end
                
            else
                err = abs( U \ ( L \ F(p) ) );
            end;
            
        end;
        
        %% Pass/Fail of the error estimate?
        if NormControl
            tol = max( RelTol*norm(y_new) , AbsTol );
            scale = ( tol / norm(err) / 2 )^(1/5);
        else
            tol = max( RelTol*abs(y_new) , AbsTol );
            scale = min( ( tol ./ err / 2 ).^(1/5) );
        end;

        % fail error tests?
        if scale < 0.5^(1/5)
            
            % fess-up to not having made it
            %disp(sprintf('odeSD: error test failed on vars at t=%e, h=%e, scale=%e',t,h,scale));
                
            % limit decrease
            if scale < 0.1
                scale = 0.1;
                recompJ = true;
            elseif scale > 0.8
                scale = 0.8; 
            end

            % Error too large -> Reduce step size
            h = h * scale;
            b = b * scale;
            % recompJ = true;
            stats_steps_failed = stats_steps_failed + 1;
            
            % carry on
            continue;
            
        end;
            
        
        %% Compute the Parameter Sensitivities if required
        if doparams
            
            % compute the Jacobians and the parameter derivatives at the converged solution
            [J_new, dJ_new, dfdp_new , d2fdtdp_new ] = df( t+h , y_new , dy_new, params, varargin{:} );
            ageJ = 0; recompJ = false;
            
            % get the iteration matrix
            J_F = h*a2*J_new + h^2*b2*dJ_new - Jeye;
            %spy(J_F);drawnow;pause;
            % decompose the iteration matrix
            if issparse(J_F)
                if useKLU
                    LU = klu(J_F, kluOpts);
                else
                    [L, U, P, Q, R] = lu(J_F);
                end               
            else
                [L, U, p] = lu(J_F,'vector');
            end;
            h_LU = h; ageLU = 0;
            stats_inversions = stats_inversions + 1;
            
            % compute the right-hand side
            F = s + h/2*( dsdt + dfdp_new ) + h^2/12*( d2sdt2 - d2fdtdp_new );
            
            % compute the sensitivities
            if issparse(J_F)
                if useKLU
                     s_new = klu (LU,'\',-F,kluOpts);
                else
                    s_new = Q * (U \ (L \ (P * (R \ -F))));
                end                
            else
                s_new = U \ ( L \ -F(p,:) );
            end;
            dsdt_new = J_new * s_new + dfdp_new;
            d2sdt2_new = dJ_new * s_new + d2fdtdp_new;
            
            % compute the integation error in the sensitivities
            if t == t_start
                
                t9 = 3.0/256.0*h;
                t8 = h2/512.0;
                t11 = (dsdt_new+dsdt)*h;
                t10 = (dsdt-dsdt_new)*h;
                t7 = (d2sdt2+d2sdt2_new)*h2;
                t6 = (d2sdt2-d2sdt2_new)*h2;
                t5 = dsdt_new*t9;
                t4 = d2sdt2_new*t8;
                t2 = dsdt*t9;
                t1 = d2sdt2*t8;
                t19 = abs(t2-t5+3.0/512.0*t7);
                t23 = abs(t2-t4+t5+t1-3.0/128.0*s_new+3.0/128.0*s);
                t27 = abs(t10/64.0+t7/128.0);
                t33 = abs(9.0/512.0*t11-9.0/256.0*s_new+9.0/256.0*s+3.0/1024.0*t6);
                t36 = abs(t1+t4+t10/256.0);
                t42 = abs(t6/1024.0+3.0/512.0*t11-3.0/256.0*s_new+3.0/256.0*s);
                perr = t19+t23+t27+t33+t36+t42;

                % loop over each parameter
                for i=1:nparam

                    % step scaling
                    if NormControl
                        tol = max( RelTol*norm(s_new(:,i)) , AbsTol ); %% Again only look 
                        scale = min( scale , ( tol / norm(perr(:,i)) / 2 )^(1/5) );
                    else
                        tol = max( RelTol*norm(s_new(:,i),Inf) , AbsTol );
                        scale = min( scale , min( ( tol ./ perr(:,i) / 2 ) )^(1/5) ); %% LOOK here
                    end;

                end;
                
            else
                
                % Error estimation using step extrapolation (coefficients computed above)
                F = -s_new + s + w_dy * dsdt + w_dfdy * d2sdt2 + w_dy_new * dsdt_new + w_dfdy_new * d2sdt2_new + w_dy_last * dsdt_last;

                if issparse(J_F)
                    if useKLU
                        perr = klu(LU, '\', F,kluOpts);
                    else
                        perr = abs( Q * (U \ (L \ (P * (R \ F)))) );
                    end
                else
                    perr = abs( U \ ( L \ F(p,:) ) );
                end;
                
                % step scaling
                if NormControl
                    
                    % loop over each parameter
                    for i=1:nparam
                        tol = max( RelTol*norm(s_new(:,i)) , AbsTol );
                        scale = min( scale , ( tol / norm(perr(:,i)) / 2 )^(1/5) );
                    end;
                    
                else
                    
                    % compute everything at once
                    tol = max( RelTol*max( abs(s_new) , [] , 1 ) , AbsTol );
                    scale = min( scale , ( max( max( perr , [] , 1 ) ./ tol ) * 2 )^(-1/5) );
                    
                end;

            end;

            % did all go well?
            if scale < 0.5^(1/5)
                
                % be up-front about our failure
                %disp(sprintf('odeSD: error test failed on params at t=%e, h=%e, scale=%e',t,h,scale));
                
                % limit decrease
                if scale < 0.1
                    scale = 0.1;
                    recompJ = true;
                elseif scale > 0.8
                    scale = 0.8;
                end

                % Error too large -> Reduce step size
                h = h * scale;
                b = b * scale;
                % recompJ = true;
                stats_steps_failed = stats_steps_failed + 1;

                % carry on
                continue;

            end;
                
        end;
        
        
        %% Apply step
        if scale > 2
            scale = 2;
        elseif abs( scale - 1 ) < 0.05 % && ageLU < 10
            scale = 1;
        end;
        % disp(sprintf('odeSD: taking step at t=%e with h=%e',t,h));
        
        
        % Calculate all intermediate steps
        if ( t_len > 2 && tid <= t_len && t+h >= t_span(tid) ) || Refine > 1
            % Interpolate all intermediate steps at once
            tees = [];
            while tid <= t_len && t+h >= t_span(tid)
                tees = [ tees ; (-1) + 2*(t_span(tid) - t)/h ];
                tid = tid + 1;
            end
            if Refine > 1
                tees = [ tees ; 2*(1:Refine-1)'/Refine - 1 ];
            end;
            nrtees = length(tees);
            Vx = ones( nrtees , 5 ); Vx(:,2) = tees;
            for i = 3:5, Vx(:,i) = 2*tees.*Vx(:,i-1)-Vx(:,i-2); end;
            
            h2 = h * h;
            t9 = (-dy-dy_new)*h;
            t10 = (-dy+dy_new)*h;
            t16 = (dfdy_new+dfdy)*h2;
            t15 = (dfdy-dfdy_new)*h2;
            t8 = 0.1953125E-2*h2;
            t7 = dfdy*t8;
            t5 = dfdy_new*t8;
            alpha(:,1) = 0.5*y_new+0.5*y-0.7421875E-1*t10+0.5859375E-2*t16;
            alpha(:,2) = -1.0*t7+t5+0.4296875E-1*t9+0.5859375*y_new-0.5859375*y;
            alpha(:,3) = 0.78125E-1*t10-0.78125E-2*t16;
            alpha(:,4) = 0.29296875E-2*t15-0.48828125E-1*t9+0.9765625E-1*y-0.9765625E-1*y_new;
            alpha(:,5) = t5+t7-0.390625E-2*t10;
            
            y_interpolated = Vx*alpha';
            if nrpoints + nrtees > length(T)
                Y(end+1:end+100,:) = 0;
                T(end+1:end+100) = 0;
                if doparams
                    S(:,:,end+1:end+100) = 0; 
                end;
            end;
            Y(nrpoints+1:nrpoints+nrtees,:) = y_interpolated;
            T(nrpoints+1:nrpoints+nrtees) = t + h*(tees+1)/2;
            if doparams
                t9 = (-dsdt-dsdt_new)*h;
                t10 = (-dsdt+dsdt_new)*h;
                t16 = (d2sdt2_new+d2sdt2)*h2;
                t15 = (d2sdt2-d2sdt2_new)*h2;
                t8 = 0.1953125E-2*h2;
                t7 = d2sdt2*t8;
                t5 = d2sdt2_new*t8;
                alpha0 = 0.5*s_new+0.5*s-0.7421875E-1*t10+0.5859375E-2*t16;
                alpha1 = -1.0*t7+t5+0.4296875E-1*t9+0.5859375*s_new-0.5859375*s;
                alpha2 = 0.78125E-1*t10-0.78125E-2*t16;
                alpha3 = 0.29296875E-2*t15-0.48828125E-1*t9+0.9765625E-1*s-0.9765625E-1*s_new;
                alpha4 = t5+t7-0.390625E-2*t10;
                for i=1:nrtees
                    S(:,:,nrpoints+i) = alpha0 + Vx(i,2)*alpha1 + Vx(i,3)*alpha2 + Vx(i,4)*alpha3 + Vx(i,5)*alpha4;
                end;
            end;
            nrpoints = nrpoints + nrtees;
            if ~isempty(OutputFcn)
                OFstat = OutputFcn( t + h*(Vx(:,2)+1)/2 , y_interpolated(:,OutputSel)' , [] );
                if any( OFstat ), break; end;
            end;
        end

        %% Events?
        if ~isempty(Events)
            [ vals , term , dir ] = Events( t+h , y_new );
            idx = find( vals == 0 | vals.*vals_old < 0 );
            if ~isempty(idx), c5 = V_5_inv * [y, y_new, dy*h/2, dy_new*h/2, dfdy*h^2/4, dfdy_new*h^2/4]'; end;
            for i=idx
                if dir(i) == 0 || ...
                    ( dir(i) > 0 && vals(i) > vals_old(i) ) || ...
                    ( dir(i) < 0 && vals(i) < vals_old(i) )
                    % disp(sprintf('odeSD: caught event %i at t=%e',i,t+h));
                    left = -1; right = 1; val_left = vals_old(i); val_right = vals(i);
                    while ( abs( val_left - val_right ) > 100*eps ) && ( abs( right - left ) > 100*eps )
                        m = left - (right-left) * val_left / (val_right-val_left);
                        Vx = [ 1 , m ]; for j=3:6, Vx(j) = 2 * m * Vx(j-1) - Vx(j-2); end;
                        y_m = (Vx * c5)';
                        vals_m = Events( t + (m + 1) / 2 * h , y_m );
                        val_m = vals_m(i);
                        if val_m == 0
                            break;
                        elseif val_m * val_left < 0
                            right = m; 
                            val_right = val_m;
                        else
                            left = m; 
                            val_left = val_m; 
                        end
                    end;
                    TE(end+1) = t + (m+1)/2 * h;
                    YE(end+1,:) = y_m';
                    IE(end+1) = i;
                    if term(i), T(end+1) = TE(end); Y(end+1,:) = YE(end,:); break; end; % this is probably wrong, fix!
                end;
            end;
            vals_old = vals; 
        end;


        %% Compute Coefficients for Extrapolation of next point
        if nrsteps == 1

            extrap_coeff = [ y , dy * h , -(2*dy*h + 3*y - 3 * y_new + h*dy_new) , 2*y + dy*h - 2*y_new + h*dy_new ];

        else

            % use BDF
            t6 = h_last^2; t8 = h^2; t11 = t8+2*h*h_last+t6; t16 = 1/(t11*h_last); t15 = y_new*t6; t7 = h*t8; t14 = t7*y_last;
            t13 = dy_new*t6; t5 = h_last*t6; t12 = -3*h_last*t8+t5; 
            extrap_coeff = [ y , ...
                (2*t5*y_new-t14-t8*t13+(-t5*dy_new+3*t15)*h+(-2*t5+t7-3*t6*h)*y)*t16 , ...
                -(-2*t14+(-t5*h+t7*h_last)*dy_new+t12*y_new+(2*t7-t12)*y)*t16 , ...
                h*(-t15+(h_last*dy_new-y_last)*t8+(-2*h_last*y_new+t13)*h+t11*y)*t16 ];

        end;
        
        %% Add point to result
        y_last = y; h_last = h; dy_last = dy;
        y = y_new;
        t = t+h;
        nrsteps = nrsteps + 1;
        if nrsteps > length(T)
            Y(end+1:end+100*Refine,:) = 0;
            T(end+1:end+100*Refine) = 0;
            if doparams, S(:,:,end+1:end+100*Refine) = 0; end;
        end;
        if t_len == 2
            Y(nrsteps,:) = y';
            T(nrsteps) = t;
            if doparams, S(:,:,nrsteps) = s_new; end;
        end;
        dy = dy_new;
        dfdy = dfdy_new;
        if doparams
            s = s_new; dsdt_last = dsdt; dsdt = dsdt_new; d2sdt2 = d2sdt2_new;
        end;

        % output function?
        if ~isempty(OutputFcn), if OutputFcn( t , y(OutputSel) , [] ), break; end; end;

        stats_steps_ok = stats_steps_ok + 1;
        b = scale;
        h = h * scale;
            
    end
        
    % output function?
    if ~isempty(OutputFcn), OutputFcn( [] , [] , 'done' ); end;
    
    % clean-up output
    if t_len == 2
        Y = Y(1:nrsteps,:);
        T = T(1:nrsteps);
        if doparams, S = S(:,:,1:nrsteps); end;
    else
        Y = Y(1:nrpoints,:);
        T = T(1:nrpoints);
        if doparams
            S = S(:,:,1:nrpoints);
        end;
    end;
    
    % re-set warnings
    warning(warnSing);
    warning(warnAlmostSing);
    
    %% Display statistics
    if Stats
        disp(sprintf('ode_1step: avg. newton iterations per step: %f',stats_iteration_steps/stats_steps_ok));
        disp(sprintf('ode_1step: nr. matrix inversions: %i (%i steps)',stats_inversions,stats_steps_ok+stats_steps_failed));
        disp(sprintf('ode_1step: nr. newton failures: %i',stats_iterations_failed));
        stats_steps_ok 
        stats_steps_failed
        stats_iterations_failed
        stats_iterations_ok
        stats_iteration_steps
        stats_inversions
    end
    
