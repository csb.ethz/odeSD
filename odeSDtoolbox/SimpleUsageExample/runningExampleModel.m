%% ADD FOLDER AND FUNCTION HANDLES
addpath(strcat('ExampleFolder'));
functionHandle = str2func('exampleModel');
jacobianHandle = str2func('jac_exampleModel');

%% SIMULATION PARAMETERS
simTime = 600; %seconds
simRelTol = 1e-8;
simAbsTol = 1e-8;
maxRuntime = 60; %seconds

initialStateValues = [1, 2];
parameters = [2,5];

%% SIMULATION
odeSuccess = true;
try
    opts = odeset('NonNegative',1:length(initialStateValues),...
                  'Jacobian', jacobianHandle,...
                  'RelTol', simRelTol,...
                  'AbsTol', simAbsTol);
    opts.maxRuntime = maxRuntime;  %extra to limit the solver runtime
   [results.T, results.C] = odeSD(functionHandle, [0 simTime], initialStateValues', opts, parameters);
catch
   warning('Simulation failed');
   odeSuccess = false;
end