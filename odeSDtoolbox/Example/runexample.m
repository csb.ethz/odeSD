clc
clear all

%% odeSD Example
% Check if odeSD is installed, and odeSD MEX was compiled:
odeSDinstalled    = ~isempty(which('odeSD'));
odeSDmexInstalled = ~isempty(which('odeSD_mex')) && ~isempty(which('odeSD_wrapper'));

if ~odeSDinstalled && ~odeSDmexInstalled
    error('Neither odeSD nor odeSD_mex are installed. Run install.m in the toolbox folder.');
end

if odeSDinstalled
    disp('odeSD is installed');
end
if odeSDmexInstalled
    disp('odeSD_mex and odeSD_wrapper are installed');
end
disp('');

%% How many times do we want to integrate, and for how long?
N = 10;
tspan = linspace(0,1000,200)';


%% Generate function handles
mainFunctionName = 'Elowitz2000_6states';
jacFunctionName = 'jac_Elowitz2000_6states';
f = str2func(mainFunctionName);
df = str2func(jacFunctionName);

mexMainFunctionName = [mainFunctionName '_mex'];
mexJacFunctionName = [jacFunctionName '_mex'];
f_mex = str2func(mexMainFunctionName);
df_mex = str2func(mexJacFunctionName);

fprintf('\nTesting by integrating "%s" %i times \n', mainFunctionName, N);

%% Take initial conditions and parameters
[x0 p0] = feval(f);

%% Test odeSD & odeSD_mex with MEXed MATLAB functions
% set-up the options
opts = odeset( 'RelTol', 1.0e-6, ...
    'AbsTol' , eps , ...
    'Jacobian' , df , ...
    'NonNegative' , (1:length(x0)) );

% MATLAB Version
if odeSDinstalled
    tic
    for i = 1:N
        [T1 ,X1 ,~ ,~ ,~ , S1 ] = odeSD(f,tspan,x0,opts,p0);
    end
    timeTaken = toc;

    fprintf('odeSD with MATLAB right hand side:     %.3f seconds\n', timeTaken);
    plotStates(T1, X1, S1);
end

%%
% MEX Version 
if odeSDmexInstalled
    tic
    for i = 1:N
        [T1 ,X1, S1 ] = odeSD_wrapper(f,tspan,x0,opts,p0);
    end
    timeTaken = toc;
    fprintf('odeSD_mex with MATLAB right hand side: %.3f seconds\n', timeTaken);
    plotStates(T1, X1, S1);
end


%% Generate MEX right hand side
fprintf('\nCompiling right hand side. Note that this requires a compatible C compiler.\n');
fprintf('If this fails try executing mex -setup and configuring a compiler.\n');

codegen('-config:mex', mainFunctionName, '-args', {0, zeros(size(x0)), zeros(size(p0))});
codegen('-config:mex', jacFunctionName, '-args', {0, zeros(size(x0)), zeros(size(x0)), zeros(size(p0))} );
codegen('-config:mex', jacFunctionName, '-args', {0, zeros(size(x0)), zeros(size(p0))}, '-o', [jacFunctionName '_matlabODE_mex']);

%% Test odeSD & odeSD_mex with native MATLAB functions
% set-up the options
opts = odeset( opts, 'Jacobian' , df_mex);

% MATLAB Version
if odeSDinstalled
    tic
    for i = 1:N
        [T1 ,X1 ,~ ,~ ,~ , S1 ] = odeSD(f_mex,tspan,x0,opts,p0);
    end
    timeTaken = toc;
    fprintf('odeSD with MEX right hand side:        %.3f seconds\n', timeTaken);
    plotStates(T1, X1, S1);
end
% MEX Version 
if odeSDmexInstalled
    tic
    for i = 1:N
        [T1 ,X1, S1 ] = odeSD_wrapper(f_mex,tspan,x0,opts,p0);
    end
    timeTaken = toc;
    fprintf('odeSD_mex with MEX right hand side:    %.3f seconds\n', timeTaken);
    plotStates(T1, X1, S1);
end

%% Compile Hybrid Solver
fprintf('\nCompiling hybrid solver. Note that this requires a compatible C compiler.\n');
fprintf('If this fails try executing mex -setup and configuring a compiler.\n');
odeSD_compile_hybrid('Elowitz2000_6states.c', mainFunctionName);

%% Test Hybrid Solver
tic
for i = 1:N
    [T1 ,X1, S1 ] = odeSD_wrapper_hybrid(mainFunctionName, tspan, x0, opts, p0);
end
timeTaken = toc;
fprintf('odeSD_hybrid:                          %.3f seconds\n', timeTaken);
plotStates(T1, X1, S1);


